
do $$
begin
raise notice '==== TempusAccess indicators function for PT routes ===';
end$$;

DROP FUNCTION IF EXISTS pgtempus.tempus_create_pt_route_indicator_layer( character varying, character varying, character varying, integer[], integer[], integer[], date[], integer, time, time, boolean, integer );

CREATE OR REPLACE FUNCTION pgtempus.tempus_create_pt_route_indicator_layer(
                                                                                p_table_name_pt_route_trip character varying,
                                                                                p_table_name_pt_route_path character varying,
                                                                                p_table_name_pt_route character varying,
                                                                                p_sources_id smallint[],
                                                                                p_pt_trip_types integer[], 
                                                                                p_pt_agencies integer[], 
                                                                                p_days date[], 
                                                                                p_day_ag integer DEFAULT 1,
                                                                                p_time_start time DEFAULT '00:00:00'::time,
                                                                                p_time_end time DEFAULT '23:59:00'::time, 
                                                                                p_time_constraint_dep boolean DEFAULT True,
                                                                                p_time_ag integer DEFAULT 1
                                                                           ) 
RETURNS void AS
$BODY$
DECLARE
    s character varying;
	t character varying;
    time_ag_str character varying;
    day_ag_str character varying;
    
BEGIN
    -- Mandatory parameters
    IF (
            p_sources_id is null OR p_pt_trip_types is null OR p_pt_agencies is null OR p_days is null OR 
            array_length(p_sources_id, 1)=0 OR array_length(p_pt_trip_types, 1)=0 OR array_length(p_pt_agencies, 1)=0 OR array_length(p_days, 1)=0
       )
    THEN 
        RAISE EXCEPTION 'Parameters 1 to 4 must be non-empty arrays';
    END IF;
    
    IF p_time_constraint_dep = True
    THEN
        t = $$
              AND (time_from <= '$$ || p_time_end::character varying || $$')
              AND (time_from >= '$$ || p_time_start::character varying || $$')
            $$;    
    ELSE
        t = $$
              AND (time_to <= '$$ || p_time_end::character varying || $$')
              AND (time_to >= '$$ || p_time_start::character varying || $$')
            $$;    
    END IF;
    
    -- Aggregating day by day
    IF p_time_ag IS NULL THEN
        p_time_ag=1;
    END IF;
    SELECT INTO time_ag_str 
                func_name 
    FROM pgtempus.agregate WHERE id = p_time_ag;
    
    -- Aggregating all days together
    IF p_day_ag IS NULL THEN
        p_day_ag=1;
    END IF;
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate WHERE id = p_day_ag;
    
    -- Filtered data
    s = $$
        DROP TABLE IF EXISTS filtered_data;
        CREATE TEMPORARY TABLE filtered_data AS
        (
            SELECT pt_trip_id as id, 
                   pt_trip_types_id, 
                   array_agg(id ORDER BY time_from, time_to) as pt_sections_id, 
                   min(time_from) as dep_time,
                   max(time_to) as arr_time,
                   day
            FROM tempus_networks.pt_section_trip_stop_times
            WHERE source_id = ANY('$$ || p_sources_id::character varying || $$')
              AND pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$' = True
              AND pt_agency_id = ANY('$$ || p_pt_agencies::character varying || $$')
              AND day = ANY('$$ || p_days::character varying || $$')
              $$ || t || $$
            GROUP BY pt_trip_id, day, pt_trip_types_id
        )
        $$;
    RAISE NOTICE 'Filtered data:\n%', s; 
    EXECUTE(s);
    
    s = $$
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_route_trip || $$ CASCADE;
        CREATE TABLE tempus_results.$$ || p_table_name_pt_route_trip || $$ AS
        (        
            SELECT row_number() over() as gid, q.*
            FROM
            (
                -- Aggregate days together
                SELECT d.id, 
                       pt_trip_complete.original_id, 
                       pt_trip_complete.pt_stops_id,
                       pt_trip_complete.pt_stops_name, 
                       pt_trip_complete.times::character varying[],
                       pt_trip_complete.source_id,
                       d.pt_trip_types_id, 
                       (SELECT array_agg(DISTINCT pt_trip_type.name) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(d.pt_trip_types_id)) as pt_trip_types_name, 
                       array(select distinct unnest(array_cat_agg(pt_trip_type.gtfs_codes)) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(d.pt_trip_types_id) ORDER BY 1) as gtfs_codes,
                       pt_trip_complete.pt_route_id, 
                       pt_trip_complete.direction,
                       pt_trip_complete.direction_headsign, 
                       pt_route_path_geom.id as pt_route_path_id, 
                       pt_trip_complete.pt_agency_id,
                       pt_trip_complete.traffic_rules, 
                       round(pt_trip_complete.total_dist::numeric, 2) as total_dist, 
                       pt_trip_complete.geom_arc as pt_arcs_geom,
                       pt_trip_complete.geom_nodes as pt_stops_geom,
                       '$$ || p_days::character varying || $$'::date[] as days,
                       array_remove(array_agg(DISTINCT CASE WHEN d.id IS NOT NULL THEN day END ORDER BY CASE WHEN d.id IS NOT NULL THEN day END), NULL) as pt_serv_days, 
                       (times[array_length(pt_trip_complete.times,1)] - pt_trip_complete.times[1])::time AS total_time, 
                       round((pt_trip_complete.total_dist / (extract(epoch from (pt_trip_complete.times[array_length(pt_trip_complete.times,1)] - pt_trip_complete.times[1]))) * 3.6)::numeric,2) as speed -- km/h
                FROM filtered_data d JOIN tempus_networks.pt_section_geom ON ( pt_section_geom.id = ANY(d.pt_sections_id) )
                                     JOIN tempus_networks.pt_trip_complete ON (pt_trip_complete.id = d.id) 
                                     JOIN tempus_networks.pt_route_path_geom ON (pt_trip_complete.pt_sections_id = pt_route_path_geom.pt_sections_id)
                GROUP BY d.id, 
                         pt_trip_complete.original_id, 
                         pt_trip_complete.pt_stops_id, 
                         pt_trip_complete.pt_stops_name,
                         pt_trip_complete.times, 
                         pt_trip_complete.source_id,
                         d.pt_trip_types_id, 
                         pt_trip_complete.pt_route_id,
                         pt_trip_complete.direction, 
                         pt_trip_complete.direction_headsign, 
                         pt_route_path_geom.id, 
                         pt_trip_complete.pt_agency_id,
                         pt_trip_complete.traffic_rules, 
                         pt_trip_complete.total_dist, 
                         pt_trip_complete.geom_arc,
                         pt_trip_complete.geom_nodes
                ORDER BY pt_trip_complete.pt_route_id,
                         pt_trip_complete.direction, 
                         pt_route_path_geom.id,
                         pt_trip_complete.times
            ) q
        ); 
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_route_trip || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || p_table_name_pt_route_trip || $$', 2, 1 )
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s); 
    
    -- Add mapping fields and key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route_trip || $$ ADD COLUMN symbol_size real;
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route_trip || $$ ADD COLUMN symbol_color real;
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route_trip || $$ ADD CONSTRAINT $$ || p_table_name_pt_route_trip || $$_pkey PRIMARY KEY(gid);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    -- Daily aggregated data
    s=$$
        DROP TABLE IF EXISTS daily_aggregated_data; 
        CREATE TEMPORARY TABLE daily_aggregated_data AS
        (
            SELECT pt_sections_id, 
                   array(SELECT DISTINCT unnest(array_cat_agg(pt_trip_types_id))) as pt_trip_types_id, 
                   day, 
                   array_agg(id) as pt_trips_id, 
                   min(dep_time) as first_dep,
                   min(arr_time) as first_arr,
                   max(dep_time) as last_dep,
                   max(arr_time) as last_arr,
                   max(dep_time) - min(dep_time) as time_ampl_dep,
                   max(arr_time) - min(arr_time) as time_ampl_arr,
                 $$ || time_ag_str || $$(arr_time - dep_time) as total_time,
                   count(id) as serv_num
            FROM filtered_data
            GROUP BY pt_sections_id, day
        );
    $$;
    RAISE NOTICE 'Daily aggregated data:\n%', s;
    EXECUTE(s); 
    
    s = $$
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_route_path || $$ CASCADE;
        CREATE TABLE tempus_results.$$ || p_table_name_pt_route_path || $$ AS
        (        
            -- Aggregate days together
            SELECT pt_route_path_geom.id,
                   pt_route_path_geom.pt_stops_name, 
                   pt_route_path_geom.pt_stops_id, 
                   d.pt_sections_id, 
                   array_intersect(pt_route_path_geom.pt_trips_id, d.pt_trips_id) as pt_trips_id, 
                   d.pt_trip_types_id, 
                   (SELECT array_agg(DISTINCT pt_trip_type.name) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(d.pt_trip_types_id)) as pt_trip_types_name, 
                   array(select distinct unnest(array_cat_agg(pt_trip_type.gtfs_codes)) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(d.pt_trip_types_id) ORDER BY 1) as gtfs_codes,
                   pt_route_path_geom.pt_route_id, 
                   pt_route_path_geom.direction,
                   pt_route_path_geom.direction_headsign, 
                   pt_route_path_geom.pt_agency_id, 
                   pt_route_path_geom.source_id, 
                   round(min(pt_route_path_geom.total_dist)::numeric,2) as total_dist,
                   min(pt_route_path_geom.geom_arc) as pt_arcs_geom,
                   min(pt_route_path_geom.geom_nodes) as pt_stops_geom,
                   '$$ || p_days::character varying || $$'::date[] as days,
                   array_remove(array_agg(DISTINCT CASE WHEN d.pt_sections_id IS NOT NULL THEN day END ORDER BY CASE WHEN d.pt_sections_id IS NOT NULL THEN day END), NULL) as pt_serv_days, 
                   date_trunc('second', $$ || day_ag_str || $$( d.first_dep ))::time as first_dep, 
                   date_trunc('second', $$ || day_ag_str || $$( d.first_arr ))::time as first_arr, 
                   date_trunc('second', $$ || day_ag_str || $$( d.last_dep ))::time as last_dep, 
                   date_trunc('second', $$ || day_ag_str || $$( d.last_arr ))::time as last_arr, 
                   extract(epoch from $$ || day_ag_str || $$( d.time_ampl_dep ))/60 as time_ampl_dep, 
                   extract(epoch from $$ || day_ag_str || $$( d.time_ampl_arr ))/60 as time_ampl_arr, 
                   $$ || day_ag_str || $$( d.total_time )::time as total_time, 
                   round($$ || day_ag_str || $$( d.serv_num ),2) as serv_num, 
                   round($$ || day_ag_str || $$( pt_route_path_geom.total_dist / (extract(epoch from (d.total_time))) * 3.6 )::numeric,2) as speed -- km/h
            FROM daily_aggregated_data d JOIN tempus_networks.pt_route_path_geom ON ( d.pt_sections_id = pt_route_path_geom.pt_sections_id )
            GROUP BY pt_route_path_geom.id, 
                     d.pt_sections_id, 
                     pt_route_path_geom.pt_stops_id, 
                     pt_route_path_geom.pt_stops_name, 
                     pt_route_path_geom.pt_trips_id,
                     d.pt_trips_id, 
                     pt_route_path_geom.direction, 
                     pt_route_path_geom.direction_headsign, 
                     d.pt_trip_types_id, 
                     pt_route_path_geom.pt_route_id, 
                     pt_route_path_geom.pt_agency_id, 
                     pt_route_path_geom.source_id, 
                     pt_route_path_geom.total_dist,
                     pt_route_path_geom.geom_arc,
                     pt_route_path_geom.geom_nodes
            ORDER BY pt_route_path_geom.id
        );
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_route_path || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || p_table_name_pt_route_path || $$', 2, 2 )
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
        
    -- Add mapping fields and key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route_path || $$ ADD COLUMN symbol_size real;
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route_path || $$ ADD COLUMN symbol_color real;
            --ALTER TABLE tempus_results.$$ || p_table_name_pt_route_path || $$ ADD CONSTRAINT $$ || p_table_name_pt_route_path || $$_pkey PRIMARY KEY(id);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
        
    -- Filtered data
    s = $$
        DROP TABLE IF EXISTS filtered_data;
        CREATE TEMPORARY TABLE filtered_data AS
        (
            SELECT pt_route_id as id, 
                   unnest(pt_trip_types_id) as pt_trip_type_id, 
                   pt_trip_id,
                   day
            FROM tempus_networks.pt_section_trip_stop_times
            WHERE source_id = ANY('$$ || p_sources_id::character varying || $$')
              AND pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$' = True
              AND pt_agency_id = ANY('$$ || p_pt_agencies::character varying || $$')
              AND day = ANY('$$ || p_days::character varying || $$')
              $$ || t || $$
        )
        $$; 
    RAISE NOTICE 'Filtered data:\n%', s; 
    EXECUTE(s);
    
    -- Daily aggregated data
    s = $$
        DROP TABLE IF EXISTS daily_agregated_data;
        CREATE TEMPORARY TABLE daily_agregated_data AS
        (
            SELECT d.id, 
                   d.pt_trip_type_id, 
                   array_agg(d.pt_trip_id) as pt_trips_id, 
                   array_length(array_agg(d.pt_trip_id), 1) as serv_num, 
                   sum(pt_trip_complete.total_dist) as veh_km,
                   day
            FROM filtered_data d JOIN tempus_networks.pt_trip_complete ON ( pt_trip_complete.id = d.pt_trip_id) 
            GROUP BY d.id, d.pt_trip_type_id, day
        )
        $$;
    RAISE NOTICE 'Daily agregated data:\n%', s; 
    EXECUTE(s);
    
    s = $$
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_route || $$ CASCADE;
        CREATE TABLE tempus_results.$$ || p_table_name_pt_route || $$ AS
        (        
            SELECT row_number() over() as gid, q.*
            FROM 
            (
                -- Aggregate days together 
                SELECT pt_route_complete.id, 
                       pt_route_complete.original_id, 
                       pt_route_complete.short_name,
                       pt_route_complete.long_name,
                       d.pt_trip_type_id, 
                       pt_trip_type.gtfs_codes,
                       pt_route_complete.pt_agency_id, 
                       pt_route_complete.source_id, 
                       '$$ || p_days::character varying || $$'::date[] as days,
                       array_remove(array_agg(DISTINCT CASE WHEN d.id IS NOT NULL THEN day END ORDER BY CASE WHEN d.id IS NOT NULL THEN day END), NULL) as pt_serv_days, 
                       round($$ || day_ag_str || $$( d.serv_num ),2) as serv_num, 
                       round($$ || day_ag_str || $$( d.veh_km ),2) as veh_km
                FROM daily_agregated_data d JOIN tempus_networks.pt_route_complete ON ( d.id = pt_route_complete.id )
                                            JOIN pgtempus.pt_trip_type ON (pt_trip_type.id = d.pt_trip_type_id)
                GROUP BY pt_route_complete.id, 
                         pt_route_complete.original_id, 
                         pt_route_complete.short_name, 
                         pt_route_complete.long_name, 
                         d.pt_trip_type_id, 
                         pt_trip_type.gtfs_codes, 
                         pt_route_complete.pt_agency_id, 
                         pt_route_complete.source_id
                ORDER BY pt_route_complete.id
            ) q
        ); 
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_route || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || p_table_name_pt_route || $$', 2, 3 )
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    -- Add key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_route || $$ ADD CONSTRAINT $$ || p_table_name_pt_route || $$_pkey PRIMARY KEY(gid);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    RETURN;
END; 

$BODY$
LANGUAGE plpgsql; 
  
  
