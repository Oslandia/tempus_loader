DROP SCHEMA IF EXISTS %(temp_schema) CASCADE;
CREATE SCHEMA %(temp_schema); 

CREATE TABLE %(temp_schema).agency AS
(
    SELECT pt_agency.id as agency_id, 
           pt_agency.name as agency_name, 
           pt_agency.url as agency_url, 
           pt_agency.timezone as agency_timezone, 
           pt_agency.lang as agency_lang 
    FROM tempus_networks.pt_agency JOIN pgtempus.source ON (source.id = pt_agency.source_id)
    WHERE source.name = ANY(ARRAY%(source_name))
    ORDER BY 1
);

CREATE TABLE %(temp_schema).calendar AS
(
    SELECT id as service_id, 
           id as monday, 
           id as tuesday, 
           id as wednesday, 
           id as thursday, 
           id as friday, 
           id as saturday, 
           id as sunday, 
           id as start_date, 
           id as end_date
    FROM tempus_networks.days_period
    WHERE 1=0
);

CREATE TABLE %(temp_schema).calendar_dates AS
(
    SELECT days_period.id as service_id, 
           to_char(unnest(days_period.days), 'YYYYMMDD') as date, 
           1 as exception_type 
    FROM tempus_networks.days_period JOIN tempus_networks.pt_trip ON (pt_trip.days_period_id = days_period.id)
                                     JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                                     JOIN tempus_networks.pt_agency ON (pt_route.pt_agency_id = pt_agency.id)
                                     JOIN pgtempus.source ON (source.id = pt_agency.source_id)
    WHERE source.name = ANY(ARRAY%(source_name))
);

CREATE TABLE %(temp_schema).routes AS
(
    SELECT DISTINCT 
	       pt_route.id as route_id, 
           pt_agency.id as agency_id, 
           pt_route.short_name as route_short_name, 
           pt_route.long_name as route_long_name, 
           pt_route.description as route_desc, 
		   unnest(pt_trip_type.gtfs_codes) as route_type,
           pt_route.url as route_url,
           pt_route.color as route_color,
           pt_route.text_color as route_text_color 
    FROM tempus_networks.pt_route JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id) 
                                  JOIN pgtempus.source ON (source.id = pt_agency.source_id)
                                  JOIN tempus_networks.pt_trip ON (pt_trip.pt_route_id = pt_route.id)
                                  JOIN pgtempus.pt_trip_type ON (pt_trip_type.id = ANY(pt_trip.pt_trip_types_id))
    WHERE source.name = ANY(ARRAY%(source_name))
);


CREATE TABLE %(temp_schema).stop_times AS
WITH q AS (
    SELECT id, pt_route_id, geom_arc
    FROM tempus_networks.pt_trip
	order by 1, 2
)
    SELECT q.id as trip_id,
           st2.time_to as arrival_time,
           st1.time_from as departure_time,
           node.id as stop_id,
           row_number() over(PARTITION BY q.id ORDER BY st1.time_from, st2.time_to) as stop_sequence,
           st1.direction_label as stop_headsign,
           st1.pickup_type as pickup_type,
           st2.dropoff_type as drop_off_type,
           (st_LineLocatePoint(q.geom_arc, node.geom)*st_length(q.geom_arc))::integer as shape_dist_traveled, 
           CASE WHEN least(st1.interpolated_time_from, st2.interpolated_time_to) = False  THEN 1 ELSE 0 END as timepoint
    FROM tempus_networks.node JOIN tempus_networks.arc arc1 ON (arc1.node_from_id = node.id)
                              JOIN tempus_networks.pt_section_stop_times st1 ON (st1.arc_id = arc1.id)
                              JOIN tempus_networks.arc arc2 ON (arc2.node_to_id = node.id)
                              JOIN tempus_networks.pt_section_stop_times st2 ON (st2.arc_id = arc2.id AND st2.pt_trip_id = st1.pt_trip_id)
                              JOIN q ON (q.id = st1.pt_trip_id AND q.id = st2.pt_trip_id)
                              JOIN tempus_networks.pt_route ON (pt_route.id = q.pt_route_id)
                              JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                              JOIN pgtempus.source ON (pt_agency.source_id = source.id)
    WHERE source.name = ANY(ARRAY%(source_name));

CREATE TABLE %(temp_schema).stops AS
(
    SELECT node.id as stop_id, 
           pt_stop.name as stop_name, 
           pt_stop.description as stop_desc, 
           st_y(st_transform(pt_stop_geom, 4326)) as stop_lat, 
           st_x(st_transform(pt_stop_geom, 4326)) as stop_lon, 
           pt_stop.pt_fare_zone_id as zone_id, 
           pt_stop.url as stop_url,
           0 as location_type,
           pt_stop_geom.station_id as parent_station
    FROM tempus_networks.pt_stop JOIN tempus_networks.node ON (node.id = pt_stop.node_id)
                                 JOIN pgtempus.source ON (source.id = node.source_id)
                                 JOIN tempus_networks.pt_stop_geom ON (pt_stop_geom.id = pt_stop.node_id)
    WHERE source.name = ANY(ARRAY%(source_name))
    UNION
    SELECT pt_stop_geom.station_id as stop_id,
           pt_stop_geom.station_name as stop_name,
           pt_stop.description as stop_desc,
           st_y(st_transform(pt_station_geom, 4326)) as stop_lat,
           st_x(st_transform(pt_station_geom, 4326)) as stop_lon,
           pt_stop.pt_fare_zone_id as zone_id,
           pt_stop.url as stop_url,
           1 as location_type,
           null as parent_station
    FROM tempus_networks.pt_stop JOIN tempus_networks.node ON (node.id = pt_stop.node_id)
                                 JOIN pgtempus.source ON (source.id = node.source_id)
                                 JOIN tempus_networks.pt_stop_geom ON (pt_stop_geom.id = pt_stop.node_id)
    WHERE source.name = ANY(ARRAY%(source_name))
);

CREATE TABLE %(temp_schema).transfers AS
(
    SELECT stop1.node_id as from_stop_id,
           stop2.node_id as to_stop_id,
           2 as transfer_type,
           st_length(geography(st_transform(geom,4326)))::integer as min_transfer_time -- transfer time in seconds, with hypothesis of a 1 m/s speed
    FROM tempus_networks.arc JOIN tempus_networks.pt_stop stop1 ON (arc.node_from_id = stop1.node_id)
                             JOIN tempus_networks.pt_stop stop2 ON (arc.node_to_id = stop2.node_id)
                             JOIN pgtempus.source ON (source.id = ANY(arc.sources_id)
    WHERE arc.type = 1 AND source.name = ANY(ARRAY%(source_name))
);

CREATE TABLE %(temp_schema).shapes AS
(
    WITH s AS (    
        WITH r AS (
            WITH q AS (
                SELECT pt_route_path_geom.id as shape_id, st_y(st_transform((st_dumppoints(geom_arc)).geom, 4326)) as shape_pt_lat, st_x(st_transform((st_dumppoints(geom_arc)).geom, 4326)) as shape_pt_lon
                FROM tempus_networks.pt_route_path_geom JOIN pgtempus.source ON (source.id = pt_route_path_geom.source_id)
                WHERE source.name = ANY(ARRAY%(source_name))
                ORDER BY pt_route_path_geom.id, (st_dumppoints(geom_arc)).path
            )
            SELECT shape_id, shape_pt_lat, shape_pt_lon, row_number() over(partition by shape_id) as shape_pt_sequence 
            FROM q
        )	
        SELECT shape_id, shape_pt_lat, shape_pt_lon, min(shape_pt_sequence)
        FROM r
        GROUP BY shape_id, shape_pt_lat, shape_pt_lon
        ORDER BY 1, 4
    )
    SELECT shape_id, shape_pt_lat, shape_pt_lon, row_number() over(partition by shape_id) as shape_pt_sequence
    FROM s
);

CREATE TABLE %(temp_schema).trips AS
(
    SELECT pt_trip.id as trip_id, 
           pt_trip.pt_route_id as route_id, 
           pt_trip.days_period_id as service_id, 
           pt_trip.direction_headsign as trip_headsign, 
           pt_trip.direction as direction_id, 
           CASE WHEN 2 = ANY(pt_trip.traffic_rules) THEN 1 ELSE 2 END as wheelchair_accessible, 
           CASE WHEN 3 = ANY(pt_trip.traffic_rules) THEN 1 ELSE 2 END as bikes_allowed,
           pt_route_path_geom.id as shape_id 
    FROM tempus_networks.pt_trip JOIN tempus_networks.pt_route_path_geom ON (pt_trip.id = ANY(pt_route_path_geom.pt_trips_id))
                                 JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                                 JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                 JOIN pgtempus.source ON (source.id = pt_agency.source_id)
    WHERE source.name = ANY(ARRAY%(source_name))
);

CREATE TABLE %(temp_schema).fare_attributes AS
(
    SELECT pt_fare_rule.id as fare_id, 
           pt_zonal_fare.fare as price, 
           pt_fare_rule.currency_type as currency_type, 
           pt_fare_rule.payment_method as payment_method, 
           pt_fare_rule.pt_agency_id as agency_id, 
           pt_fare_rule.max_transfers as transfers, 
           pt_fare_rule.max_transfer_duration as transfer_duration
        FROM pgtempus.pt_fare_rule JOIN tempus_networks.pt_zonal_fare ON (pt_fare_rule.id = pt_zonal_fare.pt_fare_rule)
                                   JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_fare_rule.pt_agency_id)
                                   JOIN pgtempus.source ON (source.id = pt_agency.source_id)
        WHERE source.name = ANY(ARRAY%(source_name))
);

CREATE TABLE %(temp_schema).fare_rules AS
(
    SELECT pt_zonal_fare.pt_fare_rule as fare_id, 
           pt_zonal_fare.pt_route_id as route_id, 
           pt_zonal_fare.zone_id_from as origin_id, 
           pt_zonal_fare.zone_id_to as destination_id, 
           null as contains_id 
        FROM tempus_networks.pt_zonal_fare JOIN tempus_networks.pt_route ON (pt_route.id = pt_zonal_fare.pt_route_id)
                                           JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                           JOIN pgtempus.source ON (source.id = pt_agency.source_id)
        WHERE source.name = ANY(ARRAY%(source_name))
);
