do $$
begin
raise notice '==== Refresh materialized views ===';
end$$;

SELECT pgtempus.tempus_refresh_materialized_views();

do $$
begin
raise notice '==== Restore triggers ===';
end$$;

SELECT pgtempus.tempus_restore_triggers();
