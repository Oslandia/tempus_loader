/*
        Substitutions options
        %(source_name): name of the PT network
        %(max_dist): maximum distance to link a PT stop to a road arc
        %(temp_schema): name of schema containing temporary data
*/

do $$
begin
raise notice '==== PT network and transport modes creation ====';
end$$;

DROP TABLE IF EXISTS %(temp_schema).pt_trip_type_idmap; 
CREATE TABLE %(temp_schema).pt_trip_type_idmap 
(
    id serial PRIMARY KEY,
    gtfs_codes smallint[], 
    name character varying, 
    security_time interval
);
SELECT setval('%(temp_schema).pt_trip_type_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM pgtempus.pt_trip_type), false);

INSERT INTO %(temp_schema).pt_trip_type_idmap(gtfs_codes, name)
SELECT DISTINCT ARRAY[route_type]::smallint[], CASE WHEN route_type = 0 THEN 'Tramway ou métro léger' 
                                                    WHEN route_type = 1 THEN 'Métro' 
                                                    WHEN route_type = 2 THEN 'Train'
                                                    WHEN route_type = 3 THEN 'Bus'
                                                    WHEN route_type = 4 THEN 'Ferry' 
                                                    WHEN route_type = 5 THEN 'Tramway à traction par câble'
                                                    WHEN route_type = 6 THEN 'Télécabine ou téléphérique'
                                                    WHEN route_type = 7 THEN 'Funiculaire'
                                               END
FROM %(temp_schema).routes                                               
EXCEPT
SELECT gtfs_codes, name
FROM pgtempus.pt_trip_type;

INSERT INTO pgtempus.pt_trip_type( id, name, gtfs_codes )
SELECT id, name, gtfs_codes FROM %(temp_schema).pt_trip_type_idmap ORDER BY id;
SELECT setval('pgtempus.pt_trip_type_id_seq', (SELECT coalesce(max(id)+1, 1) FROM pgtempus.pt_trip_type), false);

do $$
begin
raise notice '==== PT agencies creation ====';
end$$;

DROP TABLE IF EXISTS %(temp_schema).agency_idmap; 
CREATE TABLE %(temp_schema).agency_idmap 
(
    id serial PRIMARY KEY,
    original_id character varying
);

SELECT setval('%(temp_schema).agency_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.pt_agency), false);
INSERT INTO %(temp_schema).agency_idmap( original_id )
SELECT DISTINCT agency_id FROM %(temp_schema).agency;
CREATE INDEX agency_idmap_original_id_idx ON %(temp_schema).agency_idmap(original_id);

INSERT INTO tempus_networks.pt_agency(id, original_id, source_id, name, url, timezone, lang)
    SELECT
            (SELECT id FROM %(temp_schema).agency_idmap WHERE agency_idmap.original_id=agency.agency_id) AS id, 
            agency_id, 
            (SELECT id FROM pgtempus.source WHERE name = '%(source_name)'), 
            agency_name, 
            agency_url, 
            agency_timezone, 
            agency_lang
    FROM %(temp_schema).agency;

do $$
begin
raise notice '==== PT calendars creation ====';
end$$;

CREATE TABLE %(temp_schema).days_period
(
    id serial PRIMARY KEY,
    service_id character varying, 
    days date[]
);
SELECT setval('%(temp_schema).days_period_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.days_period), false);

WITH foo AS
(
    SELECT start_date::date + generate_series(0, end_date::date - start_date::date) AS date, 
        service_id, 
        monday::boolean AS monday
        , tuesday::boolean AS tuesday
        , wednesday::boolean AS wednesday
        , thursday::boolean AS thursday
        , friday::boolean AS friday
        , saturday::boolean AS saturday
        , sunday::boolean AS sunday
        , start_date::date AS start_date
        , end_date::date AS end_date
    FROM 
        %(temp_schema).calendar
)
INSERT INTO %(temp_schema).days_period( service_id, days )
(
    SELECT service_id, array_agg(DISTINCT date ORDER BY date) as days
    FROM
    (
        (
            SELECT foo.service_id, date
            FROM foo
            WHERE (monday::boolean = true AND extract('dow' FROM date)=1) 
               OR (tuesday::boolean = true AND extract('dow' FROM date)=2)
               OR (wednesday::boolean = true AND extract('dow' FROM date)=3)
               OR (thursday::boolean = true AND extract('dow' FROM date)=4)
               OR (friday::boolean = true AND extract('dow' FROM date)=5)
               OR (saturday::boolean = true AND extract('dow' FROM date)=6)
               OR (sunday::boolean = true AND extract('dow' FROM date)=0)
        )
        UNION
        (
            (
                SELECT calendar_dates.service_id, calendar_dates.date::date
                FROM %(temp_schema).calendar_dates
                WHERE exception_type::integer=1 
            )
            EXCEPT
            (
                SELECT calendar_dates.service_id, calendar_dates.date::date
                FROM %(temp_schema).calendar_dates
                WHERE exception_type::integer=2
            )
        )
    ) q
    GROUP BY service_id
);

INSERT INTO tempus_networks.days_period( id, original_id, days ) 
SELECT id, service_id, days
FROM %(temp_schema).days_period;

do $$
begin
RAISE NOTICE '==== PT stops and inter-stops arcs creation ====';
end$$;

-- General nodes
DROP TABLE IF EXISTS %(temp_schema).node; 
CREATE TABLE %(temp_schema).node
(
    id bigserial PRIMARY KEY,
    original_id character varying, 
    geom Geometry(PointZ, %(target_srid))
);
ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node));

INSERT INTO %(temp_schema).node( original_id, geom )
    SELECT stop_id, st_force3DZ(st_transform(st_setsrid(st_point(stop_lon, stop_lat), %(source_srid)), %(target_srid)))::Geometry(PointZ, %(target_srid))
    FROM %(temp_schema).stops;
    
CREATE INDEX ON %(temp_schema).node USING btree(original_id);
CREATE INDEX ON %(temp_schema).node USING gist(geom);

DROP TABLE IF EXISTS %(temp_schema).zone_idmap; 
CREATE TABLE %(temp_schema).zone_idmap 
(
    id serial PRIMARY KEY,
    original_id character varying
);

SELECT setval('%(temp_schema).zone_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.pt_fare_zone), false);
INSERT INTO %(temp_schema).zone_idmap( original_id )
SELECT DISTINCT zone_id FROM %(temp_schema).stops;
CREATE INDEX zone_idmap_original_id_idx ON %(temp_schema).zone_idmap(original_id);

INSERT INTO tempus_networks.pt_fare_zone(id, original_id)
SELECT id, 
       original_id
FROM %(temp_schema).zone_idmap; 

-- Stops which are really served by public transport services are selected as future nodes of the PT network
DROP TABLE IF EXISTS %(temp_schema).pt_stop; 
CREATE TABLE %(temp_schema).pt_stop
(
    node_id serial, 
    original_id character varying, 
    parent_node_id integer,
    location_type integer, 
    stop_name character varying,
    traffic_rules smallint[], 
    zone_id character varying, 
    url character varying,
    description character varying,
    security_time time
);

INSERT INTO %(temp_schema).pt_stop( node_id, original_id, parent_node_id, location_type, stop_name, traffic_rules, zone_id, url, description )
    SELECT DISTINCT
            node.id, 
            stops.stop_id,
            parent_node.id, 
            stops.location_type, 
            stops.stop_name, 
            CASE 
                 WHEN stops.wheelchair_boarding = 0 OR stops.wheelchair_boarding IS NULL THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[]
                 WHEN stops.wheelchair_boarding = 1 THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Walking' or name = 'Wheelchair')
                 WHEN stops.wheelchair_boarding = 2 THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[]
            END, 
            stops.zone_id,
            stops.stop_url,
            stops.stop_desc
    FROM %(temp_schema).stops JOIN %(temp_schema).node ON (node.original_id = stops.stop_id)
                              JOIN %(temp_schema).stop_times ON (stops.stop_id = stop_times.stop_id)
                              LEFT JOIN %(temp_schema).node as parent_node ON (parent_node.original_id = stops.parent_station); 
CREATE INDEX ON %(temp_schema).pt_stop USING btree(original_id);

-- POI
DROP TABLE IF EXISTS %(temp_schema).poi; 
CREATE TABLE %(temp_schema).poi
(
    node_id serial, 
    original_id character varying, 
    name character varying,
    type integer
);

-- Insert POI
INSERT INTO %(temp_schema).poi( name, node_id, type )
    (
    -- Station
    SELECT DISTINCT stop_name, 
                    node.id,
                    1 as type
    FROM %(temp_schema).stops JOIN %(temp_schema).node ON (node.original_id = stops.stop_id)
    WHERE stop_id IN (SELECT DISTINCT parent_station FROM %(temp_schema).stops WHERE parent_station IS NOT NULL) OR location_type = 1 
    UNION
    -- Exit and entrance
    SELECT stop_name, 
           node.id, 
           2 as type
    FROM %(temp_schema).stops JOIN %(temp_schema).node ON (node.original_id = stops.stop_id)
    WHERE location_type = 2
    UNION 
    -- Platform
    SELECT stop_name, 
           node.id, 
           1 as type
    FROM %(temp_schema).stops JOIN %(temp_schema).node ON (node.original_id = stops.stop_id)
    WHERE location_type = 0 or location_type = 4 or location_type IS NULL
    )
    EXCEPT
    (
    SELECT DISTINCT stop_name, node_id, 1 FROM %(temp_schema).pt_stop    
    );

CREATE INDEX ON %(temp_schema).poi USING btree(original_id);

CREATE TABLE %(temp_schema).arc( 
                                    id bigserial, 
                                    sources_id smallint[], 
                                    original_id character varying, 
                                    node_from_id integer, 
                                    node_to_id integer, 
                                    traffic_rules smallint[], 
                                    street_name character varying,
                                    road_number character varying,
                                    length float, 
                                    diffusion smallint,
                                    crossability smallint,
                                    section_type_id smallint,
                                    geom Geometry(LinestringZ, %(target_srid))
                               );
ALTER SEQUENCE %(temp_schema).arc_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).arc_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.arc));

INSERT INTO %(temp_schema).arc( original_id, sources_id, node_from_id, node_to_id, traffic_rules, section_type_id, length, diffusion, crossability, geom )
    -- Link each real stop to its parent station with bidirectional virtual walking arcs
    SELECT pt_stop.original_id, 
           ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
           node_id, 
           parent_node_id, 
           ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[] as traffic_rules, 
           1 as section_type_id, 
           0 as length, 
           3 as diffusion,
           0 as crossability,           
           st_setsrid(st_makeline(node.geom, parent_node.geom), %(target_srid)) as geom
    FROM %(temp_schema).pt_stop JOIN %(temp_schema).node ON (pt_stop.node_id = node.id)
                                JOIN %(temp_schema).node parent_node ON (pt_stop.parent_node_id = parent_node.id)
    UNION
    SELECT pt_stop.original_id, 
           ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
           parent_node_id, 
           node_id, 
           ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[] as traffic_rules, 
           1 as section_type_id, 
           0 as length, 
           3 as diffusion,
           0 as crossability,
           st_setsrid(st_makeline(parent_node.geom, node.geom), %(target_srid)) as geom
    FROM %(temp_schema).pt_stop JOIN %(temp_schema).node ON (pt_stop.node_id = node.id)
                                JOIN %(temp_schema).node parent_node ON (pt_stop.parent_node_id = parent_node.id)
    UNION
    -- Add arcs corresponding to the "transfers.txt" table
    SELECT null, 
           ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
           node_from.id, 
           node_to.id, 
           ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[] as traffic_rules, 
           2 as section_type_id, 
           coalesce(min_transfer_time::integer, st_length(st_makeline(node_from.geom, node_to.geom))) as length, 
           0 as diffusion,
           0 as crossability,
           st_setsrid(st_makeline(node_from.geom, node_to.geom), %(target_srid)) as geom
    FROM %(temp_schema).transfers JOIN %(temp_schema).node node_from ON (transfers.from_stop_id = node_from.original_id)
                                  JOIN %(temp_schema).node node_to ON (transfers.to_stop_id = node_to.original_id)
    WHERE transfer_type != 3
    UNION
    -- Add arcs corresponding to the "pathways.txt" table
    SELECT pathways.pathway_id,  
           ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
           node_from.id, 
           node_to.id, 
           ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[] as traffic_rules, 
           2 as section_type_id, 
           coalesce(traversal_time::integer, st_length(st_makeline(node_from.geom, node_to.geom))) as length, 
           3 as diffusion,
           0 as crossability,
           st_setsrid(st_makeline(node_from.geom, node_to.geom), %(target_srid)) as geom
    FROM %(temp_schema).pathways JOIN %(temp_schema).node node_from ON (pathways.from_stop_id = node_from.original_id)
                                 JOIN %(temp_schema).node node_to ON (pathways.to_stop_id = node_to.original_id); 

-- New nodes
INSERT INTO tempus_networks.node(id, original_id, source_id, geom)
    SELECT id, original_id, (SELECT id FROM pgtempus.source WHERE name = '%(source_name)'), geom
    FROM %(temp_schema).node; 

-- New POI
INSERT INTO tempus_networks.poi(name, type, node_id)
SELECT name, type, node_id
FROM %(temp_schema).poi; 

-- New PT stops
INSERT INTO tempus_networks.pt_stop(node_id, name, traffic_rules, pt_fare_zone_id, gtfs_location_type)
    SELECT
            node_id, 
            stop_name, 
            traffic_rules,
            (SELECT id FROM %(temp_schema).zone_idmap WHERE zone_idmap.original_id = pt_stop.zone_id) AS fare_zone_id, 
            location_type AS gtfs_location_type
    FROM %(temp_schema).pt_stop;

INSERT INTO tempus_networks.arc(id, sources_id, original_id, node_from_id, node_to_id, length, type, diffusion, crossability, geom)
    SELECT id, sources_id, original_id, node_from_id, node_to_id, st_length(geom)*1.4, 1, diffusion, crossability, geom  
    FROM %(temp_schema).arc;

-- Corresponding road section attributes
INSERT INTO tempus_networks.road_section_attributes( arc_id, traffic_rules, street_name, road_number, section_type_id )
    SELECT id, traffic_rules, street_name, road_number, section_type_id
    FROM %(temp_schema).arc;

---- Nodes corresponding to abstract POIs in the nodes temporary table are deleted, since they do not need to be linked to the road network
DELETE FROM %(temp_schema).node
WHERE id IN (SELECT node_id FROM %(temp_schema).poi JOIN pgtempus.poi_type ON (poi_type.id = poi.type) WHERE poi_type.abstract = True);

do $$
begin
raise notice '==== PT routes ====';
end$$;

DROP TABLE IF EXISTS %(temp_schema).route_idmap; 
CREATE TABLE %(temp_schema).route_idmap 
(
    id serial PRIMARY KEY,
    original_id character varying
);

SELECT setval('%(temp_schema).route_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.pt_route), false);
INSERT INTO %(temp_schema).route_idmap( original_id )
SELECT DISTINCT route_id FROM %(temp_schema).routes ORDER BY route_id;
CREATE INDEX route_idmap_original_id_idx ON %(temp_schema).route_idmap(original_id);

INSERT INTO tempus_networks.pt_route (id, original_id, pt_agency_id, short_name, long_name, description, url, color, text_color) 
(
    SELECT  (SELECT id FROM %(temp_schema).route_idmap WHERE routes.route_id=route_idmap.original_id) as id,
            route_id, 
            (SELECT id FROM %(temp_schema).agency_idmap WHERE routes.agency_id=agency_idmap.original_id),
            route_short_name, 
            route_long_name, 
            route_desc, 
            route_url,
            route_color,
            route_text_color
    FROM %(temp_schema).routes
);

do $$
begin
raise notice '==== PT fare rules creation ====';
end$$;

DROP TABLE IF EXISTS %(temp_schema).fare_rule_idmap; 
CREATE TABLE %(temp_schema).fare_rule_idmap 
(
    id serial PRIMARY KEY,
    original_id character varying
);

SELECT setval('%(temp_schema).fare_rule_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM pgtempus.pt_fare_rule), false);
INSERT INTO %(temp_schema).fare_rule_idmap( original_id )
SELECT DISTINCT fare_id FROM %(temp_schema).fare_attributes;
CREATE INDEX fare_rule_idmap_original_id_idx ON %(temp_schema).fare_rule_idmap(original_id);

INSERT INTO pgtempus.pt_fare_rule(id, original_id, pt_agency_id, currency_type, payment_method, max_transfers, max_transfer_duration)
(
SELECT (SELECT id FROM %(temp_schema).fare_rule_idmap WHERE original_id = fare_attributes.fare_id) as id, 
       fare_attributes.fare_id,
       (SELECT id FROM %(temp_schema).agency_idmap WHERE agency_idmap.original_id=fare_attributes.agency_id) as pt_agency_id, 
       currency_type, 
       payment_method,
       transfers as max_transfers,
       transfer_duration as max_transfer_duration
FROM %(temp_schema).fare_attributes
);

INSERT INTO tempus_networks.pt_zonal_fare(pt_fare_rule, pt_route_id, zone_id_from, zone_id_to, fare)
SELECT (SELECT id FROM pgtempus.pt_fare_rule where original_id = fare_rules.fare_id),
       (SELECT id FROM %(temp_schema).route_idmap WHERE route_idmap.original_id=fare_rules.route_id) as pt_route_id, 
       (SELECT id FROM %(temp_schema).zone_idmap WHERE zone_idmap.original_id=fare_rules.origin_id) as zone_id_from,
       (SELECT id FROM %(temp_schema).zone_idmap WHERE zone_idmap.original_id=fare_rules.destination_id) as zone_id_to,
       (SELECT price FROM %(temp_schema).fare_attributes WHERE fare_attributes.fare_id = fare_rules.fare_id)
FROM %(temp_schema).fare_rules;

do $$
begin
RAISE NOTICE '==== PT arcs, PT trips, shapes and stop_times creation ====';
end$$;

UPDATE %(temp_schema).shapes
SET geom = st_force3DZ(st_transform(st_setsrid(st_makepoint(shape_pt_lon, shape_pt_lat), %(source_srid)), %(target_srid)));

CREATE INDEX shapes_shape_id_idx ON %(temp_schema).shapes(shape_id);

CREATE TABLE %(temp_schema).pt_section_stop_times AS
(
    WITH pt_stop_sequence AS (
        SELECT
            trip_id
            -- gtfs stop sequences can have holes
            -- use the dense rank to have then continuous
            , dense_rank() over win AS new_stopseq
            , stop_sequence
            , stop_id
            , %(temp_schema).format_gtfs_time(arrival_time) as arrival_time
            , %(temp_schema).format_gtfs_time(departure_time) as departure_time
            , stop_headsign
            , pickup_type
            , drop_off_type
        FROM %(temp_schema).stop_times 
        window win AS (PARTITION BY trip_id ORDER BY stop_sequence)
    )
    SELECT stop_from.node_id AS node_from_id,
           stop_to.node_id AS node_to_id,
           t1.departure_time, 
           t2.arrival_time, 
           trips.trip_id,
           t1.stop_headsign as direction_label,
           t1.pickup_type,
           t2.drop_off_type
    FROM pt_stop_sequence AS t1 JOIN pt_stop_sequence AS t2 ON (t1.trip_id = t2.trip_id AND t1.new_stopseq = t2.new_stopseq - 1)
                                JOIN %(temp_schema).pt_stop stop_from ON (stop_from.original_id = t1.stop_id)
                                JOIN %(temp_schema).pt_stop stop_to ON (stop_to.original_id = t2.stop_id)
                                JOIN %(temp_schema).trips ON t1.trip_id = trips.trip_id
                                JOIN %(temp_schema).routes ON (routes.route_id = trips.route_id)
    ORDER BY stop_from.node_id, stop_to.node_id, trips.shape_id
);
CREATE INDEX pt_section_stop_times_trip_id_idx ON %(temp_schema).pt_section_stop_times(trip_id);
CREATE INDEX pt_section_stop_times_node_from_id_idx ON %(temp_schema).pt_section_stop_times(node_from_id);
CREATE INDEX pt_section_stop_times_node_to_id_idx ON %(temp_schema).pt_section_stop_times(node_to_id);

CREATE INDEX frequencies_trip_id_idx ON %(temp_schema).frequencies(trip_id);

CREATE TABLE %(temp_schema).pt_section
(
    id bigserial, 
    node_from_id integer, 
    node_to_id integer,
    shape_id character varying,
    diffusion smallint,
    crossability smallint,
    sub_arcs bigint[],
    geom Geometry(LineStringZ, %(target_srid))
);
SELECT setval('%(temp_schema).pt_section_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.arc), false);

INSERT INTO %(temp_schema).pt_section( node_from_id, node_to_id, shape_id, diffusion, crossability )
SELECT DISTINCT ON (node_from_id, node_to_id) 
                     node_from_id, 
                     node_to_id, 
                     trips.shape_id, 
                     0, 
                     CASE WHEN routes.route_type = 2 THEN 3 ELSE 0 END
FROM %(temp_schema).pt_section_stop_times JOIN %(temp_schema).trips ON (trips.trip_id = pt_section_stop_times.trip_id)
                                          JOIN %(temp_schema).routes ON (routes.route_id = trips.route_id)
ORDER BY node_from_id, node_to_id;

WITH shape_line AS (
    SELECT shape_id, st_force3DZ(st_makeline(geom ORDER BY shape_pt_sequence)) as geom
    FROM %(temp_schema).shapes
    GROUP BY shape_id
)
UPDATE %(temp_schema).pt_section
SET geom = coalesce(
                    CASE 
                        WHEN least(st_linelocatepoint(shape_line.geom, node_from.geom), st_linelocatepoint(shape_line.geom, node_to.geom)) = greatest(st_linelocatepoint(shape_line.geom, node_from.geom), st_linelocatepoint(shape_line.geom, node_to.geom))
                        THEN NULL -- Line is reduced to a point 
                        ELSE st_addpoint(
                                            st_addpoint(st_linesubstring (
                                                                              shape_line.geom, 
                                                                              least(st_linelocatepoint(shape_line.geom, node_from.geom), st_linelocatepoint(shape_line.geom, node_to.geom)), 
                                                                              greatest(st_linelocatepoint(shape_line.geom, node_from.geom), st_linelocatepoint(shape_line.geom, node_to.geom))
                                                                         ), 
                                            node_to.geom), 
                                            node_from.geom, 
                                            0
                                        )
                    END, 
                    st_force3DZ(st_setsrid(st_makeline(node_from.geom, node_to.geom), %(target_srid)))
                   )::Geometry(LinestringZ, %(target_srid))
FROM shape_line, tempus_networks.node AS node_from, tempus_networks.pt_stop AS pt_stop_to, tempus_networks.node AS node_to
WHERE (shape_line.shape_id = pt_section.shape_id) AND (node_from_id = node_from.id) AND (pt_stop_to.node_id = node_to.id) AND (node_to_id = node_to.id); 

INSERT INTO tempus_networks.arc(id, sources_id, node_from_id, node_to_id, length, type, diffusion, crossability, geom)
SELECT id, 
       ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')], 
       node_from_id, 
       node_to_id, 
       st_length(geom), 
       2,
       diffusion, 
       crossability, 
       geom       
FROM %(temp_schema).pt_section;

INSERT INTO tempus_networks.pt_section(arc_id, sub_arcs)
SELECT id, 
       sub_arcs  
FROM %(temp_schema).pt_section;

DROP TABLE IF EXISTS %(temp_schema).trip_idmap; 
CREATE TABLE %(temp_schema).trip_idmap 
(
    id serial PRIMARY KEY,
    original_id character varying, 
    route_id character varying,
    direction_id integer
);
SELECT setval('%(temp_schema).trip_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.pt_trip), false);
INSERT INTO %(temp_schema).trip_idmap( original_id, route_id, direction_id )
SELECT DISTINCT trip_id, route_id, direction_id FROM %(temp_schema).trips ORDER BY route_id, direction_id;
CREATE INDEX trip_idmap_original_id_idx ON %(temp_schema).trip_idmap(original_id);
CREATE INDEX trip_idmap_id_idx ON %(temp_schema).trip_idmap(id);

SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arc');

WITH shape_line AS (
    SELECT shape_id, st_force3DZ(st_makeline(geom ORDER BY shape_pt_sequence)) as geom
    FROM %(temp_schema).shapes
    GROUP BY shape_id
)
INSERT INTO tempus_networks.pt_trip( id, original_id, arcs_id, pt_route_id, days_period_id, pt_trip_types_id, traffic_rules, name, direction, direction_headsign, geom_arc, geom_nodes )
(
    WITH pt_trip AS (	
		SELECT DISTINCT 
               trip_idmap.id as id, 
			   trips.trip_id as original_id, 
			   route_idmap.id as pt_route_id, 
			   days_period.id as days_period_id,
			   ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Walking')]::smallint[] 
			   || CASE WHEN wheelchair_accessible=1 THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Wheelchair')]::smallint[] ELSE ARRAY[]::smallint[] END
			   || CASE WHEN bikes_allowed=1 THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Bicycle')]::smallint[] ELSE ARRAY[]::smallint[] END as traffic_rules, 
			   trips.trip_short_name as name,
			   trips.direction_id as direction, 
			   trips.trip_headsign as direction_headsign, 
               shape_line.geom as geom_arc
		FROM %(temp_schema).trips JOIN %(temp_schema).trip_idmap ON (trip_idmap.original_id = trips.trip_id)
                                  JOIN %(temp_schema).routes ON (routes.route_id = trips.route_id)
                                  JOIN %(temp_schema).route_idmap ON (route_idmap.original_id = routes.route_id)
                                  JOIN %(temp_schema).days_period ON (days_period.service_id = trips.service_id)
                                  JOIN shape_line ON (shape_line.shape_id = trips.shape_id)
	), additional_fields AS (
		SELECT pt_section_stop_times.trip_id as original_id,
		       array_agg(arc.id order by pt_section_stop_times.departure_time, pt_section_stop_times.arrival_time) as arcs_id,
	           array_agg(DISTINCT pt_trip_type.id) as pt_trip_types_id, 
		       st_collect(DISTINCT node.geom) as geom_nodes
        FROM %(temp_schema).pt_section_stop_times JOIN tempus_networks.arc ON (arc.node_from_id = pt_section_stop_times.node_from_id AND arc.node_to_id = pt_section_stop_times.node_to_id)
                                                  JOIN tempus_networks.node ON (node.id = arc.node_from_id or node.id = arc.node_to_id)
                                                  JOIN %(temp_schema).trips ON (trips.trip_id = pt_section_stop_times.trip_id)
                                                  JOIN %(temp_schema).routes ON (routes.route_id = trips.route_id)
                                                  JOIN pgtempus.pt_trip_type ON (routes.route_type = ANY(pt_trip_type.gtfs_codes))
		GROUP BY pt_section_stop_times.trip_id
	)
	SELECT pt_trip.id, 
	       pt_trip.original_id, 
		   additional_fields.arcs_id, 
		   pt_trip.pt_route_id, 
		   pt_trip.days_period_id, 
		   additional_fields.pt_trip_types_id, 
		   pt_trip.traffic_rules, 
		   pt_trip.name, 
		   pt_trip.direction, 
		   pt_trip.direction_headsign, 
		   pt_trip.geom_arc, 
		   additional_fields.geom_nodes
	FROM pt_trip JOIN additional_fields ON (pt_trip.original_id = additional_fields.original_id)
    ORDER BY pt_trip.id
);

SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.road_section_attributes');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arcs_sequence_time');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arcs_sequence_toll');

--  New nodes are linked to the road network
DO
$$
BEGIN
    IF ( '%(link)'::character varying = 'True'::character varying ) THEN
        CREATE INDEX ON tempus_networks.arc USING gist( geography(st_transform(geom, 4326)) ); 
        PERFORM pgtempus.tempus_attach_node_to_a_road_section( node.id, (SELECT array_agg(id) FROM pgtempus.source WHERE type = 1), %(max_dist), ARRAY[1,4]::smallint[] )
        FROM %(temp_schema).node;
        DROP INDEX tempus_networks.arc_geography_idx; 
        PERFORM setval('tempus_networks.arc_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.arc), false); 
    END IF;
END
$$;

-- Only trips which are not described by frequencies are first included
INSERT INTO tempus_networks.pt_section_stop_times( arc_id, pt_trip_id, time_from, time_to, interpolated_time_from, interpolated_time_to, direction_label, dropoff_type, pickup_type )
(
    SELECT arc.id, 
           trip_idmap.id, 
           pt_section_stop_times.departure_time, 
           pt_section_stop_times.arrival_time, 
           False, 
           False, 
           direction_label, 
           drop_off_type,
           pickup_type
    FROM %(temp_schema).pt_section_stop_times JOIN %(temp_schema).trip_idmap ON (trip_idmap.original_id = pt_section_stop_times.trip_id)
                                              JOIN tempus_networks.arc ON (arc.node_from_id = pt_section_stop_times.node_from_id AND arc.node_to_id = pt_section_stop_times.node_to_id)
                                              LEFT JOIN %(temp_schema).frequencies ON (trip_idmap.original_id = frequencies.trip_id) 
    WHERE frequencies.start_time IS NULL 
); 

-- Trips described by frequencies are then added (if any)
INSERT INTO tempus_networks.pt_section_stop_times( arc_id, pt_trip_id, time_from, time_to, interpolated_time_from, interpolated_time_to )
(
        SELECT arc.id, 
               trip_idmap.id, 
               pt_section_stop_times.departure_time, 
               pt_section_stop_times.arrival_time, 
               False, 
               False
    FROM %(temp_schema).pt_section_stop_times JOIN %(temp_schema).trip_idmap ON (trip_idmap.original_id = pt_section_stop_times.trip_id)
                                              JOIN tempus_networks.arc ON (arc.node_from_id = pt_section_stop_times.node_from_id AND arc.node_to_id = pt_section_stop_times.node_to_id)
                                              JOIN %(temp_schema).frequencies ON (trip_idmap.original_id = frequencies.trip_id)
);

WITH q AS (
    SELECT node.id, max(    CASE WHEN pt_trip_type.gtfs_codes = '{0}' THEN '00:05:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{1}' THEN '00:05:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{2}' THEN '00:20:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{3}' THEN '00:05:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{4}' THEN '00:20:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{5}' THEN '00:05:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{6}' THEN '00:05:00'::interval
                                 WHEN pt_trip_type.gtfs_codes = '{7}' THEN '00:05:00'::interval
                   		    END ) as security_time
    FROM tempus_networks.pt_section_stop_times t JOIN tempus_networks.arc ON (arc.id = t.arc_id)
                                                 JOIN tempus_networks.node ON (arc.node_from_id = node.id)
                                                 JOIN tempus_networks.pt_trip ON (pt_trip.id = t.pt_trip_id)
                                                 JOIN pgtempus.pt_trip_type ON (pt_trip_type.id = ANY(pt_trip.pt_trip_types_id))
    WHERE pt_trip_type.gtfs_codes is not null
	GROUP BY node.id 
)
UPDATE tempus_networks.pt_stop
SET security_time = q.security_time
FROM q
WHERE q.id = pt_stop.node_id;



