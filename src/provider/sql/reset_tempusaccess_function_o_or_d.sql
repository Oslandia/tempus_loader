
DROP FUNCTION IF EXISTS pgtempus.tempus_create_o_or_d_indicator_layer(
                                                                        bigint[], 
                                                                        real,
                                                                        smallint,
                                                                        boolean,
                                                                        smallint[], 
                                                                        smallint[], 
                                                                        smallint[], 
                                                                        smallint[], 
                                                                        date[], 
                                                                        time[], 
                                                                        smallint, 
                                                                        smallint, 
                                                                        smallint, 
                                                                        boolean, 
                                                                        real,
                                                                        real, 
                                                                        real,
                                                                        real,
                                                                        real,
                                                                        real,
                                                                        real,
                                                                        real,
                                                                        interval, 
                                                                        interval, 
                                                                        integer, 
                                                                        integer, 
                                                                        boolean, 
                                                                        character varying  
                                                                     ); 
CREATE OR REPLACE FUNCTION pgtempus.tempus_create_o_or_d_indicator_layer(
                                                                            p_root_nodes bigint[], 
                                                                            p_max_cost real,
                                                                            p_root_mode smallint,
                                                                            p_time_constraint_dep boolean,
                                                                            p_sources_id smallint[], 
                                                                            p_transport_modes smallint[], 
                                                                            p_pt_trip_types smallint[], 
                                                                            p_pt_agencies_id smallint[], 
                                                                            p_days date[] DEFAULT ARRAY['2000-01-01'::date], 
                                                                            p_times time[] DEFAULT ARRAY['00:00:00'::time], 
                                                                            p_day_ag smallint DEFAULT 1, 
                                                                            p_time_ag smallint DEFAULT 1, 
                                                                            p_node_ag smallint DEFAULT 1,
                                                                            p_debug boolean DEFAULT False, 
                                                                            p_walking_speed real DEFAULT 3.6,
                                                                            p_bicycle_speed real DEFAULT 11, 
                                                                            p_pt_time_weight real DEFAULT 1,
                                                                            p_waiting_time_weight real DEFAULT 1,
                                                                            p_indiv_mode_time_weight real DEFAULT 1,
                                                                            p_fare_weight real DEFAULT 0,
                                                                            p_pt_transfer_weight real DEFAULT 0,
                                                                            p_mode_change_weight real DEFAULT 0,
                                                                            p_max_walking_time interval DEFAULT NULL, 
                                                                            p_max_bicycle_time interval DEFAULT NULL, 
                                                                            p_max_pt_transfers integer DEFAULT NULL, 
                                                                            p_max_mode_changes integer DEFAULT NULL, 
                                                                            p_pt_security_times boolean DEFAULT False, 
                                                                            p_table_name_isochron character varying DEFAULT 'isochron'                        
                                                                        ) 
RETURNS void AS
$BODY$

DECLARE
    s1 character varying;
    s character varying;
    r record;
    q record; 
    query_id integer;
    time_ag_str character varying; 
    day_ag_str character varying;
    node_ag_str character varying;
    pref character varying; 
    source_filter1 character varying;
    source_filter2 character varying;
    traffic_rules_filter character varying;
    traffic_rules_filter2 character varying;
    traffic_rules_filter3 character varying; 
    toll_rules_filter character varying;
    transport_mode_filter character varying;   
    pt_trip_types_filter character varying;
    pt_trip_types_filter2 character varying;
    pt_agencies_filter character varying;
    pt_agencies_filter2 character varying;
    error boolean;

BEGIN
    pref = '';
    IF (p_time_constraint_dep) THEN pref = 'o';
    ELSE pref = 'd';
    END IF;
    
    s = $$
        -- Path trees table
        DROP TABLE IF EXISTS tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees; 
        CREATE TABLE tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees
        (
            id bigserial PRIMARY KEY,
            query_id integer, 
            root_node bigint,
            root_mode smallint,
            root_timestamp timestamp, 
            obj_id bigint, 
            pred_obj_id bigint, 
            arc_id bigint, 
            pred_node_id bigint, 
            node_id bigint, 
            transport_mode_id smallint, 
            automaton_state integer, 
            pred_cost real, 
            cost real, 
            pt_trip_id bigint, 
            waiting_time time, 
            departure_time time, 
            arrival_time time, 
            total_bicycle_time time, 
            total_walking_time time, 
            total_mode_changes smallint, 
            total_pt_transfers smallint, 
            total_fare real, 
            in_paths boolean, 
            length real,
            crossability character varying, 
            diffusion character varying, 
            symbol_color real,
            geom Geometry( LinestringZ, 2154 ), 
            UNIQUE(query_id, arc_id, transport_mode_id, pt_trip_id, automaton_state)
        ); 
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.query_id IS 'Unique ID of shortest paths query';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.root_node IS 'ID of the root node';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.root_mode IS 'ID of the root transport mode';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.root_timestamp IS 'Theoretical arrival time';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.obj_id IS 'Current object (triplet node, mode, state) ID';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.pred_obj_id IS 'Predecessor object (triplet node, mode, state) ID in the shortest path tree'; 
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.arc_id IS 'Arc ID';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.pred_node_id IS 'Predecessor node in the path';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.node_id IS 'Current node';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.transport_mode_id IS 'Transport mode used on the current arc';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.automaton_state IS 'State of the automaton associated to the labeled object (for debugging only).';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.cost IS 'Value of the cost from the root node to the current node';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.pred_cost IS 'Value of the cost from the root node to the predecessor node';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.pt_trip_id IS 'PT trip ID used on the current arc';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.waiting_time IS 'Wait time at the origin of the arc, before departure';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.departure_time IS 'Departure time of the origin of the arc';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.arrival_time IS 'Arrival time at the destination of the arc'; 
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.total_bicycle_time IS 'Cumulated bicycle time since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.total_walking_time IS 'Cumulated walking time since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.total_mode_changes IS 'Number of mode changes since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.total_pt_transfers IS 'Number of PT transfers since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees.total_fare IS 'Cumulated fare since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        
        DROP TABLE IF EXISTS tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time; 
        CREATE TABLE tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time
        (
            id bigserial PRIMARY KEY,
            query_id integer, 
            root_node bigint,
            root_mode smallint,
            root_timestamp timestamp, 
            arc_id bigint, 
            pred_node_id bigint, 
            node_id bigint, 
            pred_cost real, 
            cost real, 
            departure_time time, 
            arrival_time time, 
            total_bicycle_time time, 
            total_walking_time time, 
            total_mode_changes smallint, 
            total_pt_transfers smallint, 
            total_fare real, 
            length real,
            crossability character varying, 
            diffusion character varying, 
            symbol_color real,
            geom Geometry( LinestringZ, 2154 ), 
            UNIQUE(query_id, arc_id)
        ); 
        
        DROP TABLE IF EXISTS tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$; 
        CREATE TABLE tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$
        (
            arc_id bigint PRIMARY KEY, 
            pred_node_id bigint, 
            node_id bigint, 
            pred_cost real, 
            cost real, 
            departure_time time, 
            arrival_time time, 
            total_bicycle_time time, 
            total_walking_time time, 
            total_mode_changes smallint, 
            total_pt_transfers smallint, 
            total_fare real, 
            length real,
            crossability character varying, 
            diffusion character varying, 
            count_nodes integer,
            symbol_color real,
            geom Geometry( LinestringZ, 2154 )
        );
        
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees';
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time';
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || pref || $$_$$ || p_table_name_isochron || $$';
    $$;
    EXECUTE(s);
    
    IF (p_time_constraint_dep) THEN
        s = $$
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees', 5, 1 );
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time', 5, 2 ); 
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$', 5, 3 ); 
        $$;
    ELSE
        s = $$
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees', 5, 4 );
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time', 5, 5 ); 
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ( '$$ || pref || $$_$$ || p_table_name_isochron || $$', 5, 6 ); 
        $$;
    END IF;
    EXECUTE(s);
    
    -- Define filters
    source_filter1 = $$(source_id = ANY('$$ || p_sources_id::character varying || $$'::smallint[]))$$;
    source_filter2 = $$('$$ || p_sources_id::character varying || $$'::smallint[] @> sources_id)$$;
    traffic_rules_filter = $$(traffic_rules IS NULL OR traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    traffic_rules_filter2 = $$(arc_complete.traffic_rules IS NULL OR arc_complete.traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    traffic_rules_filter3 = $$(traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    toll_rules_filter = $$(toll_rules IS NULL OR toll_rules && (SELECT array_agg(toll_rule) FROM pgtempus.transport_mode WHERE toll_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    transport_mode_filter = $$(transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[]))$$;
    pt_trip_types_filter = $$((pt_trip_types_id IS NULL) OR (pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$'::smallint[]))$$;
    pt_trip_types_filter2 = $$((t.pt_trip_types_id IS NULL) OR (t.pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$'::smallint[]))$$;
    pt_agencies_filter = $$((pt_agencies_id IS NULL) OR (pt_agencies_id && '$$ || p_pt_agencies_id::character varying || $$'::smallint[]))$$;
    pt_agencies_filter2 = $$(t.pt_agency_id = ANY('$$ || p_pt_agencies_id::character varying || $$'::smallint[]))$$;
    
    -- Check if all origins or destinations exist
    s=$$
    (
        SELECT id
        FROM tempus_networks.node_complete
        WHERE id = ANY('$$ || p_root_nodes::character varying || $$')
    )
    EXCEPT
    (
        SELECT id
        FROM tempus_networks.node_complete
        WHERE $$ || source_filter1 || $$ 
          AND $$ || traffic_rules_filter || $$
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP RAISE WARNING 'Node %, referenced in origins or destinations, is not defined', r.id;
    END LOOP;

    -- Check if initial and final modes exist
    s=$$
    (
        SELECT id
        FROM pgtempus.transport_mode
        WHERE id = $$ || p_root_mode || $$
    )
    EXCEPT
    (
        SELECT id
        FROM pgtempus.transport_mode
        WHERE $$ || transport_mode_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    
    FOR r IN EXECUTE(s)
    LOOP RAISE EXCEPTION 'Transport mode % is not defined', r.id;
    END LOOP;
    -- Check if all arcs have their corresponding nodes
    s=$$
    ( 
        SELECT node_from_id as id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
        UNION
        SELECT node_to_id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    ) 
    EXCEPT
    ( 
        SELECT id
        FROM tempus_networks.node_complete
        WHERE $$ || source_filter1 || $$ 
          AND $$ || traffic_rules_filter || $$ 
    ) 
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP RAISE WARNING 'Node %, referenced from the arcs table, is not defined', r.id;
    END LOOP;
    
    -- Check if all road sections costs have their corresponding arcs and transport modes
    s=$$
    (
        -- Walking and bicycle costs
        SELECT arc_complete.id as arc_id,
               transport_mode.id::integer as transport_mode_id
        FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
        WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
          AND transport_mode.category = ANY(ARRAY['Walking'::pgtempus.transport_mode_category, 'Bicycle'::pgtempus.transport_mode_category])
          AND arc_complete.type = 1
          AND $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
        UNION
        -- Car costs
        SELECT arc_complete.id as arc_id,
               transport_mode.id::integer as transport_mode_id
        FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                          LEFT JOIN tempus_networks.road_section_speed ON (road_section_speed.arc_id = arc_complete.id AND road_section_speed.speed_rule = transport_mode.speed_rule)
        WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
          AND transport_mode.category = 'Car'::pgtempus.transport_mode_category
          AND arc_complete.type = 1
          AND $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    )
    EXCEPT
    (
        SELECT arc_complete.id, transport_mode.id
        FROM tempus_networks.arc_complete CROSS JOIN pgtempus.transport_mode
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
          AND $$ || transport_mode_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP
        RAISE EXCEPTION 'The pair (Arc, Transport mode) = (%, %), referenced from the road sections costs table, is not defined', r.arc_id, r.transport_mode_id;
    END LOOP;
    
    -- Check if all arcs sequences have their corresponding arcs 
    s=$$
    (
        SELECT unnest(penalized_path.arcs_id) as arc_id
        FROM tempus_networks.penalized_path
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || toll_rules_filter || $$ 
    )
    EXCEPT
    (
        SELECT id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP
        RAISE WARNING 'The arc %, referenced from the arcs sequences table, is not defined', r.arc_id;
    END LOOP;
    
    IF (p_debug) THEN SET pgtempus.debug = on;
    ELSE 
    SET pgtempus.debug = off;
    END IF;
    
    RAISE NOTICE 'Graph building...';    
    PERFORM pgtempus.tempus_build_multimodal_graph(
                                                    'g', 
                                                    $$
                                                      SELECT id, 
                                                             name::text, 
                                                             category::text, 
                                                             traffic_rule, 
                                                             toll_rule, 
                                                             vehicle_parking_rule
                                                      FROM pgtempus.transport_mode
                                                      WHERE $$ || transport_mode_filter, 
                                                    $$
                                                      SELECT id, 
                                                             st_x(geom)::real as x,
                                                             st_y(geom)::real as y, 
                                                             st_z(geom)::real as z
                                                      FROM tempus_networks.node_complete
                                                      WHERE arc_types != ARRAY[3]::smallint[] 
                                                        AND $$ || source_filter1 || $$ 
                                                        AND $$ || traffic_rules_filter,
                                                    $$
                                                      SELECT id, 
                                                             node_from_id, 
                                                             node_to_id, 
                                                             type, 
                                                             traffic_rules
                                                      FROM tempus_networks.arc_complete
                                                      WHERE type = ANY(ARRAY[1,2]) 
                                                        AND $$ || source_filter2 || $$ 
                                                        AND $$ || traffic_rules_filter || $$ 
                                                        AND $$ || pt_trip_types_filter || $$ 
                                                        AND $$ || pt_agencies_filter
                                                  );
    RAISE NOTICE 'Assigning road costs...';
    PERFORM pgtempus.tempus_set_static_road_section_costs(
                                                            'g',
                                                            $$
                                                                -- Walking costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / $$ || p_walking_speed::character varying || $$ * 3600 
                                                                       END::integer as travel_time, 
                                                                       0::real as fare
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Walking'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter || $$ 
                                                                UNION
                                                                -- Bicycle costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / $$ || p_bicycle_speed::character varying || $$ * 3600 
                                                                       END::integer as travel_time, 
                                                                       0::real as fare
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Bicycle'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter || $$ 
                                                                UNION
                                                                -- Car costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL OR road_section_speed.value = 0 THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / COALESCE(road_section_speed.value, 30) * 3600 
                                                                       END::integer as duration, 
                                                                       0::real as additional_cost
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON ( transport_mode.traffic_rule = ANY(arc_complete.traffic_rules) )
                                                                                                  LEFT JOIN tempus_networks.road_section_speed ON (road_section_speed.arc_id = arc_complete.id AND road_section_speed.speed_rule = transport_mode.speed_rule)
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Car'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter
                                                         ); 
    RAISE NOTICE 'Assigning arcs sequence time penalties...'; 
    PERFORM pgtempus.tempus_set_arcs_sequence_costs(
                                                        'g',
                                                        $$
                                                            SELECT arcs_id,
                                                                   traffic_rules, 
                                                                   time_value, 
                                                                   toll_rules,
                                                                   toll_value
                                                            FROM tempus_networks.penalized_path JOIN pgtempus.transport_mode ON ( transport_mode.traffic_rule = ANY(penalized_path.traffic_rules) )
                                                            WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                              AND $$ || source_filter2 || $$ 
                                                              AND $$ || traffic_rules_filter3 || $$ 
                                                              AND $$ || toll_rules_filter
                                                   ); 
    RAISE NOTICE 'Assigning parking costs...';  
    PERFORM pgtempus.tempus_set_parking_costs(
                                                'g', 
                                                $$
                                                    SELECT v.node_id, 
                                                           v.vehicle_parking_rule, 
                                                           extract(epoch from v.leaving_time)::integer as leaving_time, 
                                                           v.leaving_fare, 
                                                           extract(epoch from v.taking_time)::integer as taking_time, 
                                                           v.taking_fare
                                                    FROM tempus_networks.vehicle_parking_cost v JOIN pgtempus.transport_mode ON (transport_mode.vehicle_parking_rule = v.vehicle_parking_rule)
                                                                                                JOIN tempus_networks.node_complete  ON (node_complete.id = v.node_id)
                                                    WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                      AND node_complete.arc_types != ARRAY[3]::smallint[] 
                                                      AND $$ || source_filter1 || $$ 
                                                      AND $$ || traffic_rules_filter
                                             );
    RAISE NOTICE 'Assigning vehicles positions...';
    PERFORM pgtempus.tempus_set_vehicles_positions(
                                                        'g', 
                                                        $$
                                                            SELECT v.node_id, v.transport_mode_id
                                                            FROM pgtempus.vehicle v JOIN tempus_networks.node_complete ON ( v.node_id = node_complete.id )
                                                            WHERE transport_mode_id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                              AND node_complete.arc_types != ARRAY[3]::smallint[] 
                                                              AND $$ || source_filter1 || $$ 
                                                              AND $$ || traffic_rules_filter
                                                  );  
    
    IF (p_pt_security_times) THEN 
        RAISE NOTICE 'Assigning PT boarding security times...';
        PERFORM pgtempus.tempus_set_pt_boarding_security_times(
                                                                    'g', 
                                                                    $$
                                                                        SELECT pt_stop.node_id, 
                                                                               extract(epoch from security_time)::smallint as pt_boarding_security_time
                                                                        FROM tempus_networks.pt_stop JOIN tempus_networks.node_complete ON (pt_stop.node_id = node_complete.id)
                                                                        WHERE node_complete.arc_types != ARRAY[3]::smallint[] 
                                                                          AND $$ || source_filter1 || $$ 
                                                                          AND $$ || traffic_rules_filter || $$ 
                                                                          AND $$ || pt_trip_types_filter || $$ 
                                                                          AND $$ || pt_agencies_filter 
                                                              );
    END IF;

    FOR index_day IN 1..array_upper(p_days, 1)
    LOOP        
        RAISE NOTICE 'Assigning PT timetables (different for each day)...';
        PERFORM pgtempus.tempus_set_pt_section_timetable (
                                                            'g', 
                                                            $$
                                                                SELECT t.id as arc_id, 
                                                                       t.time_from as departure, 
                                                                       t.time_to as arrival, 
                                                                       t.pt_trip_id, 
                                                                       t.traffic_rules
                                                                FROM tempus_networks.pt_section_trip_stop_times t JOIN tempus_networks.arc_complete ON (arc_complete.id = t.id)
                                                                WHERE day = '$$ || p_days[index_day]::character varying || $$'::date 
                                                                  AND $$ || source_filter2 || $$ 
                                                                  AND $$ || traffic_rules_filter2 || $$ 
                                                                  AND $$ || pt_trip_types_filter2 || $$ 
                                                                  AND $$ || pt_agencies_filter2|| $$ 
                                                                ORDER BY t.id, time_from, time_to
                                                            $$, 
                                                            p_days[index_day]::timestamp
                                                         );
        FOR index_time IN 1..array_upper(p_times, 1)
        LOOP
            FOR index_root IN 1..array_upper(p_root_nodes, 1)
            LOOP
                -- Get query number
                s = $$ SELECT coalesce( max(query_id)+1, 1 ) as id FROM tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees; $$; 
                FOR q IN EXECUTE(s)
                LOOP 
                    query_id = q.id; 
                END LOOP;
                
                
                -- Check if root exists
                error = False;
                s = $$
                    SELECT count(id)
                    FROM tempus_networks.node_complete
                    WHERE $$ || source_filter1 || $$ 
                      AND $$ || traffic_rules_filter || $$
                      AND id = $$ || p_root_nodes[index_root];
                FOR r IN EXECUTE(s)
                LOOP 
                    IF (r.count = 0) THEN
                        RAISE WARNING 'Node % referenced in % is not defined. Corresponding query will be skipped.', p_root_nodes[index_root], CASE WHEN p_time_constraint_dep THEN 'origins' ELSE 'destinations' END;
                        error = True;
                    END IF;
                END LOOP;
                CONTINUE WHEN (error = True); 
                
                IF (p_time_constraint_dep is True) THEN                
                    RAISE NOTICE 'Origin = %', p_root_nodes[index_root];
                    s1 = $$
                            INSERT INTO tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees (
                                                                                                                        query_id, 
                                                                                                                        root_node,
                                                                                                                        root_mode, 
                                                                                                                        root_timestamp, 
                                                                                                                        obj_id, 
                                                                                                                        pred_obj_id, 
                                                                                                                        arc_id, 
                                                                                                                        pred_node_id, 
                                                                                                                        node_id,
                                                                                                                        transport_mode_id, 
                                                                                                                        automaton_state,
                                                                                                                        pred_cost,
                                                                                                                        cost, 
                                                                                                                        pt_trip_id, 
                                                                                                                        waiting_time, 
                                                                                                                        departure_time, 
                                                                                                                        arrival_time, 
                                                                                                                        total_bicycle_time,
                                                                                                                        total_walking_time,
                                                                                                                        total_mode_changes, 
                                                                                                                        total_pt_transfers,
                                                                                                                        total_fare, 
                                                                                                                        in_paths, 
                                                                                                                        length,
                                                                                                                        crossability, 
                                                                                                                        diffusion, 
                                                                                                                        geom
                                                                                                                    )
                                SELECT $$ || query_id || $$, 
                                       $$ || p_root_nodes[index_root]::character varying || $$::bigint, 
                                       $$ || p_root_mode::character varying || $$::smallint, 
                                       '$$ || (p_days[index_day] + p_times[index_time])::character varying || $$'::timestamp, 
                                       p.obj_id, 
                                       p.pred_obj_id, 
                                       p.arc_id, 
                                       p.pred_node_id, 
                                       p.node_id, 
                                       p.transport_mode_id::smallint, 
                                       p.automaton_state, 
                                       p.pred_cost, 
                                       p.cost, 
                                       p.pt_trip_id, 
                                       p.waiting_time, 
                                       p.departure_time, 
                                       p.arrival_time, 
                                       p.total_bicycle_time::time, 
                                       p.total_walking_time::time, 
                                       p.total_mode_changes::smallint, 
                                       p.total_pt_transfers::smallint, 
                                       p.total_fare::real, 
                                       p.in_paths::boolean, 
                                       arc.length::real, 
                                       arc.crossability::character varying, 
                                       arc.diffusion::character varying, 
                                       arc.geom
                                FROM pgtempus.tempus_one_to_many_paths(
                                                                        graph_id := 'g',
                                                                        algo_name := 'multimodal_ls'::text, 
                                                                        orig_node := $$ || p_root_nodes[index_root]::character varying || $$::bigint, 
                                                                        dest_nodes := null::bigint[], 
                                                                        orig_transport_mode := $$ || p_root_mode || $$::smallint, 
                                                                        dest_transport_mode := null::smallint, 
                                                                        max_cost := $$ || p_max_cost || $$::real,
                                                                        allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                                        departure_time := '$$ || (p_days[index_day] + p_times[index_time])::character varying || $$',
                                                                        pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                                        waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                                        indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                                        fare_weight := $$ || p_fare_weight || $$::real,
                                                                        pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                                        mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                                        max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_walking_time::character varying || $$'$$ END || $$::interval, 
                                                                        max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_bicycle_time::character varying || $$'$$ END || $$::interval, 
                                                                        max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE $$'$$ || p_max_pt_transfers::character varying || $$'$$ END || $$::smallint, 
                                                                        max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE $$'$$ || p_max_mode_changes::character varying || $$'$$ END || $$::smallint
                                                                      ) p LEFT JOIN tempus_networks.arc ON ( arc.id = p.arc_id ) 
                                ORDER BY p.obj_id; 
                    $$;
                    --RAISE NOTICE '%', s1;
                    EXECUTE(s1);
                ELSE                      
                    RAISE NOTICE 'Destination = %', p_root_nodes[index_root];
                    s1=$$
                            INSERT INTO tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees (
                                                                                                                       query_id, 
                                                                                                                       root_node,
                                                                                                                       root_mode, 
                                                                                                                       root_timestamp, 
                                                                                                                       obj_id, 
                                                                                                                       pred_obj_id, 
                                                                                                                       arc_id, 
                                                                                                                       pred_node_id, 
                                                                                                                       node_id,
                                                                                                                       transport_mode_id, 
                                                                                                                       automaton_state,
                                                                                                                       pred_cost,
                                                                                                                       cost, 
                                                                                                                       pt_trip_id, 
                                                                                                                       waiting_time, 
                                                                                                                       departure_time, 
                                                                                                                       arrival_time, 
                                                                                                                       total_bicycle_time,
                                                                                                                       total_walking_time,
                                                                                                                       total_mode_changes, 
                                                                                                                       total_pt_transfers,
                                                                                                                       total_fare, 
                                                                                                                       in_paths, 
                                                                                                                       length, 
                                                                                                                       crossability, 
                                                                                                                       diffusion, 
                                                                                                                       geom
                                                                                                                    ) 
                        SELECT $$ || query_id || $$, 
                               $$ || p_root_nodes[index_root] || $$::bigint, 
                               $$ || p_root_mode || $$::smallint, 
                               '$$ || (p_days[index_day] + p_times[index_time])::character varying || $$', 
                               p.obj_id,
                               p.pred_obj_id,
                               p.arc_id, 
                               p.pred_node_id, 
                               p.node_id, 
                               p.transport_mode_id::smallint,  
                               p.automaton_state,
                               p.pred_cost,
                               p.cost,
                               p.pt_trip_id, 
                               p.waiting_time, 
                               p.departure_time, 
                               p.arrival_time, 
                               p.total_bicycle_time::time, 
                               p.total_walking_time::time,
                               p.total_mode_changes::smallint, 
                               p.total_pt_transfers::smallint, 
                               p.total_fare::real, 
                               p.in_paths::boolean, 
                               arc.length, 
                               arc.crossability::character varying, 
                               arc.diffusion::character varying, 
                               arc.geom 
                        FROM pgtempus.tempus_many_to_one_paths(
                                                                graph_id := 'g',
                                                                algo_name := 'multimodal_ls'::text, 
                                                                dest_node := $$ || p_root_nodes[index_root]::character varying || $$, 
                                                                orig_nodes := null::bigint[], 
                                                                dest_transport_mode := $$ || p_root_mode || $$::smallint, 
                                                                orig_transport_mode := null::smallint, 
                                                                max_cost := $$ || p_max_cost || $$::real,
                                                                allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                                arrival_time := '$$ || (p_days[index_day] + p_times[index_time])::character varying || $$',
                                                                pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                                waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                                indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                                fare_weight := $$ || p_fare_weight || $$::real,
                                                                pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                                mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                                max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE p_max_walking_time::character varying END || $$::interval, 
                                                                max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE p_max_bicycle_time::character varying END || $$::interval, 
                                                                max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE p_max_pt_transfers::character varying END || $$::smallint, 
                                                                max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE p_max_mode_changes::character varying END || $$::smallint
                                                              ) p LEFT JOIN tempus_networks.arc ON ( arc.id = p.arc_id )  
                        ORDER BY p.obj_id;  
                    $$;
                    EXECUTE(s1);
                END IF; 
            END LOOP;
        END LOOP;
    END LOOP; 
    
    SELECT INTO time_ag_str 
                func_name 
    FROM pgtempus.agregate 
    WHERE id = p_time_ag; 
    
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate 
    WHERE id = p_day_ag; 
    
    SELECT INTO node_ag_str 
                func_name 
    FROM pgtempus.agregate 
    WHERE id = p_node_ag;     
    
    s = $$
        INSERT INTO tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time (query_id, root_node, root_mode, root_timestamp, arc_id, pred_node_id, node_id, pred_cost, cost, departure_time, arrival_time, total_bicycle_time, total_walking_time, total_mode_changes, total_pt_transfers, total_fare, diffusion, crossability, length, geom)
        SELECT DISTINCT ON ( query_id, root_node, root_mode, root_timestamp, arc_id, pred_node_id, node_id )
               query_id, root_node, root_mode, root_timestamp, arc_id, pred_node_id, node_id, pred_cost, cost, departure_time, arrival_time, total_bicycle_time, total_walking_time, total_mode_changes, total_pt_transfers, total_fare, diffusion, crossability, length, geom
        FROM tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_paths_trees
        ORDER BY query_id, root_node, root_mode, root_timestamp, arc_id, pred_node_id, node_id, cost ASC;    
        
        INSERT INTO tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$ (arc_id, pred_node_id, node_id, pred_cost, cost, departure_time, arrival_time, total_bicycle_time, total_walking_time, total_mode_changes, total_pt_transfers, total_fare, length, crossability, diffusion, count_nodes, geom)
        (
            WITH isochron_root AS (
                WITH isochron_root_day AS (
                    SELECT root_node, 
                           root_mode,
                           root_timestamp::date as d,
                           arc_id, 
                           pred_node_id, 
                           node_id, 
                           $$ || time_ag_str || $$(pred_cost) as pred_cost, 
                           $$ || time_ag_str || $$(cost) as cost, 
                           $$ || time_ag_str || $$(departure_time) as departure_time, 
                           $$ || time_ag_str || $$(arrival_time) as arrival_time, 
                           $$ || time_ag_str || $$(total_bicycle_time) as total_bicycle_time, 
                           $$ || time_ag_str || $$(total_walking_time) as total_walking_time, 
                           $$ || time_ag_str || $$(total_mode_changes) as total_mode_changes, 
                           $$ || time_ag_str || $$(total_pt_transfers) as total_pt_transfers, 
                           $$ || time_ag_str || $$(total_fare) as total_fare, 
                           length, 
                           crossability, 
                           diffusion, 
                           geom
                    FROM tempus_results.$$ || pref || $$_$$ || p_table_name_isochron || $$_root_day_time
                    GROUP BY root_node, root_mode, root_timestamp::date, arc_id, pred_node_id, node_id, length, crossability, diffusion, geom
                )
                SELECT root_node, 
                       root_mode,
                       arc_id, 
                       pred_node_id, 
                       node_id, 
                       $$ || day_ag_str || $$(pred_cost) as pred_cost, 
                       $$ || day_ag_str || $$(cost) as cost, 
                       $$ || day_ag_str || $$(departure_time) as departure_time, 
                       $$ || day_ag_str || $$(arrival_time) as arrival_time, 
                       $$ || day_ag_str || $$(total_bicycle_time) as total_bicycle_time, 
                       $$ || day_ag_str || $$(total_walking_time) as total_walking_time, 
                       $$ || day_ag_str || $$(total_mode_changes) as total_mode_changes, 
                       $$ || day_ag_str || $$(total_pt_transfers) as total_pt_transfers, 
                       $$ || day_ag_str || $$(total_fare) as total_fare, 
                       length,
                       crossability,
                       diffusion,
                       geom
                FROM isochron_root_day
                GROUP BY root_node, root_mode, arc_id, pred_node_id, node_id, length, crossability, diffusion, geom    
            )
            SELECT arc_id, 
                   pred_node_id, 
                   node_id, 
                   $$ || node_ag_str || $$(pred_cost) as pred_cost, 
                   $$ || node_ag_str || $$(cost) as cost, 
                   $$ || node_ag_str || $$(departure_time) as departure_time, 
                   $$ || node_ag_str || $$(arrival_time) as arrival_time, 
                   $$ || node_ag_str || $$(total_bicycle_time) as total_bicycle_time, 
                   $$ || node_ag_str || $$(total_walking_time) as total_walking_time, 
                   $$ || node_ag_str || $$(total_mode_changes) as total_mode_changes, 
                   $$ || node_ag_str || $$(total_pt_transfers) as total_pt_transfers, 
                   $$ || node_ag_str || $$(total_fare) as total_fare, 
                   length,
                   crossability,
                   diffusion,
                   count(*) as count_nodes, 
                   geom
            FROM isochron_root
            WHERE arc_id is not null
            GROUP BY arc_id, pred_node_id, node_id, length, crossability, diffusion, geom
            ORDER BY arc_id, pred_node_id, node_id, length, crossability, diffusion, geom, $$ || node_ag_str || $$(cost)
        ); 
    $$; 
    
    EXECUTE(s);
        
    RETURN;
END;
$BODY$
LANGUAGE plpgsql;

