/*
        Substitutions options
        %(temp_schema): name of schema containing temporary data
        %(target_srid): SRID code to use for geographic data
*/

-- Tempus database schema building: version 2
--

CREATE EXTENSION IF NOT EXISTS postgis;
DROP SCHEMA IF EXISTS pgtempus CASCADE;
CREATE SCHEMA pgtempus;
CREATE EXTENSION IF NOT EXISTS pgtempus SCHEMA pgtempus;

DROP AGGREGATE IF EXISTS array_cat_agg(anyarray) CASCADE;
CREATE AGGREGATE array_cat_agg(anyarray) 
(
    SFUNC=array_cat,
    STYPE=anyarray
);

CREATE OR REPLACE FUNCTION array_intersect(anyarray, anyarray) RETURNS anyarray as $$
SELECT ARRAY(
    SELECT $1[i]
    FROM generate_series( array_lower($1, 1), array_upper($1, 1) ) i
    WHERE ARRAY[$1[i]] && $2
);
$$ language sql;

DROP SCHEMA IF EXISTS %(temp_schema) CASCADE;
DROP SCHEMA IF EXISTS tempus_networks CASCADE;
DROP SCHEMA IF EXISTS tempus_zoning CASCADE;
DROP SCHEMA IF EXISTS tempus_results CASCADE;

DELETE FROM public.geometry_columns 
WHERE f_table_schema='tempus_networks' 
   OR f_table_schema='tempus_zoning' 
   OR f_table_schema='tempus_results';

CREATE SCHEMA tempus_networks; 
COMMENT ON SCHEMA tempus_networks IS 'Road and public transport networks and costs data and functions';

CREATE SCHEMA tempus_zoning;
COMMENT ON SCHEMA tempus_zoning IS 'Zoning data';

CREATE SCHEMA tempus_results;
COMMENT ON SCHEMA tempus_results IS 'Paths and accessibility indic stored Tempus results';

DO
$$
BEGIN
	raise notice '==== Transport modes and categories definition ===';
END
$$;

DROP TYPE IF EXISTS pgtempus.transport_mode_category CASCADE;
CREATE TYPE pgtempus.transport_mode_category AS ENUM (
    'Walking',
    'Car',
    'Bicycle'
);

DROP TABLE IF EXISTS pgtempus.traffic_rule CASCADE;
CREATE TABLE pgtempus.traffic_rule (
    id smallserial PRIMARY KEY, 
    name character varying
);
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Walking');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Wheelchair');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Bicycle');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Car accepting toll roads');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Car refusing toll roads');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Taxi');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Carpooling');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Truck');
INSERT INTO pgtempus.traffic_rule (name) VALUES ('Coach');

DROP TABLE IF EXISTS pgtempus.speed_rule CASCADE;
CREATE TABLE pgtempus.speed_rule (
    id smallserial PRIMARY KEY, 
    name character varying
);
INSERT INTO pgtempus.speed_rule (name) VALUES ('Car');

DROP TABLE IF EXISTS pgtempus.toll_rule CASCADE;
CREATE TABLE pgtempus.toll_rule (
    id smallserial PRIMARY KEY, 
    name character varying
); 
INSERT INTO pgtempus.toll_rule (name) VALUES ('Class1');

DROP TABLE IF EXISTS pgtempus.vehicle_parking_rule CASCADE;
CREATE TABLE pgtempus.vehicle_parking_rule (
    id smallserial PRIMARY KEY, 
    name character varying
);
INSERT INTO pgtempus.vehicle_parking_rule (name) VALUES ('Private bicycle');
INSERT INTO pgtempus.vehicle_parking_rule (name) VALUES ('Private car');

DROP TABLE IF EXISTS pgtempus.pt_trip_type CASCADE;
CREATE TABLE pgtempus.pt_trip_type (
    id smallserial PRIMARY KEY,
    name character varying NOT NULL,
    comment character varying,
    gtfs_codes integer[]
);
COMMENT ON TABLE pgtempus.pt_trip_type IS 'Public transport trip type';
COMMENT ON COLUMN pgtempus.pt_trip_type.gtfs_codes IS 'List of extended route types associated to each pt_trip_type (see: https://developers.google.com/transit/gtfs/reference/extended-route-types)'; 
INSERT INTO pgtempus.pt_trip_type(id, name, comment)
VALUES(0, 'All', 'All public transport modes');

DROP TABLE IF EXISTS pgtempus.road_section_type CASCADE;
CREATE TABLE pgtempus.road_section_type (
    id smallserial PRIMARY KEY,
    name character varying
);  
COMMENT ON TABLE pgtempus.road_section_type IS 'Definition of a hierarchy / typology for road sections. Never directly read by C++. ';
INSERT INTO pgtempus.road_section_type VALUES(1, 'Virtuel');

DROP TABLE IF EXISTS pgtempus.poi_type CASCADE;
CREATE TABLE pgtempus.poi_type (
    id smallserial PRIMARY KEY,
    name character varying, 
    abstract boolean
);
COMMENT ON TABLE pgtempus.poi_type IS 'Type of point of interest. Never read by C++.';
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Public transport stop area', True);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Public transport station entrance or exit', False);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Car park', False);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Bicycle park', False);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Shared car rental point', False);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Shared bicycle rental point', False);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Zone centroid', True);
INSERT INTO pgtempus.poi_type(name, abstract)
VALUES('Other', False);

DROP TABLE IF EXISTS pgtempus.barrier_type CASCADE;
CREATE TABLE pgtempus.barrier_type (
    id smallserial PRIMARY KEY,
    name character varying
); 

DROP TABLE IF EXISTS pgtempus.transport_mode CASCADE;
CREATE TABLE pgtempus.transport_mode (
    id smallserial PRIMARY KEY, 
    category pgtempus.transport_mode_category NOT NULL,
    name text NOT NULL,
    traffic_rule smallint NOT NULL REFERENCES pgtempus.traffic_rule ON UPDATE CASCADE ON DELETE CASCADE, 
    speed_rule smallint REFERENCES pgtempus.speed_rule ON UPDATE CASCADE ON DELETE CASCADE, 
    toll_rule smallint REFERENCES pgtempus.toll_rule ON UPDATE CASCADE ON DELETE CASCADE, 
    vehicle_parking_rule smallint REFERENCES pgtempus.vehicle_parking_rule ON UPDATE CASCADE ON DELETE CASCADE
);
COMMENT ON TABLE pgtempus.transport_mode IS 'Available transport modes';
COMMENT ON COLUMN pgtempus.transport_mode.name IS 'Mode name'; 
COMMENT ON COLUMN pgtempus.transport_mode.traffic_rule IS 'Defines the road traffic rules followed by the mode. ';
COMMENT ON COLUMN pgtempus.transport_mode.speed_rule IS 'Defines the speed rules followed by the mode, NULL when speed is timetable defined or is a constant value. ';
CREATE INDEX ON pgtempus.transport_mode(traffic_rule);
CREATE INDEX ON pgtempus.transport_mode(speed_rule);
CREATE INDEX ON pgtempus.transport_mode(toll_rule);
CREATE INDEX ON pgtempus.transport_mode(vehicle_parking_rule);
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Walking'::pgtempus.transport_mode_category, 'Marche',1,null,null,null);
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Walking'::pgtempus.transport_mode_category, 'Fauteuil roulant', 2, null, null, null); 
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Bicycle'::pgtempus.transport_mode_category, 'Vélo indiv.', 3, null, null, 1);
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Car'::pgtempus.transport_mode_category, 'Voiture indiv. (avec péage)',4,1,1,2);
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Car'::pgtempus.transport_mode_category, 'Voiture indiv. (sans péage)',5,1,null, 2);
INSERT INTO pgtempus.transport_mode (category, name, traffic_rule, speed_rule, toll_rule, vehicle_parking_rule)
VALUES('Car'::pgtempus.transport_mode_category, 'Taxi', 6, 1, null, null);

DROP TABLE IF EXISTS pgtempus.source CASCADE;
CREATE TABLE pgtempus.source
(
    id smallserial PRIMARY KEY, 
    name character varying UNIQUE, 
    type integer, 
    comment character varying
);
COMMENT ON TABLE pgtempus.source IS 'List of data sources loaded in the database. Never directly read by C++. ';
COMMENT ON COLUMN pgtempus.source.type IS '1: road, 2: PT, 3: POI, 5: Physical barrier' ; 
COMMENT ON COLUMN pgtempus.source.name IS 'Short name used in user interfaces to define the source';

do $$
begin
raise notice '==== Parameters definition ====';
end$$;

DROP TABLE IF EXISTS pgtempus.data_format CASCADE;
CREATE TABLE pgtempus.data_format
(
    data_type character varying,
    data_format character varying, 
    name character varying,
    model_version character varying,
    default_encoding character varying,
    default_srid integer, 
    path_type character varying
); 
COMMENT ON TABLE pgtempus.data_format IS 'Plugin system table: do not modify!'; 

DROP TABLE IF EXISTS pgtempus.agregate CASCADE;
CREATE TABLE pgtempus.agregate
(
    id integer PRIMARY KEY,
    name character varying,
    func_name character varying
); 
COMMENT ON TABLE pgtempus.agregate IS 'Accepted agregate (between nodes, times or days) for indic calculation. Plugin system table: do not modify !';

DROP TABLE IF EXISTS pgtempus.modality CASCADE;
CREATE TABLE pgtempus.modality
(
    var_name character varying, 
    mod_id integer, 
    mod_lib character varying,
    needs_pt boolean, 
    CONSTRAINT modality_pkey PRIMARY KEY (var_name, mod_id)
); 
COMMENT ON TABLE pgtempus.modality IS 'Plugin system table: do not modify !';

DROP TABLE IF EXISTS pgtempus.base_obj_type CASCADE;
CREATE TABLE pgtempus.base_obj_type
(
  id integer NOT NULL PRIMARY KEY,
  name character varying,
  def_name character varying,
  needs_pt boolean
); 
COMMENT ON TABLE pgtempus.base_obj_type
  IS 'Available base object types for indic calculation. Plugin system table: do not modify !';
COMMENT ON COLUMN pgtempus.base_obj_type.id IS 'Integer ID';
COMMENT ON COLUMN pgtempus.base_obj_type.name IS 'Object name';
COMMENT ON COLUMN pgtempus.base_obj_type.def_name IS 'Default name of the layer';
COMMENT ON COLUMN pgtempus.base_obj_type.needs_pt IS 'True if a PT network is needed for this object type';

DROP TABLE IF EXISTS pgtempus.indic CASCADE;
CREATE TABLE pgtempus.indic
(
    id integer PRIMARY KEY,
    name character varying,
    map_size boolean,
    map_color boolean,
    col_name character varying
);
COMMENT ON TABLE pgtempus.indic
  IS 'Plugin system table: do not modify!'; 
  
DROP TABLE IF EXISTS pgtempus.db_param CASCADE;
CREATE TABLE pgtempus.db_param
(
    id serial PRIMARY KEY,
    name character varying,
    value character varying
);
INSERT INTO pgtempus.db_param(name, value) VALUES('srid', '%(target_srid)'); 

DO
$$
BEGIN
	raise notice '==== Calendars definition ==='; 
END
$$;

DROP TABLE IF EXISTS tempus_networks.days_period CASCADE;
CREATE TABLE tempus_networks.days_period
(
    id integer PRIMARY KEY,
    original_id character varying, 
    name varchar,
    days date[]
);
COMMENT ON TABLE tempus_networks.days_period IS 'List of days during which road arcs_sequences, speed profiles and PT trips apply';
INSERT INTO tempus_networks.days_period VALUES (0, NULL, 'Always', NULL);

DROP TABLE IF EXISTS tempus_networks.holiday_period CASCADE;
CREATE TABLE tempus_networks.holiday_period
(
    id serial,
    name varchar,
    start_date date,
    end_date date
);
COMMENT ON TABLE tempus_networks.holiday_period IS 'Holidays definition : can be modified to add new holidays periods. Never used directly by C++. ';

do $$
begin
raise notice '==== Networks general definition ===';
end$$;

DROP TABLE IF EXISTS tempus_networks.node CASCADE;
CREATE TABLE tempus_networks.node
(
    id bigserial PRIMARY KEY,
    original_id character varying, 
    source_id smallint NOT NULL REFERENCES pgtempus.source ON DELETE CASCADE ON UPDATE CASCADE, 
    geom Geometry(PointZ, %(target_srid)) NOT NULL
); 
COMMENT ON TABLE tempus_networks.node IS 'Generic nodes (corresponding either to PT, road, POI or barrier)';
CREATE INDEX ON tempus_networks.node(source_id);
CREATE INDEX ON tempus_networks.node USING gist (geom); 

DROP FUNCTION IF EXISTS tempus_networks.node_updated();
CREATE FUNCTION tempus_networks.node_updated()
RETURNS TRIGGER LANGUAGE plpgsql AS 
$$
BEGIN
    IF ( ( OLD.id != NEW.id ) OR ( OLD.source_id != NEW.source_id ) OR (OLD.original_id != NEW.original_id ) ) THEN
        RAISE EXCEPTION 'Update of fields id, source_id or original_id not allowed';
    END IF;
    
    IF ( OLD.geom != NEW.geom ) THEN
        UPDATE tempus_networks.arc
        SET geom = st_makeline(node_from.geom, node_to.geom)
        FROM tempus_networks.node node_from, tempus_networks.node node_to
        WHERE (arc.node_from_id = NEW.id OR arc.node_to_id = NEW.id) AND (node_from.id = arc.node_from_id AND node_to.id = arc.node_to_id);
    END IF;
END 
$$;

DROP TABLE IF EXISTS tempus_networks.arc CASCADE;
CREATE TABLE tempus_networks.arc
(
    id bigserial PRIMARY KEY,
    original_id character varying,
    sources_id smallint[] NOT NULL,
    node_from_id bigint NOT NULL REFERENCES tempus_networks.node ON DELETE CASCADE ON UPDATE CASCADE,
    node_to_id bigint NOT NULL REFERENCES tempus_networks.node ON DELETE CASCADE ON UPDATE CASCADE,
    type smallint, 
    length float,
    crossability smallint,
    diffusion smallint,
    geom Geometry(LinestringZ, %(target_srid))
);
CREATE INDEX ON tempus_networks.arc(node_from_id);
CREATE INDEX ON tempus_networks.arc(node_to_id);
CREATE INDEX ON tempus_networks.arc USING gist (geom);
COMMENT ON TABLE tempus_networks.arc IS 'Generic arcs (corresponding either to PT, road, POI or barrier)';
COMMENT ON COLUMN tempus_networks.arc.length IS 'Length in meters';
COMMENT ON COLUMN tempus_networks.arc.crossability IS 'Rule of crossability for isochrons: 0 = crossable from both sides, 1 = crossable from left to right, 2 = crossable from right to left, 3 = not crossable'; 
COMMENT ON COLUMN tempus_networks.arc.diffusion IS 'Rule of diffusion for isochrons: 0 = no diffusion, 1 = diffusion on the right of the arc, 2 = diffusion on the left of the arc, 3 = diffusion on both sides';

DROP FUNCTION IF EXISTS tempus_networks.arc_updated();
CREATE FUNCTION tempus_networks.arc_updated()
RETURNS TRIGGER LANGUAGE plpgsql AS 
$$
BEGIN
    -- Some fields must not be updated
    IF ( ( OLD.id != NEW.id ) OR ( OLD.sources_id != NEW.sources_id ) OR (OLD.original_id != NEW.original_id ) OR ( OLD.node_from_id != NEW.node_from_id ) OR ( OLD.node_to_id != NEW.node_to_id ) OR ( OLD.type != NEW.type ) ) THEN
      RAISE EXCEPTION 'Update of fields id, original_id, sources_id, node_from_id, node_to_id or type not allowed';
    END IF;
    
    IF ( OLD.geom != NEW.geom ) THEN
        WITH q AS (
            SELECT arcs_sequence.id, st_makeline(arc.geom) as geom
            FROM tempus_networks.arcs_sequence JOIN tempus_networks.arc ON (arc.id = ANY(arcs_sequence.arcs_id))
            WHERE arc.id = NEW.id
            GROUP BY arcs_sequence.id
        )
        UPDATE tempus_networks.arcs_sequence
        SET geom = q.geom
        FROM q
        WHERE arcs_sequence.id = q.id;
    
        WITH q AS (
            SELECT pt_trip.id, st_makeline(arc.geom) as geom
            FROM tempus_networks.pt_trip JOIN tempus_networks.arc ON (arc.id = ANY(pt_trip.arcs_id))
            WHERE arc.id = NEW.id
            GROUP BY pt_trip.id
        )
        UPDATE tempus_networks.pt_trip
        SET geom = q.geom
        FROM q
        WHERE pt_trip.id = q.id;
    END IF;
END 
$$;

DROP TABLE IF EXISTS tempus_networks.arcs_sequence CASCADE;
CREATE TABLE tempus_networks.arcs_sequence
(
    id bigserial PRIMARY KEY, 
    original_id character varying, 
    sources_id smallint[] NOT NULL, 
    arcs_id bigint[] NOT NULL, 
    geom Geometry(LinestringZ, %(target_srid))
);
COMMENT ON TABLE tempus_networks.arcs_sequence IS 'Road arcs sequence submitted to a cost penalty. Directly read by C++.';
COMMENT ON COLUMN tempus_networks.arcs_sequence.arcs_id IS 'Involved road arcs ID, not always forming a path';
CREATE INDEX arcs_sequence_arcs_id_idx ON tempus_networks.arcs_sequence USING gin(arcs_id);
CREATE INDEX arcs_sequences_geom_idx ON tempus_networks.arcs_sequence USING gist(geom);

DROP FUNCTION IF EXISTS tempus_networks.arcs_sequence_updated();
CREATE FUNCTION tempus_networks.arcs_sequence_updated()
RETURNS TRIGGER LANGUAGE plpgsql AS 
$$
BEGIN
    IF ( ( OLD.id != NEW.id ) OR ( OLD.sources_id != NEW.sources_id ) OR (OLD.original_id != NEW.original_id ) OR ( OLD.geom != NEW.geom ) ) THEN
      RAISE EXCEPTION 'Update of fields id, original_id, sources_id or geom not allowed';
    END IF; 
    
    IF ( OLD.arcs_id != NEW.arcs_id ) THEN        
        WITH r AS (
            WITH q AS (
                SELECT row_number() over() as id, a.arc_id, a.o
                FROM tempus_networks.arcs_sequence, unnest(arcs_sequence.arc_id) WITH ORDINALITY a(arc_id, o)
                WHERE arcs_sequence.id = NEW.id
            )    
            SELECT st_makeline(arc.geom ORDER BY q.o) as geom
            FROM q JOIN tempus_networks.arc ON (arc.id = q.arc_id)
        )
        UPDATE tempus_networks.arcs_sequence
        SET geom = r.geom
        FROM r
        WHERE arcs_sequence.id = NEW.id;
    END IF;
END 
$$;

do $$
begin
raise notice '==== Road networks definition ===';
end$$;

DROP TABLE IF EXISTS tempus_networks.road_section_attributes CASCADE;
CREATE TABLE tempus_networks.road_section_attributes
(
    arc_id bigint REFERENCES tempus_networks.arc ON UPDATE CASCADE ON DELETE CASCADE, 
    section_type_id smallint REFERENCES pgtempus.road_section_type ON DELETE CASCADE ON UPDATE CASCADE, 
    traffic_rules smallint[] NOT NULL, 
    car_speed_limit smallint, 
    street_name character varying, 
    road_number character varying, 
    lanes smallint,
    total_lanes smallint,
    max_slope real, 
    min_width real,
    PRIMARY KEY(arc_id)
);
COMMENT ON TABLE tempus_networks.road_section_attributes IS 'Oriented road arcs. Directly read by C++';
COMMENT ON COLUMN tempus_networks.road_section_attributes.car_speed_limit IS 'Car speed limit in km/h';
COMMENT ON COLUMN tempus_networks.road_section_attributes.street_name IS 'Street name';
COMMENT ON COLUMN tempus_networks.road_section_attributes.road_number IS 'Road number';
COMMENT ON COLUMN tempus_networks.road_section_attributes.lanes IS 'Number of lanes';
COMMENT ON COLUMN tempus_networks.road_section_attributes.total_lanes IS 'Total number of lanes (both directions)';
COMMENT ON COLUMN tempus_networks.road_section_attributes.traffic_rules IS 'List of allowed traffic rules';
CREATE INDEX road_section_attributes_arc_id_idx ON tempus_networks.road_section_attributes(arc_id);
CREATE INDEX road_section_attributes_section_type_id_idx ON tempus_networks.road_section_attributes(section_type_id);
CREATE INDEX road_section_attributes_traffic_rules_idx ON tempus_networks.road_section_attributes USING gin(traffic_rules);

DROP TABLE IF EXISTS tempus_networks.road_section_speed CASCADE;
CREATE TABLE tempus_networks.road_section_speed
(
    arc_id integer NOT NULL REFERENCES tempus_networks.arc ON DELETE CASCADE ON UPDATE CASCADE, 
    speed_rule smallint REFERENCES pgtempus.speed_rule, 
    value real,
    PRIMARY KEY(arc_id, speed_rule) 
);
COMMENT ON TABLE tempus_networks.road_section_speed IS 'Road arc travel time. Not directly read by C++. Can be used to build cost functions.';
COMMENT ON COLUMN tempus_networks.road_section_speed.value IS 'Speed in km/h'; 
CREATE INDEX road_section_speed_arc_id_idx ON tempus_networks.road_section_speed(arc_id);
CREATE INDEX road_section_speed_speed_rule_idx ON tempus_networks.road_section_speed(speed_rule);

DROP TABLE IF EXISTS tempus_networks.arcs_sequence_time CASCADE;
CREATE TABLE tempus_networks.arcs_sequence_time
(
    arcs_sequence_id integer NOT NULL REFERENCES tempus_networks.arcs_sequence ON DELETE CASCADE ON UPDATE CASCADE, 
    traffic_rules smallint[], 
    value integer,
    PRIMARY KEY(arcs_sequence_id, traffic_rules)
);
COMMENT ON TABLE tempus_networks.arcs_sequence_time IS 'arcs sequence time penalty (including forbidden movements). Not directly read by C++. Can be used to build cost functions.';
COMMENT ON COLUMN tempus_networks.arcs_sequence_time.value IS 'Time in sec';
CREATE INDEX arcs_sequence_time_arcs_sequence_id_idx ON tempus_networks.arcs_sequence_time(arcs_sequence_id);
CREATE INDEX arcs_sequence_time_traffic_rules_idx ON tempus_networks.arcs_sequence_time USING gin(traffic_rules);

DROP TABLE IF EXISTS tempus_networks.arcs_sequence_toll CASCADE;
CREATE TABLE tempus_networks.arcs_sequence_toll
(
    arcs_sequence_id integer NOT NULL REFERENCES tempus_networks.arcs_sequence ON DELETE CASCADE ON UPDATE CASCADE, 
    toll_rules smallint[], 
    value real,
    PRIMARY KEY(arcs_sequence_id, toll_rules)
);
COMMENT ON TABLE tempus_networks.arcs_sequence_toll IS 'Road arcs sequence toll penalty. Not directly read by C++. Can be used to build cost functions.';
COMMENT ON COLUMN tempus_networks.arcs_sequence_toll.value IS 'Toll in local currency';
CREATE INDEX arcs_sequence_toll_arcs_sequence_id_idx ON tempus_networks.arcs_sequence_toll(arcs_sequence_id);
CREATE INDEX arcs_sequence_toll_toll_rules_idx ON tempus_networks.arcs_sequence_toll USING gin(toll_rules);

do $$
begin
raise notice '==== Barriers definition ===';
end$$;

DROP TABLE IF EXISTS tempus_networks.barrier CASCADE;
CREATE TABLE tempus_networks.barrier
(
    arc_id bigint NOT NULL REFERENCES tempus_networks.arc ON DELETE CASCADE ON UPDATE CASCADE,
    name character varying,
    type smallint REFERENCES pgtempus.barrier_type
);
COMMENT ON TABLE tempus_networks.barrier IS 'Physical barriers which stop accessibility diffusion';
CREATE INDEX barrier_arc_id_idx ON tempus_networks.barrier(arc_id);
CREATE INDEX barrier_type_idx ON tempus_networks.barrier(type);

do $$
begin
raise notice '==== POI definition ===';
end$$;

DROP TABLE IF EXISTS tempus_networks.poi CASCADE;
CREATE TABLE tempus_networks.poi
(
    node_id bigint PRIMARY KEY REFERENCES tempus_networks.node ON DELETE CASCADE ON UPDATE CASCADE,
    name character varying,
    type smallint REFERENCES pgtempus.poi_type, 
    comment character varying
);
COMMENT ON TABLE tempus_networks.poi IS 'Table of points of interest, directly linked to the road network in the Tempus graph. These points correspond to real road nodes, where sometimes vehicles can be parked. This table is only for display. Never read by C++.';
COMMENT ON COLUMN tempus_networks.poi.type IS 'Type is only used for mapping purposes. It won''t change the Tempus graph.';
COMMENT ON COLUMN tempus_networks.poi.node_id IS 'Corresponding road node. Can''t be NULL.'; 
CREATE INDEX poi_node_id_idx ON tempus_networks.poi(node_id);
CREATE INDEX poi_type_idx ON tempus_networks.poi(type);

do $$
begin
raise notice '==== Mode changing cost definition ===';
end$$;

DROP TABLE IF EXISTS pgtempus.vehicle;
CREATE TABLE pgtempus.vehicle
(
    id serial PRIMARY KEY, 
    node_id bigint REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE, 
    transport_mode_id smallint REFERENCES pgtempus.transport_mode ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX vehicle_transport_mode_id_idx ON pgtempus.vehicle(transport_mode_id);
CREATE INDEX vehicle_node_id_idx ON pgtempus.vehicle(node_id);

DROP TABLE IF EXISTS tempus_networks.vehicle_parking_cost CASCADE;
CREATE TABLE tempus_networks.vehicle_parking_cost
(
    vehicle_parking_rule smallint NOT NULL REFERENCES pgtempus.vehicle_parking_rule,
    node_id bigint NOT NULL REFERENCES tempus_networks.node ON DELETE CASCADE ON UPDATE CASCADE,
    leaving_time time,
    leaving_fare real, 
    taking_time time,
    taking_fare real,
    PRIMARY KEY(vehicle_parking_rule, node_id)
);
COMMENT ON TABLE tempus_networks.vehicle_parking_cost IS 'Costs which are applied when there is a mode change at a given node and when a given mode is left or taken. When a (node, mode) pair is not in this table, it means this mode cannot be left or taken at this node.';
COMMENT ON COLUMN tempus_networks.vehicle_parking_cost.leaving_time IS 'In seconds';
COMMENT ON COLUMN tempus_networks.vehicle_parking_cost.taking_time IS 'In seconds';
CREATE INDEX vehicle_parking_cost_vehicle_parking_rule_idx ON tempus_networks.vehicle_parking_cost(vehicle_parking_rule);
CREATE INDEX vehicle_parking_cost_node_id_idx ON tempus_networks.vehicle_parking_cost(node_id);

do $$
begin
raise notice '==== PT networks and fare definition ===';
end$$;

DROP TABLE IF EXISTS tempus_networks.pt_agency CASCADE;
CREATE TABLE tempus_networks.pt_agency (
        id smallserial PRIMARY KEY,
        original_id character varying,
        source_id smallint REFERENCES pgtempus.source ON UPDATE CASCADE ON DELETE CASCADE,
        name VARCHAR NOT NULL,
        url character varying,
        timezone character varying,
        lang character varying
);
CREATE INDEX ON tempus_networks.pt_agency(source_id);
COMMENT ON TABLE tempus_networks.pt_agency IS 'Public transport agencies. A network can be composed of several agencies.';
COMMENT ON COLUMN tempus_networks.pt_agency.name IS 'Short name used in user interfaces to define the network';
COMMENT ON COLUMN tempus_networks.pt_agency.url IS 'Kept for GTFS compatibility';
COMMENT ON COLUMN tempus_networks.pt_agency.timezone IS 'Kept for GTFS compatibility';
COMMENT ON COLUMN tempus_networks.pt_agency.lang IS 'Kept for GTFS compatibility';

DROP TABLE IF EXISTS pgtempus.pt_fare_rule CASCADE;
CREATE TABLE pgtempus.pt_fare_rule 
(
    id smallserial PRIMARY KEY, 
    original_id character varying, 
    name character varying, 
    pt_agency_id smallint REFERENCES tempus_networks.pt_agency ON UPDATE CASCADE ON DELETE CASCADE, 
    currency_type character varying,
    payment_method smallint,
    max_transfers smallint,
    max_transfer_duration smallint,
    max_travel_duration smallint
);
COMMENT ON TABLE tempus_networks.pt_agency IS 'Type of ticket for each PT agency and each user category.';
CREATE INDEX ON pgtempus.pt_fare_rule(pt_agency_id);

DROP TABLE IF EXISTS tempus_networks.pt_route CASCADE;
CREATE TABLE tempus_networks.pt_route (
        id serial PRIMARY KEY,
        original_id character varying, 
        pt_agency_id smallint NOT NULL REFERENCES tempus_networks.pt_agency ON UPDATE CASCADE ON DELETE CASCADE, 
        short_name character varying NOT NULL,
        long_name character varying,
        description character varying,
        url character varying,
        color character varying,
        text_color character varying
);
CREATE INDEX pt_route_pt_agency_id_idx ON tempus_networks.pt_route(pt_agency_id);
COMMENT ON TABLE tempus_networks.pt_route IS 'Public transport routes: group of trips having a common name or number (including return trips or variants).';
COMMENT ON COLUMN tempus_networks.pt_route.short_name IS 'Short name used in user interfaces to define the route';
COMMENT ON COLUMN tempus_networks.pt_route.long_name IS 'Long name to define the route, facultative.';
COMMENT ON COLUMN tempus_networks.pt_route.description IS 'Kept for GTFS compatibility. ';
COMMENT ON COLUMN tempus_networks.pt_route.url IS 'Kept for GTFS compatibility. ';
COMMENT ON COLUMN tempus_networks.pt_route.color IS 'Kept for GTFS compatibility. ';
COMMENT ON COLUMN tempus_networks.pt_route.text_color IS 'Kept for GTFS compatibility. ';

DROP TABLE IF EXISTS tempus_networks.pt_fare_zone CASCADE;
CREATE TABLE tempus_networks.pt_fare_zone (
        id serial PRIMARY KEY, 
        original_id character varying,
        name character varying, 
        geom Geometry(MultipolygonZ, %(target_srid))
);
COMMENT ON TABLE tempus_networks.pt_fare_zone IS 'Public transport geographical fare zones.'; 

DROP TABLE IF EXISTS tempus_networks.pt_stop CASCADE;
CREATE TABLE tempus_networks.pt_stop
(
    node_id bigint NOT NULL REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE, 
    name character varying NOT NULL,
    pt_fare_zone_id integer REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE,
    gtfs_location_type integer,
    traffic_rules smallint[],
    security_time time,
    url character varying,
    description character varying,
    PRIMARY KEY(node_id)
);
CREATE INDEX pt_stop_node_id_idx ON tempus_networks.pt_stop(node_id); 
CREATE INDEX pt_stop_pt_fare_zone_id_idx ON tempus_networks.pt_stop(pt_fare_zone_id); 
CREATE INDEX pt_stop_traffic_rules_idx ON tempus_networks.pt_stop USING gin(traffic_rules); 
COMMENT ON TABLE tempus_networks.pt_stop IS 'Public transport stops, used to define trips. All stops in this table should have associated trips. "';
COMMENT ON COLUMN tempus_networks.pt_stop.gtfs_location_type IS 'Copy of the GTFS field: 0 for platform, 1 for station, 2 for station entrance or exit, 3 for generic interarc, 4 for boarding area. Kept in DB for checking, but not used by the application.';
COMMENT ON COLUMN tempus_networks.pt_stop.name IS 'Stop name in local language';
COMMENT ON COLUMN tempus_networks.pt_stop.traffic_rules IS 'Tells which traffic rules can access this stop, particularly important for wheelchairs.';
COMMENT ON COLUMN tempus_networks.pt_stop.pt_fare_zone_id IS 'Fare zone ID reference (foreign key)';
COMMENT ON COLUMN tempus_networks.pt_stop.url IS 'Kept for GTFS compatibility';
COMMENT ON COLUMN tempus_networks.pt_stop.description IS 'Kept for GTFS compatibility';

DROP TABLE IF EXISTS tempus_networks.pt_zonal_fare CASCADE;
CREATE TABLE tempus_networks.pt_zonal_fare (
        pt_fare_rule integer REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE,
        pt_route_id bigint REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE,
        zone_id_from integer REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE,
        zone_id_to integer REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE,
        fare float4 NOT NULL
);
CREATE INDEX ON tempus_networks.pt_zonal_fare(pt_fare_rule);
CREATE INDEX ON tempus_networks.pt_zonal_fare(pt_route_id);
CREATE INDEX ON tempus_networks.pt_zonal_fare(zone_id_from);
CREATE INDEX ON tempus_networks.pt_zonal_fare(zone_id_to);
COMMENT ON TABLE tempus_networks.pt_zonal_fare IS 'Public transport zonal fare can depend on user category (pt_fare_rule), origin and destination zones and pt_agency. Can be used to define public transport cost function. Never called by C++.';

DROP TABLE IF EXISTS tempus_networks.pt_unit_fare CASCADE;
CREATE TABLE tempus_networks.pt_unit_fare (
        pt_fare_rule integer REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE, 
        pt_route_id bigint REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE,
        fare float4 NOT NULL
);
CREATE INDEX ON tempus_networks.pt_unit_fare(pt_fare_rule);
CREATE INDEX ON tempus_networks.pt_unit_fare(pt_route_id);

CREATE TABLE tempus_networks.pt_od_fare (
        pt_fare_rule integer REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE,         
        pt_route_id bigint REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE,
        pt_stop_id_from bigint, 
        pt_stop_id_to bigint, 
        fare float4 NOT NULL
);
CREATE INDEX ON tempus_networks.pt_od_fare(pt_fare_rule);
CREATE INDEX ON tempus_networks.pt_od_fare(pt_route_id);

DROP TABLE IF EXISTS tempus_networks.pt_trip CASCADE;
CREATE TABLE tempus_networks.pt_trip (
        id bigserial PRIMARY KEY,
        original_id character varying,
        arcs_id bigint[] NOT NULL,
        pt_route_id integer REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE,
        days_period_id integer REFERENCES tempus_networks.days_period ON UPDATE CASCADE ON DELETE CASCADE,
        traffic_rules smallint[] NOT NULL, 
        pt_trip_types_id smallint[],
        name character varying,
        direction smallint, 
        direction_headsign character varying, 
        geom_arc Geometry(LinestringZ, %(target_srid)) NOT NULL, 
        geom_nodes Geometry(MultiPointZ, %(target_srid)) NOT NULL
);
CREATE INDEX ON tempus_networks.pt_trip (pt_route_id);
CREATE INDEX ON tempus_networks.pt_trip (days_period_id);
CREATE INDEX pt_trip_traffic_rules_idx ON tempus_networks.pt_trip USING gin(traffic_rules);
COMMENT ON TABLE tempus_networks.pt_trip IS 'Public transport trips, having a defined stops sequence and timetable.';
COMMENT ON COLUMN tempus_networks.pt_trip.pt_trip_types_id IS 'List of associated PT trip types, see pgtempus.pt_trip_type table.';
COMMENT ON COLUMN tempus_networks.pt_trip.traffic_rules IS 'List of traffic rules allowed to board this service';

DROP FUNCTION IF EXISTS tempus_networks.pt_trip_updated();
CREATE FUNCTION tempus_networks.pt_trip_updated()
RETURNS TRIGGER LANGUAGE plpgsql AS 
$$
BEGIN
    IF ( ( OLD.id != NEW.id ) OR (OLD.original_id != NEW.original_id ) OR ( OLD.geom_arc != NEW.geom_arc ) OR ( OLD.geom_nodes != NEW.geom_nodes ) ) THEN
      RAISE EXCEPTION 'Update of fields id, original_id, geom_arc or geom_nodes not allowed';
    END IF; 
    
    IF ( OLD.arcs_id != NEW.arcs_id ) THEN
        WITH r AS (
            WITH q AS (
                SELECT row_number() over() as id, a.arc_id, a.o
                FROM tempus_networks.pt_trip, unnest(pt_trip.arc_id) WITH ORDINALITY a(arc_id, o)
                WHERE pt_trip.id = NEW.id
            )    
            SELECT st_makeline(arc.geom ORDER BY q.o) as geom
            FROM q JOIN tempus_networks.arc ON (arc.id = q.arc_id)
        )
        UPDATE tempus_networks.pt_trip
        SET geom = r.geom
        FROM r
        WHERE pt_trip.id = NEW.id;
        
        WITH q AS (
            SELECT st_collect(node.geom) as geom
            FROM tempus_networks.node JOIN tempus_networks.arc ON (arc.node_from_id = node.id OR arc.node_to_id = node.id)
            WHERE arc.id = ANY(NEW.arcs_id)
        )
        UPDATE tempus_networks.pt_trip
        SET geom_nodes = q.geom
        FROM q
        WHERE pt_trip.id = NEW.id;
    END IF;
END 
$$;

DROP TABLE IF EXISTS tempus_networks.pt_section CASCADE;
CREATE TABLE tempus_networks.pt_section
(
    arc_id bigint NOT NULL REFERENCES tempus_networks.arc ON DELETE CASCADE ON UPDATE CASCADE,
    sub_arcs bigint[],
    PRIMARY KEY(arc_id)
);
CREATE INDEX ON tempus_networks.pt_section (arc_id);
COMMENT ON TABLE tempus_networks.pt_section IS 'PT arcs, between two consecutive stops. Directly read by C++';

DROP TABLE IF EXISTS tempus_networks.pt_section_stop_times CASCADE;
CREATE TABLE tempus_networks.pt_section_stop_times (
        arc_id integer NOT NULL REFERENCES tempus_networks.arc(id) ON DELETE CASCADE ON UPDATE CASCADE,
        pt_trip_id bigint REFERENCES tempus_networks.pt_trip ON DELETE CASCADE ON UPDATE CASCADE,
        time_from interval, 
        time_to interval, 
        interpolated_time_from boolean, 
        interpolated_time_to boolean,
        direction_label character varying,
        pickup_type smallint,
        dropoff_type smallint,
        PRIMARY KEY (arc_id, pt_trip_id)
);
CREATE INDEX ON tempus_networks.pt_section_stop_times(arc_id);
CREATE INDEX ON tempus_networks.pt_section_stop_times(pt_trip_id);
COMMENT ON TABLE tempus_networks.pt_section_stop_times IS 'Public transport arcs stop times';
COMMENT ON COLUMN tempus_networks.pt_section_stop_times.direction_label IS 'Kept for GTFS compatibility';
COMMENT ON COLUMN tempus_networks.pt_section_stop_times.pickup_type IS 'Kept for GTFS compatibility';
COMMENT ON COLUMN tempus_networks.pt_section_stop_times.dropoff_type IS 'Kept for GTFS compatibility';

DO
$$
BEGIN
	raise notice '==== Materialized views ===';
END
$$;

-- Arcs
DROP MATERIALIZED VIEW IF EXISTS tempus_networks.arc_complete CASCADE;
CREATE MATERIALIZED VIEW tempus_networks.arc_complete AS
SELECT arc.id, 
       arc.original_id, 
       arc.sources_id,
       arc.node_from_id, 
       arc.node_to_id, 
       arc.crossability, 
       arc.diffusion, 
       road_section_attributes.section_type_id, 
       arc.type, 
       round(arc.length::numeric, 3) as length, 
       arc.geom, 
       coalesce(road_section_attributes.traffic_rules, array_cat_agg(pt_trip.traffic_rules)) as traffic_rules, 
       CASE WHEN (arc.type = 2) AND (array_remove(array_agg(DISTINCT pt_route.pt_agency_id ORDER BY pt_route.pt_agency_id), NULL) != ARRAY[]::smallint[]) 
            THEN array_remove(array_agg(DISTINCT pt_route.pt_agency_id ORDER BY pt_route.pt_agency_id), NULL) 
       END AS pt_agencies_id,
       CASE WHEN (arc.type = 2) AND (array_cat_agg(pt_trip.pt_trip_types_id) != ARRAY[]::smallint[]) 
            THEN array(SELECT DISTINCT unnest(array_cat_agg(pt_trip.pt_trip_types_id))) 
       END AS pt_trip_types_id
FROM tempus_networks.arc LEFT JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id)
                         LEFT JOIN tempus_networks.pt_section_stop_times ON (pt_section_stop_times.arc_id = arc.id)
                         LEFT JOIN tempus_networks.pt_trip ON (pt_trip.id = pt_section_stop_times.pt_trip_id)
                         LEFT JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
GROUP BY arc.id, arc.node_from_id, arc.node_to_id, road_section_attributes.section_type_id, arc.type, arc.length, arc.geom, road_section_attributes.traffic_rules;

-- Nodes
DROP MATERIALIZED VIEW IF EXISTS tempus_networks.node_complete;
CREATE MATERIALIZED VIEW tempus_networks.node_complete AS
SELECT node.id, 
       node.original_id, 
       node.source_id, 
       node.geom, 
       CASE WHEN array_cat_agg(arc_complete.traffic_rules) != ARRAY[]::smallint[] THEN array(SELECT DISTINCT unnest(array_cat_agg(arc_complete.traffic_rules))) END AS traffic_rules, 
       CASE WHEN array_cat_agg(arc_complete.pt_agencies_id) != ARRAY[]::smallint[] THEN array(SELECT DISTINCT unnest(array_cat_agg(arc_complete.pt_agencies_id))) END AS pt_agencies_id, 
       CASE WHEN array_cat_agg(arc_complete.pt_trip_types_id) != ARRAY[]::smallint[] THEN array(SELECT DISTINCT unnest(array_cat_agg(arc_complete.pt_trip_types_id))) END AS pt_trip_types_id,
       array_agg(DISTINCT arc_complete.type) as arc_types
FROM tempus_networks.node JOIN tempus_networks.arc_complete ON (arc_complete.node_from_id = node.id OR arc_complete.node_to_id = node.id)
GROUP BY node.id, node.source_id, node.geom; 

DO
$$
BEGIN
	raise notice '==== Non materialized views ===';
END
$$;

-- Road section view
DROP VIEW IF EXISTS tempus_networks.road_section CASCADE;
CREATE VIEW tempus_networks.road_section AS
(
    SELECT arc.id,            
           CASE WHEN node_from.source_id = node_to.source_id THEN array[node_from.source_id] ELSE array[node_from.source_id, node_to.source_id] END as sources_id, 
           arc.node_from_id, 
           arc.node_to_id, 
           arc.diffusion, 
           arc.crossability,
           road_section_attributes.section_type_id, 
           arc.length,
           road_section_attributes.traffic_rules, 
           road_section_attributes.car_speed_limit,
           road_section_attributes.street_name,
           road_section_attributes.road_number,
           road_section_attributes.lanes,
           road_section_attributes.total_lanes,
           road_section_attributes.max_slope,
           road_section_attributes.min_width,
           road_section_speed.value as car_speed, 
           arc.geom
    FROM tempus_networks.arc JOIN tempus_networks.node as node_from ON (node_from.id = arc.node_from_id)
                             JOIN tempus_networks.node as node_to ON (node_to.id = arc.node_to_id)
                             LEFT JOIN tempus_networks.road_section_attributes ON arc.id = road_section_attributes.arc_id
                             LEFT JOIN tempus_networks.road_section_speed ON (road_section_attributes.arc_id = road_section_speed.arc_id AND road_section_speed.speed_rule = (SELECT id FROM pgtempus.speed_rule WHERE name = 'Car'))
    WHERE arc.type = 1
    ORDER BY arc.id
);

CREATE RULE update_road_section_road_section_attributes AS 
ON UPDATE TO tempus_networks.road_section
DO INSTEAD (
    UPDATE tempus_networks.road_section_attributes
    SET section_type_id = NEW.section_type_id,
        traffic_rules = NEW.traffic_rules, 
        car_speed_limit = NEW.car_speed_limit,
        street_name = NEW.street_name,
        road_number = NEW.road_number,
        lanes = NEW.lanes,
        total_lanes = NEW.total_lanes,
        max_slope = NEW.max_slope,
        min_width = NEW.min_width
    WHERE arc_id = NEW.id;
); 

CREATE RULE update_road_section_arc AS 
ON UPDATE TO tempus_networks.road_section
DO INSTEAD (  
    UPDATE tempus_networks.arc
    SET id = NEW.id, 
        node_from_id = NEW.node_from_id, 
        node_to_id = NEW.node_to_id,
        diffusion = NEW.diffusion,
        crossability = NEW.crossability,
        length = NEW.length,
        geom = NEW.geom
    WHERE id = OLD.id;
);

CREATE RULE update_road_section_road_section_speed AS 
ON UPDATE TO tempus_networks.road_section
DO INSTEAD 
    UPDATE tempus_networks.road_section_speed
    SET value = NEW.car_speed
    WHERE arc_id = NEW.id AND speed_rule = (SELECT id FROM pgtempus.speed_rule WHERE name = 'Car'); 

DROP VIEW IF EXISTS tempus_networks.road_node;
CREATE VIEW tempus_networks.road_node AS
(
    SELECT node.id, 
           node.source_id, 
           array(select distinct unnest(array_cat_agg(road_section.traffic_rules)) order by 1) as traffic_rules, 
           node.geom
    FROM tempus_networks.node JOIN tempus_networks.road_section ON (node.id = road_section.node_from_id OR node.id = road_section.node_to_id)
    GROUP BY node.id, node.source_id, node.geom    
);

CREATE RULE update_road_node_node AS 
ON UPDATE TO tempus_networks.road_node
DO INSTEAD (
    UPDATE tempus_networks.node
    SET id = NEW.id,
        source_id = NEW.source_id,
        geom = NEW.geom
    WHERE id = NEW.id; 
);

-- Penalized path view
DROP VIEW IF EXISTS tempus_networks.penalized_path;
CREATE VIEW tempus_networks.penalized_path AS
SELECT arcs_sequence.id,
       arcs_sequence.original_id,
       arcs_sequence.sources_id,
       arcs_sequence.arcs_id,
       arcs_sequence_time.traffic_rules,
       arcs_sequence_time.value as time_value,
       arcs_sequence_toll.toll_rules,
       arcs_sequence_toll.value as toll_value,
       arcs_sequence.geom
FROM tempus_networks.arcs_sequence LEFT JOIN tempus_networks.arcs_sequence_time ON (arcs_sequence_time.arcs_sequence_id = arcs_sequence.id)
                                   LEFT JOIN tempus_networks.arcs_sequence_toll ON (arcs_sequence_toll.arcs_sequence_id = arcs_sequence.id); 
COMMENT ON VIEW tempus_networks.penalized_path IS 'Penalized paths (by time or toll) or forbidden paths (having infinite time value).';

CREATE RULE update_penalized_path_arcs_sequence_time AS
ON UPDATE TO tempus_networks.penalized_path
DO INSTEAD (
    UPDATE tempus_networks.arcs_sequence_time
    SET traffic_rules = NEW.traffic_rules, 
        value = NEW.time_value
    WHERE arcs_sequence_id = OLD.id;
);

CREATE RULE update_penalized_path_arcs_sequence_toll AS
ON UPDATE TO tempus_networks.penalized_path
DO INSTEAD (
    UPDATE tempus_networks.arcs_sequence_toll
    SET toll_rules = NEW.toll_rules, 
        value = NEW.toll_value
    WHERE arcs_sequence_id = OLD.id;
);

CREATE RULE update_penalized_path_arcs_sequence AS
ON UPDATE TO tempus_networks.penalized_path
DO INSTEAD (
    UPDATE tempus_networks.arcs_sequence
    SET id = NEW.id, 
        arcs_id = NEW.arcs_id
    WHERE id = OLD.id;
);

-- POI geom views
DROP VIEW IF EXISTS tempus_networks.poi_geom;
CREATE VIEW tempus_networks.poi_geom AS
SELECT node.id,
       node.source_id,
       poi.name, 
       poi.type, 
       poi.comment,  
       node.geom
FROM tempus_networks.poi JOIN tempus_networks.node ON (node.id = poi.node_id); 

CREATE RULE update_poi_geom_poi AS
ON UPDATE TO tempus_networks.poi_geom
DO INSTEAD (
    UPDATE tempus_networks.poi
    SET name = NEW.name,
        type = NEW.type
    WHERE node_id = OLD.id; 
); 

CREATE RULE delete_poi_geom AS
ON DELETE TO tempus_networks.poi_geom
DO INSTEAD
    DELETE FROM tempus_networks.poi
    WHERE node_id = OLD.id AND type = OLD.type AND name = OLD.name;

CREATE RULE update_poi_geom_node AS
ON UPDATE TO tempus_networks.poi_geom
DO INSTEAD (
    UPDATE tempus_networks.node
    SET id = NEW.id, 
        source_id = NEW.source_id, 
        geom = NEW.geom 
    WHERE id = OLD.id; 
);

CREATE RULE insert_poi_geom AS
ON INSERT TO tempus_networks.poi_geom
DO INSTEAD
    INSERT INTO tempus_networks.poi(node_id, name, type)
    VALUES( NEW.id, NEW.name, NEW.type ); 
    
DROP VIEW IF EXISTS tempus_networks.unconnected_poi_geom;
CREATE VIEW tempus_networks.unconnected_poi_geom AS
(
    SELECT node.id,
           node.source_id,
           poi.name, 
           poi.type,
           poi.comment, 
           node.geom
    FROM tempus_networks.poi JOIN tempus_networks.node ON (node.id = poi.node_id)
)
EXCEPT
(
    SELECT node_complete.id, 
           node_complete.source_id, 
           poi.name, 
           poi.type, 
           poi.comment, 
           node_complete.geom
    FROM tempus_networks.node_complete JOIN tempus_networks.poi ON (poi.node_id = node_complete.id)
); 

CREATE RULE update_unconnected_poi_geom_poi AS
ON UPDATE TO tempus_networks.unconnected_poi_geom
DO INSTEAD (
    UPDATE tempus_networks.poi
    SET name = NEW.name,
        type = NEW.type
    WHERE node_id = OLD.id; 
); 

CREATE RULE delete_unconnected_poi_geom AS
ON DELETE TO tempus_networks.unconnected_poi_geom
DO INSTEAD
    DELETE FROM tempus_networks.poi
    WHERE node_id = OLD.id AND type = OLD.type AND name = OLD.name;

CREATE RULE update_unconnected_poi_geom_node AS
ON UPDATE TO tempus_networks.unconnected_poi_geom
DO INSTEAD (
    UPDATE tempus_networks.node
    SET id = NEW.id, 
        source_id = NEW.source_id, 
        geom = NEW.geom 
    WHERE id = OLD.id; 
);

CREATE RULE insert_unconnected_poi_geom AS
ON INSERT TO tempus_networks.unconnected_poi_geom
DO INSTEAD
    INSERT INTO tempus_networks.poi(node_id, name, type)
    VALUES( NEW.id, NEW.name, NEW.type ); 

-- Vehicle parking cost geom view
DROP VIEW IF EXISTS tempus_networks.vehicle_parking_cost_geom CASCADE;
CREATE VIEW tempus_networks.vehicle_parking_cost_geom
 AS
 SELECT row_number() OVER () AS id,
    vehicle_parking_cost.vehicle_parking_rule,
    vehicle_parking_cost.node_id,
    vehicle_parking_cost.leaving_time,
    vehicle_parking_cost.leaving_fare,
    vehicle_parking_cost.taking_time,
    vehicle_parking_cost.taking_fare,
    node.geom
   FROM tempus_networks.vehicle_parking_cost JOIN tempus_networks.node ON node.id = vehicle_parking_cost.node_id; 

CREATE RULE delete_vehicle_parking_cost_geom_vehicle_parking_cost AS
ON DELETE TO tempus_networks.vehicle_parking_cost_geom
DO INSTEAD
    DELETE FROM tempus_networks.vehicle_parking_cost
    WHERE vehicle_parking_rule = OLD.vehicle_parking_rule
    AND node_id = OLD.node_id;

CREATE RULE update_vehicle_parking_cost_geom_vehicle_parking_cost AS
ON UPDATE TO tempus_networks.vehicle_parking_cost_geom
DO INSTEAD 
    UPDATE tempus_networks.vehicle_parking_cost
    SET vehicle_parking_rule = NEW.vehicle_parking_rule,
        node_id = NEW.node_id,
        leaving_time = NEW.leaving_time,
        leaving_fare = NEW.leaving_fare,
        taking_time = NEW.taking_time,
        taking_fare = NEW.taking_fare
    WHERE node_id = OLD.node_id AND vehicle_parking_rule = OLD.vehicle_parking_rule;

CREATE RULE insert_vehicle_parking_cost_geom AS
ON INSERT TO tempus_networks.vehicle_parking_cost_geom
DO INSTEAD
    INSERT INTO tempus_networks.vehicle_parking_cost(vehicle_parking_rule, node_id, leaving_time, leaving_fare, taking_time, taking_fare)
    VALUES( NEW.vehicle_parking_rule, NEW.node_id, NEW.leaving_time, NEW.leaving_fare, NEW.taking_time, NEW.taking_fare ); 

-- Vehicle geom view
DROP VIEW IF EXISTS pgtempus.vehicle_geom; 
CREATE VIEW pgtempus.vehicle_geom AS
SELECT veh.id, 
       veh.node_id, 
       veh.transport_mode_id, 
       transport_mode.category, 
       node.geom
FROM pgtempus.vehicle veh JOIN tempus_networks.node ON (node.id = veh.node_id)
                          JOIN pgtempus.transport_mode ON (veh.transport_mode_id = transport_mode.id); 

CREATE RULE delete_vehicle_geom_vehicle AS
ON DELETE TO pgtempus.vehicle_geom
DO INSTEAD
    DELETE FROM pgtempus.vehicle
    WHERE id = OLD.id;

CREATE RULE update_vehicle_geom_vehicle AS
ON UPDATE TO pgtempus.vehicle_geom
DO INSTEAD 
    UPDATE pgtempus.vehicle
    SET id = NEW.id,
        node_id = NEW.node_id,
        transport_mode_id = NEW.transport_mode_id
    WHERE id = OLD.id;

CREATE RULE insert_vehicle_geom AS
ON INSERT TO pgtempus.vehicle_geom
DO INSTEAD
    INSERT INTO pgtempus.vehicle(id, node_id, transport_mode_id)
    VALUES( NEW.id, NEW.node_id, NEW.transport_mode_id ); 

-- PT stop geom view
DROP VIEW IF EXISTS tempus_networks.pt_stop_geom;
CREATE VIEW tempus_networks.pt_stop_geom AS
(
    SELECT req.id, 
           req.name, 
           req.station_id, 
           req.station_name,
           req.source_id, 
           req.original_id, 
           req.traffic_rules, 
           req.boarding_traffic_rules,
           req.pt_fare_zone_id, 
           req.gtfs_location_type, 
           req.security_time, 
           array_agg(DISTINCT pt_trip_type_id)::smallint[] as pt_trip_types_id, 
           array_agg(DISTINCT pt_trip_type.name) as pt_trip_types_name, 
           array_agg(DISTINCT pt_agency_id)::smallint[] as pt_agencies_id, 
           array_agg(DISTINCT pt_agency.name) as pt_agencies_name,
           req.pt_stop_geom,
           req.pt_station_geom
    FROM
    (
        SELECT node_complete.id, 
               pt_stop.name, 
               node_complete.source_id, 
               node_complete.original_id, 
               poi.node_id as station_id, 
               poi.name as station_name,
               node_complete.traffic_rules,
               pt_stop.traffic_rules as boarding_traffic_rules, 
               pt_stop.pt_fare_zone_id,
               pt_stop.gtfs_location_type,
               pt_stop.security_time,
               pt_route.pt_agency_id,
               unnest(pt_trip.pt_trip_types_id) as pt_trip_type_id,
               node_complete.geom as pt_stop_geom,
               poi_node.geom as pt_station_geom
        FROM tempus_networks.pt_stop JOIN tempus_networks.node_complete ON ( node_complete.id = pt_stop.node_id )
                                     JOIN pgtempus.source On (source.id = node_complete.source_id)
                                     JOIN tempus_networks.arc ON ( arc.node_from_id = node_complete.id OR arc.node_to_id = node_complete.id )
                                     JOIN tempus_networks.pt_section_stop_times ON ( pt_section_stop_times.arc_id = arc.id )
                                     JOIN tempus_networks.pt_trip ON ( pt_trip.id = pt_section_stop_times.pt_trip_id )
                                     JOIN tempus_networks.pt_route ON ( pt_route.id = pt_trip.pt_route_id ) 
                                     LEFT JOIN tempus_networks.arc connection_arc ON ( node_complete.id = connection_arc.node_to_id )
                                     LEFT JOIN tempus_networks.road_section_attributes ON (road_section_attributes.arc_id = connection_arc.id)
                                     LEFT JOIN tempus_networks.poi ON ( poi.node_id = connection_arc.node_from_id )
                                     LEFT JOIN tempus_networks.node poi_node ON ( poi.node_id = poi_node.id )
        WHERE road_section_attributes.section_type_id=(SELECT id FROM pgtempus.road_section_type WHERE name = 'Virtuel') AND poi.type = (SELECT id FROM pgtempus.poi_type WHERE name = 'Public transport stop area')
    ) req JOIN pgtempus.pt_trip_type ON ( pt_trip_type.id = req.pt_trip_type_id )
          JOIN tempus_networks.pt_agency ON ( pt_agency.id = req.pt_agency_id )
    GROUP BY req.id, 
             req.name, 
             req.station_id,
             req.station_name, 
             req.source_id, 
             req.original_id, 
             req.traffic_rules, 
	         req.boarding_traffic_rules,
             req.pt_fare_zone_id, 
             req.gtfs_location_type, 
             req.security_time,
             req.pt_stop_geom,
             req.pt_station_geom
);

CREATE RULE update_pt_stop_geom_node AS
ON UPDATE TO tempus_networks.pt_stop_geom
DO INSTEAD (
    UPDATE tempus_networks.node
    SET original_id = NEW.original_id,
        source_id = NEW.source_id,
        geom = NEW.pt_stop_geom
    WHERE id = OLD.id;
);

CREATE RULE update_pt_stop_geom_pt_stop AS
ON UPDATE TO tempus_networks.pt_stop_geom
DO INSTEAD 
    UPDATE tempus_networks.pt_stop
    SET node_id = NEW.id,
        traffic_rules = NEW.traffic_rules,
        pt_fare_zone_id = NEW.pt_fare_zone_id,
        gtfs_location_type = NEW.gtfs_location_type,
        security_time = NEW.security_time     
    WHERE node_id = OLD.id;

DROP VIEW IF EXISTS tempus_networks.pt_section_geom;
CREATE VIEW tempus_networks.pt_section_geom AS
SELECT q.id, 
       source_id, 
       node_from_id, 
	   node_to_id, 
	   sub_arcs,
       round(length::numeric, 3) as length,
       diffusion, 
       crossability,
       array_cat_agg(DISTINCT traffic_rules ORDER BY traffic_rules) as traffic_rules, 
       array_agg(DISTINCT COALESCE(pt_trip_type_id, 1) ORDER BY COALESCE(pt_trip_type_id, 1))::smallint[] as pt_trip_types_id, 
	   array_agg(DISTINCT pt_trip_type.name ORDER BY pt_trip_type.name) as pt_trip_types_name, 
       array_agg(DISTINCT pt_route_id ORDER BY pt_route_id) as pt_routes_id, 
       array_agg(DISTINCT pt_route_long_name ORDER BY pt_route_long_name) as pt_routes_long_name, 
       array_agg(DISTINCT pt_route_short_name ORDER BY pt_route_short_name) as pt_routes_short_name, 
       array_agg(DISTINCT pt_agency_id ORDER BY pt_agency_id)::smallint[] as pt_agencies_id, 
       array_agg(DISTINCT pt_agency_name ORDER BY pt_agency_name) as pt_agencies_name, 
	   geom 
FROM
(
	SELECT arc.id, 
		   pt_agency.source_id, 
		   arc.node_from_id, 
		   arc.node_to_id,
           arc.length,
		   pt_section.sub_arcs,
           arc.diffusion, 
           arc.crossability,
		   unnest(pt_trip.pt_trip_types_id) as pt_trip_type_id, 
           pt_trip.traffic_rules, 
           pt_route.id as pt_route_id, 
           pt_route.short_name as pt_route_short_name,
           pt_route.long_name as pt_route_long_name,
           pt_agency.id as pt_agency_id, 
           pt_agency.name as pt_agency_name,
		   arc.geom
	FROM tempus_networks.arc JOIN tempus_networks.pt_section ON (arc.id = pt_section.arc_id)
							 JOIN tempus_networks.pt_section_stop_times ON (arc.id = pt_section_stop_times.arc_id)
							 JOIN tempus_networks.pt_trip ON (pt_trip.id = pt_section_stop_times.pt_trip_id)
							 JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
							 JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
) q JOIN pgtempus.pt_trip_type ON (pt_trip_type.id = COALESCE(q.pt_trip_type_id, 1))
GROUP BY q.id, source_id, node_from_id, node_to_id, sub_arcs, length, diffusion, crossability, geom;

CREATE RULE update_pt_section_geom_pt_section AS
ON UPDATE TO tempus_networks.pt_section_geom
DO INSTEAD 
    UPDATE tempus_networks.pt_section
    SET sub_arcs = NEW.sub_arcs
    WHERE arc_id = OLD.id;

CREATE RULE update_pt_section_geom_arc AS
ON UPDATE TO tempus_networks.pt_section_geom
DO INSTEAD (
    UPDATE tempus_networks.arc
    SET id = NEW.id,
        node_from_id = NEW.node_from_id,
        node_to_id = NEW.node_to_id,
        length = NEW.length,
        diffusion = NEW.diffusion, 
        crossability = NEW.crossability, 
        geom = NEW.geom
    WHERE id = OLD.id;
);

DROP VIEW IF EXISTS tempus_networks.pt_trip_complete CASCADE;
CREATE VIEW tempus_networks.pt_trip_complete AS
SELECT pt_trip.id, 
       pt_trip.original_id,
       (array_agg(pt_section_stop_times.time_from ORDER BY time_from))[1] || array_agg(pt_section_stop_times.time_to ORDER BY time_to) as times,
       pt_agency.source_id,
       pt_trip.pt_trip_types_id, 
       pt_trip.name, 
       pt_trip.traffic_rules,
       pt_trip.direction, 
       pt_trip.direction_headsign, 
       pt_trip.pt_route_id, 
       pt_route.pt_agency_id, 
       (array_agg(pt_stop_from.node_id order by pt_section_stop_times.time_from, pt_section_stop_times.time_to))[1] || array_agg(pt_stop_to.node_id order by pt_section_stop_times.time_to, pt_section_stop_times.time_from) as pt_stops_id, 
       (array_agg(pt_stop_from.name order by pt_section_stop_times.time_from, pt_section_stop_times.time_to))[1] || array_agg(pt_stop_to.name order by pt_section_stop_times.time_to, pt_section_stop_times.time_from) as pt_stops_name, 
       array_agg(pt_section_stop_times.arc_id ORDER BY pt_section_stop_times.time_from, pt_section_stop_times.time_to)::bigint[] as pt_sections_id,
       round((sum(st_length(arc.geom)))::numeric, 3) as total_dist,
       CASE WHEN max(pt_section_stop_times.time_to) != min(pt_section_stop_times.time_from) 
                THEN ((sum(st_length(arc.geom))/1000) / (extract(epoch from (max(pt_section_stop_times.time_to) - min(pt_section_stop_times.time_from)))/3600))::numeric(8,2)
            ELSE ('Infinity'::real)
       END as speed, 
       days_period.days, 
       pt_trip.geom_arc,
       pt_trip.geom_nodes
FROM tempus_networks.pt_section_stop_times JOIN tempus_networks.pt_trip ON (pt_section_stop_times.pt_trip_id = pt_trip.id)
                                           JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                                           JOIN tempus_networks.pt_agency ON (pt_route.pt_agency_id = pt_agency.id)
                                           JOIN pgtempus.source ON (source.id = pt_agency.source_id)
                                           JOIN tempus_networks.days_period ON (pt_trip.days_period_id = days_period.id)
                                           JOIN tempus_networks.arc ON (pt_section_stop_times.arc_id = arc.id)
                                           JOIN tempus_networks.pt_stop pt_stop_from ON (arc.node_from_id = pt_stop_from.node_id)
										   JOIN tempus_networks.pt_stop pt_stop_to ON (arc.node_to_id = pt_stop_to.node_id)
                                           JOIN tempus_networks.node node_from ON (node_from.id = pt_stop_from.node_id)
                                           JOIN tempus_networks.node node_to ON (node_to.id = pt_stop_to.node_id) 
GROUP BY pt_trip.id, 
         pt_trip.original_id, 
         pt_agency.source_id, 
         pt_trip.pt_trip_types_id, 
         pt_trip.name, 
         pt_trip.traffic_rules, 
         pt_trip.direction, 
         pt_trip.direction_headsign,
         pt_route.id, 
         pt_agency.id, 
         days_period.days
ORDER BY pt_route.id, 3;

CREATE RULE update_pt_trip_complete_pt_trip AS
ON UPDATE TO tempus_networks.pt_trip_complete
DO INSTEAD 
    UPDATE tempus_networks.pt_trip
    SET id = NEW.id,
        original_id = NEW.original_id,
        pt_trip_types_id = NEW.pt_trip_types_id,
        name = NEW.name,
        traffic_rules = NEW.traffic_rules,
        direction = NEW.direction,
        direction_headsign = NEW.direction_headsign,
        pt_route_id = NEW.pt_route_id        
    WHERE id = OLD.id;

CREATE RULE update_pt_trip_complete_pt_route AS
ON UPDATE TO tempus_networks.pt_trip_complete
DO INSTEAD 
    UPDATE tempus_networks.pt_route
    SET pt_agency_id = NEW.pt_agency_id        
    WHERE id = OLD.pt_route_id;

DROP VIEW IF EXISTS tempus_networks.pt_route_path_geom;
CREATE VIEW tempus_networks.pt_route_path_geom AS
SELECT row_number() over() as id, 
       min(pt_stops_name) as pt_stops_name, 
       min(pt_stops_id) as pt_stops_id, 
	   pt_sections_id, 
       array_agg(id ORDER BY id) as pt_trips_id, 
       array(select distinct unnest(array_cat_agg(pt_trip_types_id)) order by 1) as pt_trip_types_id,
       pt_route_id, 
       min(direction) as direction,
       min(direction_headsign) as direction_headsign,
       pt_agency_id, 
       source_id,
       round(avg(total_dist)::numeric, 3) as total_dist,
       array(select distinct unnest(array_cat_agg(days)) order by 1) as days, 
       st_multi(max(geom_arc))::Geometry('MultiLinestringZ', %(target_srid)) as geom_arc, 
       max(geom_nodes)::Geometry('MultiPointZ', %(target_srid)) as geom_nodes
FROM tempus_networks.pt_trip_complete
GROUP BY pt_route_id, 
         pt_agency_id, 
         source_id, 
         pt_sections_id
ORDER BY source_id, pt_agency_id, pt_route_id;

DROP VIEW IF EXISTS tempus_networks.pt_route_complete;
CREATE VIEW tempus_networks.pt_route_complete AS
SELECT req.id, 
       req.source_id, 
       req.original_id, 
       req.short_name, 
       req.long_name, 
       req.pt_agency_id, 
       array_agg(DISTINCT pt_trip_type_id ORDER BY pt_trip_type_id) as pt_trip_types_id,
       array_agg(DISTINCT pt_trip_type.name) as pt_trip_types_name
FROM
(
    SELECT pt_route.id, 
           pt_agency.source_id, 
           pt_route.original_id, 
           pt_route.short_name, 
           pt_route.long_name,  
           pt_route.pt_agency_id,
           unnest(pt_trip.pt_trip_types_id) as pt_trip_type_id
    FROM tempus_networks.pt_route JOIN tempus_networks.pt_agency ON (pt_route.pt_agency_id = pt_agency.id)
                                  JOIN tempus_networks.pt_trip ON (pt_trip.pt_route_id = pt_route.id)
) req JOIN pgtempus.pt_trip_type ON ( pt_trip_type.id = req.pt_trip_type_id )
GROUP BY req.id, 
         req.source_id, 
         req.original_id, 
         req.short_name, 
         req.long_name,  
         req.pt_agency_id;

CREATE RULE update_pt_route_complete_pt_route AS
ON UPDATE TO tempus_networks.pt_route_complete
DO INSTEAD
    UPDATE tempus_networks.pt_route
    SET id = NEW.id, 
        original_id = NEW.original_id, 
        short_name = NEW.short_name,
        long_name = NEW.long_name,
        pt_agency_id = NEW.pt_agency_id
    WHERE id = OLD.id;
    
DROP VIEW IF EXISTS tempus_networks.pt_agency_complete;
CREATE VIEW tempus_networks.pt_agency_complete AS
    SELECT pt_agency.id,
           pt_agency.name,
           pt_agency.original_id, 
           pt_agency.source_id, 
           array_agg(pt_route.id) as pt_routes_id, 
           array_agg(pt_route.long_name) as pt_routes_long_name           
    FROM tempus_networks.pt_agency JOIN tempus_networks.pt_route ON (pt_route.pt_agency_id = pt_agency.id)
    GROUP BY pt_agency.id,
             pt_agency.name,
             pt_agency.source_id
    ORDER BY pt_agency.source_id, pt_agency.id; 

CREATE RULE update_pt_agency_complete_pt_agency AS
ON UPDATE TO tempus_networks.pt_agency_complete
DO INSTEAD
    UPDATE tempus_networks.pt_agency
    SET id = NEW.id,
        name = NEW.name,
        original_id = NEW.original_id,
        source_id = NEW.source_id
    WHERE id = OLD.id;

DROP VIEW IF EXISTS tempus_networks.pt_section_trip_stop_times;
CREATE VIEW tempus_networks.pt_section_trip_stop_times AS
SELECT id, source_id, node_from_id, node_to_id, sub_arcs, geom, traffic_rules, pt_trip_types_id, pt_route_id, direction, direction_headsign, pt_agency_id, pt_trip_id, time_from, time_to, day
FROM
(
    SELECT arc.id, 
           pt_agency.source_id, 
           arc.node_from_id, 
           arc.node_to_id, 
           arc.geom, 
           pt_section.sub_arcs,
           pt_trip.traffic_rules, 
           pt_trip.pt_trip_types_id, 
           pt_trip.pt_route_id, 
           pt_trip.direction, 
           pt_trip.direction_headsign,
           pt_route.pt_agency_id, 
           pt_section_stop_times.pt_trip_id, 
           pt_section_stop_times.time_from, 
           pt_section_stop_times.time_to, 
           unnest(days_period.days) as day
    FROM tempus_networks.arc JOIN tempus_networks.pt_section ON (arc.id = pt_section.arc_id)
                             JOIN tempus_networks.pt_section_stop_times ON (pt_section.arc_id = pt_section_stop_times.arc_id)
                             JOIN tempus_networks.pt_trip ON (pt_trip.id = pt_section_stop_times.pt_trip_id)
                             JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                             JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                             JOIN tempus_networks.days_period ON (pt_trip.days_period_id = days_period.id)
) q;

CREATE RULE update_pt_section_trip_stop_times_pt_trip AS
ON UPDATE TO tempus_networks.pt_section_trip_stop_times
DO INSTEAD (
    UPDATE tempus_networks.pt_trip
    SET traffic_rules = NEW.traffic_rules,
        pt_trip_types_id = NEW.pt_trip_types_id,
        pt_route_id = NEW.pt_route_id,
        direction = NEW.direction,
        direction_headsign = NEW.direction_headsign
    WHERE id = NEW.pt_trip_id; 
); 

CREATE RULE update_pt_section_trip_stop_times_arc AS
ON UPDATE TO tempus_networks.pt_section_trip_stop_times
DO INSTEAD (
    UPDATE tempus_networks.arc
    SET id = NEW.id,
        node_from_id = NEW.node_from_id,
        node_to_id = NEW.node_to_id,
        geom = NEW.geom
    WHERE id = OLD.id;
);

CREATE RULE update_pt_section_trip_stop_times_pt_section_stop_times AS
ON UPDATE TO tempus_networks.pt_section_trip_stop_times
DO INSTEAD (
    UPDATE tempus_networks.pt_section_stop_times
    SET pt_trip_id = NEW.pt_trip_id,
        time_from = NEW.time_from,
        time_to = NEW.time_to
    WHERE arc_id = NEW.id AND pt_trip_id = OLD.pt_trip_id; 
);

DROP VIEW IF EXISTS tempus_networks.barrier_geom CASCADE;
CREATE VIEW tempus_networks.barrier_geom AS
SELECT arc.id, 
       array[(select distinct unnest(array[node_from.source_id,node_to.source_id]))] as sources_id,
       barrier.name,
       barrier.type,
       arc.geom
FROM tempus_networks.barrier JOIN tempus_networks.arc ON (barrier.arc_id = arc.id)
                             JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                             JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id);
                             
CREATE RULE update_barrier_geom_barrier AS
ON UPDATE TO tempus_networks.barrier_geom
DO INSTEAD (
    UPDATE tempus_networks.barrier
    SET name= NEW.name,
        type = NEW.type
    WHERE arc_id = OLD.id;
);  

CREATE RULE update_barrier_geom_arc AS
ON UPDATE TO tempus_networks.barrier_geom
DO INSTEAD (
    UPDATE tempus_networks.arc
    SET id = NEW.id,
        geom = NEW.geom
    WHERE id = OLD.id;
);

do $$
begin
raise notice '==== Utilitary functions ===';
end$$;

DROP FUNCTION IF EXISTS array_search(anyelement, anyarray);
CREATE FUNCTION array_search(needle anyelement, haystack anyarray)
  RETURNS integer AS
$BODY$
    SELECT i
      FROM generate_subscripts($2, 1) AS i
     WHERE $2[i] = $1
  ORDER BY i
$BODY$
LANGUAGE sql STABLE;

DROP FUNCTION IF EXISTS array_diff(anyarray, anyarray);
CREATE FUNCTION array_diff(array1 anyarray, array2 anyarray)
  RETURNS anyarray AS 
$BODY$
    SELECT coalesce(array_agg(elem), '{}')
    FROM unnest(array1) elem
    WHERE elem <> all(array2)
$BODY$
LANGUAGE sql immutable;

-- Delete useless arcs and nodes (more efficient than keeping triggers active)
DROP FUNCTION IF EXISTS tempus_networks.delete_isolated_nodes();
CREATE FUNCTION tempus_networks.delete_isolated_nodes()
RETURNS void AS
$$
BEGIN
    DELETE FROM tempus_networks.arc
    WHERE id IN 
    (
        SELECT node.id
        FROM tempus_networks.node LEFT JOIN tempus_networks.arc ON ( arc.node_from_id = node.id OR arc.node_to_id = node.id )
        WHERE arc.id IS NULL
    );
END;
$$ 
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS tempus_networks.delete_isolated_arcs();
CREATE FUNCTION tempus_networks.delete_isolated_arcs()
RETURNS void AS
$$
BEGIN
    DELETE FROM tempus_networks.arc
    WHERE id IN 
    (
        SELECT arc1.id
        FROM tempus_networks.arc arc1 LEFT JOIN tempus_networks.arc arc2 ON ( arc1.node_from_id = arc2.node_from_id OR arc1.node_from_id = arc2.node_to_id OR arc1.node_to_id = arc2.node_from_id OR arc.node_to_id = arc2.node_to_id )
        WHERE arc2.id IS NULL
    );
END;
$$ 
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS tempus_networks.node_from_coordinates( float8, float8 ) ;
CREATE FUNCTION tempus_networks.node_from_coordinates( float8, float8 ) 
RETURNS bigint AS
$$
    WITH rs AS (
        SELECT id, node_from_id, node_to_id 
        FROM tempus_networks.arc
        ORDER BY geom <-> st_setsrid(st_point($1, $2), %(target_srid))
        LIMIT 1
    )
    SELECT CASE WHEN st_distance( p1.geom, st_setsrid(st_point($1,$2), %(target_srid))) < st_distance( p2.geom, st_setsrid(st_point($1,$2), %(target_srid))) THEN p1.id ELSE p2.id END
    FROM rs, tempus_networks.node p1, tempus_networks.node p2
    WHERE rs.node_from_id = p1.id AND rs.node_to_id = p2.id
$$
LANGUAGE SQL;

DROP FUNCTION IF EXISTS tempus_networks.node_from_coordinates_and_modes(float8, float8, integer[]);
CREATE FUNCTION tempus_networks.node_from_coordinates_and_modes( float8, float8, integer[] = array[0] ) 
RETURNS bigint AS 
$$
    WITH rs AS (
        SELECT arc.id, node_from_id, node_to_id 
        FROM tempus_networks.arc JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id) 
                                 JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(road_section_attributes.traffic_rules))
        WHERE transport_mode.id IN (SELECT unnest($3))
        ORDER BY geom <-> st_setsrid(st_point($1, $2), %(target_srid))
        LIMIT 1
    )
    select case when st_distance( p1.geom, st_setsrid(st_point($1,$2), %(target_srid))) < st_distance( p2.geom, st_setsrid(st_point($1,$2), %(target_srid))) then p1.id else p2.id end
    from rs, tempus_networks.node as p1, tempus_networks.node as p2
    where rs.node_from_id = p1.id and rs.node_to_id = p2.id
$$
LANGUAGE SQL;

DROP VIEW IF EXISTS tempus_networks.chk_cycles; 
CREATE VIEW tempus_networks.chk_cycles AS
(
    SELECT *
    FROM tempus_networks.arc
    WHERE node_from_id = node_to_id
);

-- Function used to ucalendar_date the "symbol_size" or the "symbol_color" field of an indicator table, used for layer displaying in QGIS
DROP FUNCTION IF EXISTS pgtempus.tempus_map_indicator(character varying, character varying, character varying, double precision, double precision, double precision);
CREATE FUNCTION pgtempus.tempus_map_indicator(
                                        layer_name character varying, 
                                        indic_name character varying, 
                                        map_mode character varying, -- 'color' or 'size'
                                        min_value double precision, 
                                        max_value double precision, 
                                        max_indic double precision
                                      )
RETURNS void AS
$BODY$
DECLARE
s character varying;
BEGIN
    IF (min_value = max_value) THEN min_value = 0; END IF; 
    s=$$
    UPDATE tempus_results.$$ || layer_name || $$ 
    SET symbol_$$ || map_mode || $$ = ($$ || indic_name || $$ - $$ || min_value::character varying || $$) / ($$ || max_value::character varying || $$/$$ || max_indic || $$::double precision)
    $$;
    EXECUTE (s);
    
    RETURN;
END; 
$BODY$
LANGUAGE plpgsql;

DROP TABLE IF EXISTS tempus_results.catalog;
CREATE TABLE tempus_results.catalog
(
  id serial NOT NULL PRIMARY KEY, -- Primary key - numerical ID of the calculated layer
  table_name character varying NOT NULL, -- Table name
  base_obj_type_id integer REFERENCES pgtempus.base_obj_type,
  result_type_id integer,
  comment text
);
CREATE INDEX catalog_base_obj_type_id_idx ON tempus_results.catalog(base_obj_type_id);

DROP FUNCTION IF EXISTS pgtempus.tempus_add_linking_arc( bigint, bigint, bigint, double precision, double precision );
CREATE FUNCTION pgtempus.tempus_add_linking_arc( from_node bigint, to_node bigint, parallel_arc bigint, from_abscissa double precision, to_abscissa double precision )
RETURNS void AS
$BODY$
DECLARE
    new_arc_id bigint;
    r record;
BEGIN
    SELECT INTO new_arc_id coalesce(max(id)+1, 1) FROM tempus_networks.arc;
    --raise notice '% % % % %', $1, $2, $3, $4, $5;
    -- 1. Arcs
    INSERT INTO tempus_networks.arc ( id, sources_id, original_id, node_from_id, node_to_id, geom, length, diffusion, crossability, type )
    SELECT new_arc_id, 
           CASE WHEN node_from.source_id < node_to.source_id THEN array[node_from.source_id, node_to.source_id] 
                WHEN node_from.source_id = node_to.source_id THEN array[node_from.source_id] 
                ELSE array[node_to.source_id, node_from.source_id] 
           END as sources_id,
           arc.original_id, 
           $1, 
           $2, 
           CASE WHEN geometrytype(st_linesubstring( arc.geom, $4, $5 )) = 'LINESTRING'   
                    THEN st_force3D(st_addpoint(st_addpoint(st_linesubstring( arc.geom, $4, $5 ), node_to.geom), node_from.geom, 0)) 
                WHEN geometrytype(st_linesubstring( arc.geom, $4, $5 )) = 'POINT'
                    THEN st_force3D( st_makeline(node_from.geom, node_to.geom) )
           END, 
           arc.length * ($5 - $4) + 25, -- add 25 meters to avoid that the arc is used instead of the main one
           arc.diffusion, 
           arc.crossability, 
           1
    FROM tempus_networks.arc CROSS JOIN tempus_networks.node node_from
                             CROSS JOIN tempus_networks.node node_to
    WHERE arc.id = $3 AND node_from.id = $1 AND node_to.id = $2;
    
    -- 2. Road sections attributes
    INSERT INTO tempus_networks.road_section_attributes ( arc_id, section_type_id, traffic_rules, car_speed_limit, street_name, road_number, lanes, total_lanes, min_width, max_slope )
    SELECT new_arc_id, 
           road_section_attributes.section_type_id, 
           road_section_attributes.traffic_rules, 
           road_section_attributes.car_speed_limit, 
           road_section_attributes.street_name, 
           road_section_attributes.road_number, 
           road_section_attributes.lanes, 
           road_section_attributes.total_lanes, 
           road_section_attributes.min_width, 
           road_section_attributes.max_slope
    FROM tempus_networks.road_section_attributes
    WHERE arc_id = $3;
    
    -- 3. Road sections speeds
    INSERT INTO tempus_networks.road_section_speed( arc_id, speed_rule, value )
    SELECT new_arc_id, 
           speed_rule, 
           value
    FROM tempus_networks.road_section_speed
    WHERE arc_id = $3;
    
    -- 4. Arcs sequences
    IF ($4 = 0) THEN 
        FOR r in ( 
                    WITH s AS ( 
                        WITH q AS (
                            SELECT (SELECT coalesce(max(id), 0) FROM tempus_networks.arcs_sequence) + row_number() over() as id,
                                   arcs_sequence.original_id, 
                                   arcs_sequence.arcs_id[ :array_length(arcs_sequence.arcs_id, 1)-1 ] || ARRAY[new_arc_id]::bigint[] as arcs_id, 
                                   arcs_sequence_time.traffic_rules, 
                                   arcs_sequence_time.value as time_value,
                                   arcs_sequence_toll.toll_rules,
                                   arcs_sequence_toll.value as toll_value
                            FROM tempus_networks.arcs_sequence LEFT JOIN tempus_networks.arcs_sequence_time ON (arcs_sequence.id = arcs_sequence_time.arcs_sequence_id)
                                                               LEFT JOIN tempus_networks.arcs_sequence_toll ON (arcs_sequence.id = arcs_sequence_toll.arcs_sequence_id)
                            WHERE arcs_id[ array_length(arcs_id, 1) ] = $3
                        )
                        SELECT id, original_id, arcs_id, traffic_rules, time_value, toll_value, toll_rules, a.arc_id, a.o
                        FROM q, unnest(arcs_id) WITH ORDINALITY a(arc_id, o)
                    )
                    SELECT s.id, s.original_id, s.arcs_id, s.traffic_rules, s.time_value, s.toll_value, s.toll_rules, st_makeline(arc.geom ORDER BY s.o) as geom, array(SELECT DISTINCT unnest(array_cat_agg(arc.sources_id)) ORDER BY 1) as sources_id
                    FROM s JOIN tempus_networks.arc ON (arc.id = s.arc_id)
                    GROUP BY s.id, s.original_id, s.arcs_id, s.traffic_rules, s.time_value, s.toll_value, s.toll_rules
                 )
        LOOP
            INSERT INTO tempus_networks.arcs_sequence ( id, sources_id, original_id, arcs_id, geom )
            VALUES( r.id, r.sources_id, r.original_id, r.arcs_id, r.geom );
            
            IF (r.time_value is not null) THEN
                INSERT INTO tempus_networks.arcs_sequence_time ( arcs_sequence_id, traffic_rules, value )
                VALUES ( r.id, r.traffic_rules, r.time_value );
            END IF;
            
            IF (r.toll_value is not null) THEN
                INSERT INTO tempus_networks.arcs_sequence_toll ( arcs_sequence_id, toll_rules, value )
                VALUES ( r.id, r.toll_rules, r.toll_value);
            END IF;
        END LOOP;
    ELSIF ($5 = 1) THEN
        FOR r in ( 
                    WITH s AS ( 
                        WITH q AS (
                            SELECT (SELECT coalesce(max(id), 0) FROM tempus_networks.arcs_sequence) + row_number() over() as id,
                                   arcs_sequence.original_id, 
                                   ARRAY[new_arc_id]::bigint[] || arcs_sequence.arcs_id[ 2: ]  as arcs_id, 
                                   arcs_sequence_time.traffic_rules, 
                                   arcs_sequence_time.value as time_value,
                                   arcs_sequence_toll.toll_rules,
                                   arcs_sequence_toll.value as toll_value
                            FROM tempus_networks.arcs_sequence LEFT JOIN tempus_networks.arcs_sequence_time ON (arcs_sequence.id = arcs_sequence_time.arcs_sequence_id)
                                                               LEFT JOIN tempus_networks.arcs_sequence_toll ON (arcs_sequence.id = arcs_sequence_toll.arcs_sequence_id)
                            WHERE arcs_sequence.arcs_id[1] = $3
                        )
                        SELECT id, original_id, arcs_id, traffic_rules, time_value, toll_value, toll_rules, a.arc_id, a.o
                        FROM q, unnest(arcs_id) WITH ORDINALITY a(arc_id, o)
                    )
                    SELECT s.id, s.original_id, s.arcs_id, s.traffic_rules, s.time_value, s.toll_value, s.toll_rules, st_makeline(arc.geom ORDER BY s.o) as geom, array(SELECT DISTINCT unnest(array_cat_agg(arc.sources_id)) ORDER BY 1) as sources_id
                    FROM s JOIN tempus_networks.arc ON (arc.id = s.arc_id)                     
                    GROUP BY s.id, s.original_id, s.arcs_id, s.traffic_rules, s.time_value, s.toll_value, s.toll_rules
                 )
        LOOP
            INSERT INTO tempus_networks.arcs_sequence ( id, sources_id, original_id, arcs_id, geom )
            VALUES( r.id, r.sources_id, r.original_id, r.arcs_id, r.geom );
            
            IF (r.time_value is not null) THEN
                INSERT INTO tempus_networks.arcs_sequence_time ( arcs_sequence_id, traffic_rules, value )
                VALUES (r.id, r.traffic_rules, r.time_value);
            END IF;
            
            IF (r.toll_value is not null) THEN
                INSERT INTO tempus_networks.arcs_sequence_toll ( arcs_sequence_id, toll_rules, value )
                VALUES (r.id, r.toll_rules, r.toll_value);
            END IF;
        END LOOP;
    END IF;
    
    -- To fix: When the parallel arc is in the middle of a sequence => make the automaton work on arcs labels (source_id, original_id)
    /*FOR r in SELECT (SELECT coalesce(max(id), 0) FROM tempus_networks.arcs_sequence) + row_number() over() as id,
                    arcs_sequence.original_id,
                    arcs_sequence.arcs_id[:array_search($4, arcs_sequence.arcs_id)-1] || ARRAY[new_arc_id, new_arc_id+1]::bigint[] || arcs_sequence.arcs_id[array_search(l_arc_id, arcs_sequence.arcs_id)+1:] as arcs_id,
                    arcs_sequence_time.traffic_rules,
                    arcs_sequence_time.value as time_value,
                    arcs_sequence_toll.toll_rules,
                    arcs_sequence_toll.value as toll_value
             FROM tempus_networks.arcs_sequence LEFT JOIN tempus_networks.arcs_sequence_time ON (arcs_sequence.id = arcs_sequence_time.arcs_sequence_id)
                                                LEFT JOIN tempus_networks.arcs_sequence_toll ON (arcs_sequence.id = arcs_sequence_toll.arcs_sequence_id)
             WHERE array_search($4, arcs_sequence.arcs_id)>1 AND array_search($4, arcs_sequence.arcs_id)<array_length(arcs_sequence.arcs_id, 1)
    LOOP
        INSERT INTO tempus_networks.arcs_sequence ( id, source_id, original_id, arcs_id )
        VALUES( r.id, ( SELECT source_id FROM tempus_networks.node WHERE id = $1 ), r.original_id, r.arcs_id);
        
        IF (r.time_value is not null) THEN
            INSERT INTO tempus_networks.arcs_sequence_time ( arcs_sequence_id, traffic_rules, value )
            VALUES (r.id, r.traffic_rules, r.time_value);
        END IF;
        
        IF (r.toll_value is not null) THEN
            INSERT INTO tempus_networks.arcs_sequence_toll ( arcs_sequence_id, toll_rules, value )
            VALUES (r.id, r.toll_rules, r.toll_value);
        END IF;
    END LOOP;*/    
END;
$BODY$
LANGUAGE plpgsql; 

DROP FUNCTION IF EXISTS pgtempus.tempus_attach_node_to_a_road_section( bigint, smallint[], double precision, smallint[] );
CREATE FUNCTION pgtempus.tempus_attach_node_to_a_road_section( node_id bigint, target_source_ids smallint[], distance double precision, allowed_traffic_rules smallint[] )
RETURNS void AS 
$BODY$
DECLARE
    l_arc_id bigint;
    l_node_from_id bigint;
    l_node_to_id bigint;
    l_abscissa real;
    r_arc_id bigint;
    r_abscissa real;
    r record;    
BEGIN    
    -- First arc (one direction)
    l_arc_id=null;
    -- get the closest road arc (if any)
    SELECT INTO l_arc_id, l_node_from_id, l_node_to_id, l_abscissa
                arc.id, arc.node_from_id, arc.node_to_id, st_linelocatepoint(arc.geom, node.geom)
    FROM tempus_networks.node JOIN tempus_networks.arc ON (st_dwithin(node.geom, arc.geom, $3))
                              JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                              JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
                              JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id)
    WHERE node.id = $1
      AND node_from.source_id = node_to.source_id -- arc belongs only to one source
      AND ARRAY[node_from.source_id] <@ $2 -- arc in the specified networks list
      AND ( 1 = ANY(road_section_attributes.traffic_rules ) ) -- attach to roads at least walkable
    ORDER BY ($4 <@ road_section_attributes.traffic_rules) DESC, st_distance(node.geom, arc.geom) 
    LIMIT 1;
    
    IF l_arc_id IS NOT NULL THEN
        -- Build the first arc
        PERFORM pgtempus.tempus_add_linking_arc( l_node_from_id, $1, l_arc_id, 0, l_abscissa );
        -- Build the second arc
        PERFORM pgtempus.tempus_add_linking_arc( $1, l_node_to_id, l_arc_id, l_abscissa, 1 );
        -- Search for POI or PT stops located on the same arc of the main road network and link the two POI/PT nodes
        FOR r in SELECT node.id as node_id, st_linelocatepoint(parallel_arc.geom, node.geom) as abscissa
                 FROM tempus_networks.node JOIN tempus_networks.arc arc_from ON (arc_from.node_to_id = node.id)
                                           JOIN tempus_networks.arc arc_to ON (arc_to.node_from_id = node.id)
                                           JOIN tempus_networks.arc parallel_arc ON (parallel_arc.node_from_id = arc_from.node_from_id AND parallel_arc.node_to_id = arc_to.node_to_id)
                                           JOIN tempus_networks.node node_from ON (parallel_arc.node_from_id = node_from.id)
                                           JOIN tempus_networks.node node_to ON (parallel_arc.node_to_id = node_to.id)
                 WHERE parallel_arc.id = l_arc_id AND node.source_id != ANY($2) AND (node_from.source_id = node_to.source_id) AND node_from.source_id = ANY($2) AND node.id != $1
        LOOP
            IF (r.abscissa <= l_abscissa) THEN 
                PERFORM pgtempus.tempus_add_linking_arc( r.node_id, $1, l_arc_id, r.abscissa, l_abscissa );
            ELSE PERFORM pgtempus.tempus_add_linking_arc( $1, r.node_id, l_arc_id, l_abscissa, r.abscissa );
            END IF;
        END LOOP;
        
        -- Second arc: opposite direction
        r_arc_id=null;
        SELECT INTO r_arc_id, r_abscissa                    
                    arc.id, st_linelocatepoint(arc.geom, node.geom)
        FROM tempus_networks.node JOIN tempus_networks.arc ON (st_dwithin(node.geom, arc.geom, $3))
                                  JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                                  JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
                                  JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id)
        WHERE node.id = $1 
          AND arc.node_from_id = l_node_to_id -- inverse direction
          AND arc.node_to_id = l_node_from_id            
          AND node_from.source_id = node_to.source_id -- arc belongs only to one source
          AND ARRAY[node_from.source_id] <@ $2 -- arc in the specified networks list
          AND ( 1 = ANY(road_section_attributes.traffic_rules ) )  -- attach to roads at least walkable
        ORDER BY ($4 <@ road_section_attributes.traffic_rules) DESC, st_distance(node.geom, arc.geom) -- priorize arcs with the given allowed modes
        LIMIT 1;  
        
        IF r_arc_id IS NOT NULL THEN
            -- Build the first arc
            PERFORM pgtempus.tempus_add_linking_arc( l_node_to_id, $1, r_arc_id, 0, r_abscissa );
            -- Build the second arc
            PERFORM pgtempus.tempus_add_linking_arc( $1, l_node_from_id, r_arc_id, r_abscissa, 1 );
            -- Search for POI or PT stops located on the same arc of the main road network and link the two POI/PT nodes
            FOR r in SELECT node.id as node_id, st_linelocatepoint(parallel_arc.geom, node.geom) as abscissa
                     FROM tempus_networks.node JOIN tempus_networks.arc arc_from ON (arc_from.node_to_id = node.id)
                                               JOIN tempus_networks.arc arc_to ON (arc_to.node_from_id = node.id)
                                               JOIN tempus_networks.arc parallel_arc ON (parallel_arc.node_from_id = arc_from.node_from_id AND parallel_arc.node_to_id = arc_to.node_to_id)
                                               JOIN tempus_networks.node node_from ON (parallel_arc.node_from_id = node_from.id)
                                               JOIN tempus_networks.node node_to ON (parallel_arc.node_to_id = node_to.id)
                     WHERE parallel_arc.id = r_arc_id AND node.source_id != ANY($2) AND (node_from.source_id = node_to.source_id) AND node_from.source_id = ANY($2) AND node.id != $1
            LOOP
                IF (r.abscissa <= r_abscissa) THEN 
                    PERFORM pgtempus.tempus_add_linking_arc( r.node_id, $1, r_arc_id, r.abscissa, r_abscissa );
                ELSE PERFORM pgtempus.tempus_add_linking_arc( $1, r.node_id, r_arc_id, r_abscissa, r.abscissa );
                END IF;
            END LOOP;
        END IF;
    END IF;
END;
$BODY$
LANGUAGE plpgsql; 

-- Function that is TRUE when the parameter date is a french bank holiday, FALSE otherwise
-- Algorithm based on the Easter day of each year
DROP FUNCTION IF EXISTS tempus_networks.french_bank_holiday_function(date);
CREATE FUNCTION tempus_networks.french_bank_holiday_function(calendar_date date)
  RETURNS boolean AS
$BODY$
DECLARE
    lgA INTEGER;
    lG integer;
    lC integer;
    lD integer;
    lE integer;
    lH integer;
    lK integer;
    lP integer;
    lQ integer;
    lI integer;
    lB integer;
    lJ1 integer;
    lJ2 integer;
    lR integer;
    stDate VARCHAR(10);
    dtPaq DATE;
    blFerie integer;
    bHol boolean; 

BEGIN
    bHol = FALSE; 
    stDate := TO_CHAR(calendar_date, 'DDMM');
    -- Fixed bank holidays
    IF stDate IN ('0101','0105','0805','1407','1508','0111','1111','2512') THEN
        bHol=TRUE;
    END IF;
    -- date of Easter sunday
    lgA := TO_CHAR(calendar_date, 'YYYY');
    lG := mod(lgA,19);
    lC := trunc(lgA / 100);
    lD := trunc(lC / 4);
    lE := trunc((8 * lC + 13) / 25);
    lH := mod((19 * lG + lC - lD - lE + 15),30);
    lK := trunc(lH / 28);
    lP := trunc(29 /(lH + 1));
    lQ := trunc((21 - lG) / 11);
    lI := (lK * lP * lQ - 1) * lK + lH;
    lB := trunc(lgA / 4) + lgA;
    lJ1 := lB + lI + 2 + lD - lC;
    lJ2 := mod(lJ1,7);
    lR := 28 + lI - lJ2;

    IF lR > 31 THEN
        dtPaq := to_date((lR-31)::character varying || '/04/' || lgA::character varying, 'dd/mm/yyyy');
    ELSE
        dtPaq := to_date(lR::character varying || '/03/' || lgA::character varying, 'dd/mm/yyyy');
    END IF;

    -- Mobile bank holidays (Easter Monday, Ascension, Pentecôte Monday)
    IF (calendar_date = dtPaq) OR (calendar_date = (dtPaq + 1)) OR (calendar_date = (dtPaq + 39)) OR (calendar_date = (dtPaq + 50)) THEN
        bHol=TRUE;
    END IF;    
    RETURN bHol;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
COMMENT ON FUNCTION tempus_networks.french_bank_holiday_function(date) IS 'Default bank holiday function corresponds to the french bank holiday function: can be replaced by any bank_holiday_function, for example simply reading values from table tempus_networks.bank_holiday.';

DROP FUNCTION IF EXISTS pgtempus.tempus_restore_foreign_keys_and_indexes( character varying );
CREATE FUNCTION pgtempus.tempus_restore_foreign_keys_and_indexes( table_name character varying )
RETURNS void AS
$BODY$
BEGIN
    IF (table_name = 'tempus_networks.node') THEN
        ALTER TABLE tempus_networks.node DROP CONSTRAINT IF EXISTS node_source_id_fkey;
        ALTER TABLE tempus_networks.node ADD CONSTRAINT node_source_id_fkey FOREIGN KEY (source_id) REFERENCES pgtempus.source (id) ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS node_source_id_idx ON tempus_networks.node(source_id);
        CREATE INDEX IF NOT EXISTS node_geom_idx ON tempus_networks.node USING gist(geom);
    
    ELSIF (table_name = 'tempus_networks.arc') THEN 
        ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_from_id_fkey;
        ALTER TABLE tempus_networks.arc ADD CONSTRAINT arc_node_from_id_fkey FOREIGN KEY (node_from_id) REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_to_id_fkey;
        ALTER TABLE tempus_networks.arc ADD CONSTRAINT arc_node_to_id_fkey FOREIGN KEY (node_to_id) REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS arc_node_from_id_idx ON tempus_networks.arc(node_from_id);
        CREATE INDEX IF NOT EXISTS arc_node_to_id_idx ON tempus_networks.arc(node_to_id);
        CREATE INDEX IF NOT EXISTS arc_geom_idx ON tempus_networks.arc USING gist(geom);
    
    ELSIF (table_name = 'tempus_networks.arcs_sequence') THEN
        CREATE INDEX IF NOT EXISTS arcs_sequence_arcs_id_idx ON tempus_networks.arcs_sequence USING gin(arcs_id);
        CREATE INDEX arcs_sequences_geom_idx ON tempus_networks.arcs_sequence USING gist(geom);
    
    ELSIF (table_name = 'tempus_networks.road_section_attributes') THEN 
        ALTER TABLE tempus_networks.road_section_attributes DROP CONSTRAINT IF EXISTS road_section_attributes_section_type_id_fkey;
        ALTER TABLE tempus_networks.road_section_attributes ADD CONSTRAINT road_section_attributes_section_type_id_fkey FOREIGN KEY (section_type_id) REFERENCES pgtempus.road_section_type ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.road_section_attributes DROP CONSTRAINT IF EXISTS road_section_attributes_arc_id_fkey;
        ALTER TABLE tempus_networks.road_section_attributes ADD CONSTRAINT road_section_attributes_arc_id_fkey FOREIGN KEY (arc_id) REFERENCES tempus_networks.arc ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS road_section_attributes_section_type_id_idx ON tempus_networks.road_section_attributes(section_type_id);
        CREATE INDEX IF NOT EXISTS road_section_attributes_arc_id_idx ON tempus_networks.road_section_attributes(arc_id);
        CREATE INDEX IF NOT EXISTS road_section_attributes_traffic_rules_idx ON tempus_networks.road_section_attributes USING gin(traffic_rules);
    
    ELSIF (table_name = 'tempus_networks.road_section_speed') THEN
        ALTER TABLE tempus_networks.road_section_speed DROP CONSTRAINT IF EXISTS road_section_speed_arc_id_fkey;
        ALTER TABLE tempus_networks.road_section_speed ADD CONSTRAINT road_section_speed_arc_id_fkey FOREIGN KEY (arc_id) REFERENCES tempus_networks.arc ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.road_section_speed DROP CONSTRAINT IF EXISTS road_section_speed_speed_rule_fkey;
        ALTER TABLE tempus_networks.road_section_speed ADD CONSTRAINT road_section_speed_speed_rule_fkey FOREIGN KEY (speed_rule) REFERENCES pgtempus.speed_rule ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS road_section_speed_arc_id_idx ON tempus_networks.road_section_speed(arc_id);
        CREATE INDEX IF NOT EXISTS road_section_speed_speed_rule_idx ON tempus_networks.road_section_speed(speed_rule);
    
    ELSIF (table_name = 'tempus_networks.arcs_sequence_time') THEN
        ALTER TABLE tempus_networks.arcs_sequence_time DROP CONSTRAINT IF EXISTS arcs_sequence_time_arcs_sequence_id_fkey;
        ALTER TABLE tempus_networks.arcs_sequence_time ADD CONSTRAINT arcs_sequence_time_arcs_sequence_id_fkey FOREIGN KEY (arcs_sequence_id) REFERENCES tempus_networks.arcs_sequence ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS arcs_sequence_time_arcs_sequence_id_idx ON tempus_networks.arcs_sequence_time(arcs_sequence_id);
        CREATE INDEX IF NOT EXISTS arcs_sequence_time_traffic_rules_idx ON tempus_networks.arcs_sequence_time USING gin(traffic_rules);
    
    ELSIF (table_name = 'tempus_networks.arcs_sequence_toll') THEN
        ALTER TABLE tempus_networks.arcs_sequence_toll DROP CONSTRAINT IF EXISTS arcs_sequence_toll_arcs_sequence_id_fkey;
        ALTER TABLE tempus_networks.arcs_sequence_toll ADD CONSTRAINT arcs_sequence_toll_arcs_sequence_id_fkey FOREIGN KEY (arcs_sequence_id) REFERENCES tempus_networks.arcs_sequence ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS arcs_sequence_toll_arcs_sequence_id_idx ON tempus_networks.arcs_sequence_toll(arcs_sequence_id);
        CREATE INDEX IF NOT EXISTS arcs_sequence_toll_toll_rules_idx ON tempus_networks.arcs_sequence_toll USING gin(toll_rules);
    
    ELSIF (table_name = 'tempus_networks.barrier') THEN
        ALTER TABLE tempus_networks.barrier DROP CONSTRAINT IF EXISTS barrier_arc_id_fkey;
        ALTER TABLE tempus_networks.barrier ADD CONSTRAINT barrier_arc_id_fkey FOREIGN KEY (arc_id) REFERENCES tempus_networks.arc ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS barrier_arc_id_idx ON tempus_networks.barrier(arc_id);
        CREATE INDEX IF NOT EXISTS barrier_type_idx ON tempus_networks.barrier(type);

    ELSIF (table_name = 'tempus_networks.poi') THEN
        ALTER TABLE tempus_networks.poi DROP CONSTRAINT IF EXISTS poi_node_id_fkey;
        ALTER TABLE tempus_networks.poi ADD CONSTRAINT poi_node_id_fkey FOREIGN KEY (node_id) REFERENCES tempus_networks.node (id) ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS poi_node_id_idx ON tempus_networks.poi(node_id);
        CREATE INDEX IF NOT EXISTS poi_type_idx ON tempus_networks.poi(type);
    
    ELSIF (table_name = 'pgtempus.vehicle') THEN
        ALTER TABLE tempus_networks.vehicle_parking_cost DROP CONSTRAINT IF EXISTS vehicle_parking_cost_vehicle_parking_rule_fkey;
        ALTER TABLE tempus_networks.vehicle_parking_cost ADD CONSTRAINT vehicle_parking_cost_vehicle_parking_rule_fkey FOREIGN KEY (vehicle_parking_rule) REFERENCES pgtempus.vehicle_parking_rule (id) ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.vehicle_parking_cost DROP CONSTRAINT IF EXISTS vehicle_parking_cost_node_id_fkey;
        ALTER TABLE tempus_networks.vehicle_parking_cost ADD CONSTRAINT vehicle_parking_cost_node_id_fkey FOREIGN KEY (node_id) REFERENCES tempus_networks.node (id) ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS vehicle_parking_cost_vehicle_parking_rule_idx ON tempus_networks.vehicle_parking_cost(vehicle_parking_rule);
        CREATE INDEX IF NOT EXISTS vehicle_parking_cost_node_id_idx ON tempus_networks.vehicle_parking_cost(node_id);
    
    ELSIF (table_name = 'tempus_networks.vehicle_parking_cost') THEN
        ALTER TABLE pgtempus.vehicle DROP CONSTRAINT IF EXISTS vehicle_transport_mode_id_fkey;
        ALTER TABLE pgtempus.vehicle ADD CONSTRAINT vehicle_transport_mode_id_fkey FOREIGN KEY (transport_mode_id) REFERENCES pgtempus.transport_mode (id) ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE pgtempus.vehicle DROP CONSTRAINT IF EXISTS vehicle_node_id_fkey;
        ALTER TABLE pgtempus.vehicle ADD CONSTRAINT vehicle_node_id_fkey FOREIGN KEY (node_id) REFERENCES tempus_networks.node (id) ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS vehicle_transport_mode_id_idx ON pgtempus.vehicle(transport_mode_id);
        CREATE INDEX IF NOT EXISTS vehicle_node_id_idx ON pgtempus.vehicle(node_id);
    
    ELSIF (table_name = 'tempus_networks.pt_agency') THEN
        ALTER TABLE tempus_networks.pt_agency DROP CONSTRAINT IF EXISTS pt_agency_source_id_fkey;
        ALTER TABLE tempus_networks.pt_agency ADD CONSTRAINT pt_agency_source_id_fkey FOREIGN KEY (source_id) REFERENCES pgtempus.source ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_agency_source_id_idx ON tempus_networks.pt_agency(source_id);
    
    ELSIF (table_name = 'pgtempus.pt_fare_rule') THEN
        ALTER TABLE pgtempus.pt_fare_rule DROP CONSTRAINT IF EXISTS pt_fare_rule_pt_agency_id_fkey;
        ALTER TABLE pgtempus.pt_fare_rule ADD CONSTRAINT pt_fare_rule_pt_agency_id_fkey FOREIGN KEY (pt_agency_id) REFERENCES tempus_networks.pt_agency ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_fare_rule_pt_agency_id_idx ON pgtempus.pt_fare_rule(pt_agency_id);
    
    ELSIF (table_name = 'tempus_networks.pt_route') THEN
        ALTER TABLE tempus_networks.pt_route DROP CONSTRAINT IF EXISTS pt_route_pt_agency_id_fkey;
        ALTER TABLE tempus_networks.pt_route ADD CONSTRAINT pt_route_pt_agency_id_fkey FOREIGN KEY (pt_agency_id) REFERENCES tempus_networks.pt_agency ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_route_pt_agency_id_idx ON tempus_networks.pt_route(pt_agency_id);
    
    ELSIF (table_name = 'tempus_networks.pt_stop') THEN
        ALTER TABLE tempus_networks.pt_stop DROP CONSTRAINT IF EXISTS pt_stop_node_id_fkey;
        ALTER TABLE tempus_networks.pt_stop ADD CONSTRAINT pt_stop_node_id_fkey FOREIGN KEY (node_id) REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_stop DROP CONSTRAINT IF EXISTS pt_stop_pt_fare_zone_id_fkey;
        ALTER TABLE tempus_networks.pt_stop ADD CONSTRAINT pt_stop_pt_fare_zone_id_fkey FOREIGN KEY (pt_fare_zone_id)  REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_stop_node_id_idx ON tempus_networks.pt_stop(node_id); 
        CREATE INDEX IF NOT EXISTS pt_stop_pt_fare_zone_id_idx ON tempus_networks.pt_stop(pt_fare_zone_id);
        CREATE INDEX IF NOT EXISTS pt_stop_traffic_rules_idx ON tempus_networks.pt_stop USING gin(traffic_rules);

    ELSIF (table_name = 'tempus_networks.pt_zonal_fare') THEN
        ALTER TABLE tempus_networks.pt_zonal_fare ADD CONSTRAINT pt_zonal_fare_pt_fare_rule_fkey FOREIGN KEY (pt_fare_rule) REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_zonal_fare ADD CONSTRAINT pt_zonal_fare_pt_route_id_fkey FOREIGN KEY (pt_route_id) REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_zonal_fare ADD CONSTRAINT pt_zonal_fare_zone_id_from_fkey FOREIGN KEY (zone_id_from) REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_zonal_fare ADD CONSTRAINT pt_zonal_fare_zone_id_to_fkey FOREIGN KEY (zone_id_to) REFERENCES tempus_networks.pt_fare_zone ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_zonal_fare_pt_fare_rule_idx ON tempus_networks.pt_zonal_fare(pt_fare_rule);
        CREATE INDEX IF NOT EXISTS pt_zonal_fare_pt_route_id_idx ON tempus_networks.pt_zonal_fare(pt_route_id);
        CREATE INDEX IF NOT EXISTS pt_zonal_fare_zone_id_from_idx ON tempus_networks.pt_zonal_fare(zone_id_from);
        CREATE INDEX IF NOT EXISTS pt_zonal_fare_zone_id_to_idx ON tempus_networks.pt_zonal_fare(zone_id_to);

    ELSIF (table_name = 'tempus_networks.pt_unit_fare') THEN
        ALTER TABLE tempus_networks.pt_unit_fare ADD CONSTRAINT pt_unit_fare_pt_fare_rule_fkey FOREIGN KEY (pt_fare_rule) REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_unit_fare ADD CONSTRAINT pt_unit_fare_pt_route_id_fkey FOREIGN KEY (pt_route_id) REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_unit_fare_pt_fare_rule_idx ON tempus_networks.pt_unit_fare(pt_fare_rule);
        CREATE INDEX IF NOT EXISTS pt_unit_fare_pt_route_id_idx ON tempus_networks.pt_unit_fare(pt_route_id);

    ELSIF (table_name = 'tempus_networks.pt_od_fare') THEN
        ALTER TABLE tempus_networks.pt_od_fare ADD CONSTRAINT pt_od_fare_pt_fare_rule_fkey FOREIGN KEY (pt_fare_rule) REFERENCES pgtempus.pt_fare_rule ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_od_fare ADD CONSTRAINT pt_od_fare_pt_route_id_fkey FOREIGN KEY (pt_route_id) REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_od_fare_pt_fare_rule_idx ON tempus_networks.pt_od_fare(pt_fare_rule);
        CREATE INDEX IF NOT EXISTS pt_od_fare_pt_route_id_idx ON tempus_networks.pt_od_fare(pt_route_id);

    ELSIF (table_name = 'tempus_networks.pt_trip') THEN
        ALTER TABLE tempus_networks.pt_trip ADD CONSTRAINT pt_trip_pt_route_id_fkey FOREIGN KEY (pt_route_id) REFERENCES tempus_networks.pt_route ON UPDATE CASCADE ON DELETE CASCADE;
        ALTER TABLE tempus_networks.pt_trip ADD CONSTRAINT pt_trip_days_period_id_fkey FOREIGN KEY (days_period_id) REFERENCES tempus_networks.days_period ON UPDATE CASCADE ON DELETE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_trip_pt_route_id_idx ON tempus_networks.pt_trip (pt_route_id);
        CREATE INDEX IF NOT EXISTS pt_trip_days_period_id_idx ON tempus_networks.pt_trip (days_period_id);
        CREATE INDEX IF NOT EXISTS pt_trip_traffic_rules_idx ON tempus_networks.pt_trip USING gin(traffic_rules);

    ELSIF (table_name = 'tempus_networks.pt_section') THEN
        ALTER TABLE tempus_networks.pt_section ADD CONSTRAINT pt_section_arc_id_fkey FOREIGN KEY (arc_id) REFERENCES tempus_networks.arc ON DELETE CASCADE ON UPDATE CASCADE;
        CREATE INDEX IF NOT EXISTS pt_section_arc_id_idx ON tempus_networks.pt_section (arc_id);

    ELSIF (table_name = 'tempus_networks.pt_section_stop_times') THEN
        ALTER TABLE tempus_networks.pt_section_stop_times ADD CONSTRAINT pt_section_stop_times_arc_id_fkey FOREIGN KEY (arc_id) REFERENCES tempus_networks.arc ON DELETE CASCADE ON UPDATE CASCADE;
        ALTER TABLE tempus_networks.pt_section_stop_times ADD CONSTRAINT pt_section_stop_times_pt_trip_id_fkey FOREIGN KEY (pt_trip_id) REFERENCES tempus_networks.pt_trip ON DELETE CASCADE ON UPDATE CASCADE;
        ALTER TABLE tempus_networks.pt_section_stop_times ADD CONSTRAINT pt_section_stop_times_pkey PRIMARY KEY (arc_id, pt_trip_id);
        CREATE INDEX IF NOT EXISTS pt_section_stop_times_arc_id_idx ON tempus_networks.pt_section_stop_times(arc_id);
        CREATE INDEX IF NOT EXISTS pt_section_stop_times_pt_trip_id_idx ON tempus_networks.pt_section_stop_times(pt_trip_id);
    
    ELSIF (table_name = 'tempus_results.catalog') THEN
        CREATE INDEX IF NOT EXISTS catalog_base_obj_type_id_idx ON tempus_results.catalog(base_obj_type_id);
    
    END IF;
END;
$BODY$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS pgtempus.tempus_remove_foreign_keys_and_indexes(character varying);
CREATE FUNCTION pgtempus.tempus_remove_foreign_keys_and_indexes(table_name character varying)
RETURNS void AS
$BODY$
BEGIN
    IF (table_name = 'tempus_networks.node') THEN
        ALTER TABLE tempus_networks.node DROP CONSTRAINT IF EXISTS node_source_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.node_source_id_idx;
        DROP INDEX IF EXISTS tempus_networks.node_geom_idx;

    ELSIF (table_name = 'tempus_networks.arc') THEN 
        ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_from_id_fkey;
        ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_to_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.arc_node_from_id_idx;
        DROP INDEX IF EXISTS tempus_networks.arc_node_to_id_idx;
        DROP INDEX IF EXISTS tempus_networks.arc_geom_idx;

    ELSIF (table_name = 'tempus_networks.arcs_sequence') THEN
        ALTER TABLE tempus_networks.arcs_sequence DROP CONSTRAINT IF EXISTS arcs_sequence_source_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.arcs_sequences_geom_idx;
        --DROP INDEX IF EXISTS tempus_networks.arcs_sequence_arcs_id_idx;

    ELSIF (table_name = 'tempus_networks.road_section_attributes') THEN 
        ALTER TABLE tempus_networks.road_section_attributes DROP CONSTRAINT IF EXISTS road_section_attributes_section_type_id_fkey;
        ALTER TABLE tempus_networks.road_section_attributes DROP CONSTRAINT IF EXISTS road_section_attributes_arc_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.road_section_attributes_section_type_id_idx;
        DROP INDEX IF EXISTS tempus_networks.road_section_attributes_arc_id_idx;
        DROP INDEX IF EXISTS tempus_networks.road_section_attributes_traffic_rules_idx;

    ELSIF (table_name = 'tempus_networks.road_section_speed') THEN
        ALTER TABLE tempus_networks.road_section_speed DROP CONSTRAINT IF EXISTS road_section_speed_arc_id_fkey;
        ALTER TABLE tempus_networks.road_section_speed DROP CONSTRAINT IF EXISTS road_section_speed_speed_rule_fkey;
        DROP INDEX IF EXISTS tempus_networks.road_section_speed_arc_id_idx;
        DROP INDEX IF EXISTS tempus_networks.road_section_speed_speed_rule_idx;

    ELSIF (table_name = 'tempus_networks.arcs_sequence_time') THEN
        ALTER TABLE tempus_networks.arcs_sequence_time DROP CONSTRAINT IF EXISTS arcs_sequence_time_arcs_sequence_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.arcs_sequence_time_arcs_sequence_id_idx;
        DROP INDEX IF EXISTS tempus_networks.arcs_sequence_time_traffic_rules_idx;

    ELSIF (table_name = 'tempus_networks.arcs_sequence_toll') THEN
        ALTER TABLE tempus_networks.arcs_sequence_toll DROP CONSTRAINT IF EXISTS arcs_sequence_toll_arcs_sequence_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.arcs_sequence_toll_arcs_sequence_id_idx;
        DROP INDEX IF EXISTS tempus_networks.arcs_sequence_toll_toll_rules_idx;

    ELSIF (table_name = 'tempus_networks.barrier') THEN
        ALTER TABLE tempus_networks.barrier DROP CONSTRAINT IF EXISTS barrier_arc_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.barrier_arc_id_idx;

    ELSIF (table_name = 'tempus_networks.poi') THEN
        ALTER TABLE tempus_networks.poi DROP CONSTRAINT IF EXISTS poi_node_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.poi_node_id_idx;

    ELSIF (table_name = 'pgtempus.vehicle') THEN
        ALTER TABLE pgtempus.vehicle DROP CONSTRAINT IF EXISTS vehicle_transport_mode_id_fkey;
        ALTER TABLE pgtempus.vehicle DROP CONSTRAINT IF EXISTS vehicle_node_id_fkey;
        DROP INDEX IF EXISTS pgtempus.vehicle_transport_mode_id_idx;
        DROP INDEX IF EXISTS pgtempus.vehicle_node_id_idx;

    ELSIF (table_name = 'tempus_networks.vehicle_parking_cost') THEN
        ALTER TABLE tempus_networks.vehicle_parking_cost DROP CONSTRAINT IF EXISTS vehicle_parking_cost_vehicle_parking_rule_fkey;
        ALTER TABLE tempus_networks.vehicle_parking_cost DROP CONSTRAINT IF EXISTS vehicle_parking_cost_node_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.vehicle_parking_cost_vehicle_parking_rule_idx;
        DROP INDEX IF EXISTS tempus_networks.vehicle_parking_cost_node_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_agency') THEN
        ALTER TABLE tempus_networks.pt_agency DROP CONSTRAINT IF EXISTS pt_agency_source_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_agency_source_id_idx;

    ELSIF (table_name = 'pgtempus.pt_fare_rule') THEN
        ALTER TABLE pgtempus.pt_fare_rule DROP CONSTRAINT IF EXISTS pt_fare_rule_pt_agency_id_fkey;
        DROP INDEX IF EXISTS pgtempus.pt_fare_rule_pt_agency_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_route') THEN
        ALTER TABLE tempus_networks.pt_route DROP CONSTRAINT IF EXISTS pt_route_pt_agency_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_route_pt_agency_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_stop') THEN
        ALTER TABLE tempus_networks.pt_stop DROP CONSTRAINT IF EXISTS pt_stop_node_id_fkey;
        ALTER TABLE tempus_networks.pt_stop DROP CONSTRAINT IF EXISTS pt_stop_pt_fare_zone_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_stop_node_id_idx; 
        DROP INDEX IF EXISTS tempus_networks.pt_stop_pt_fare_zone_id_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_stop_traffic_rules_idx;
    
    ELSIF (table_name = 'tempus_networks.pt_zonal_fare') THEN
        ALTER TABLE tempus_networks.pt_zonal_fare DROP CONSTRAINT IF EXISTS pt_zonal_fare_pt_fare_rule_fkey;
        ALTER TABLE tempus_networks.pt_zonal_fare DROP CONSTRAINT IF EXISTS pt_zonal_fare_pt_route_id_fkey;
        ALTER TABLE tempus_networks.pt_zonal_fare DROP CONSTRAINT IF EXISTS pt_zonal_fare_zone_id_from_fkey;
        ALTER TABLE tempus_networks.pt_zonal_fare DROP CONSTRAINT IF EXISTS pt_zonal_fare_zone_id_to_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_zonal_fare_pt_fare_rule_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_zonal_fare_pt_route_id_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_zonal_fare_zone_id_from_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_zonal_fare_zone_id_to_idx;

    ELSIF (table_name = 'tempus_networks.pt_unit_fare') THEN
        ALTER TABLE tempus_networks.pt_unit_fare DROP CONSTRAINT IF EXISTS pt_unit_fare_pt_fare_rule_fkey;
        ALTER TABLE tempus_networks.pt_unit_fare DROP CONSTRAINT IF EXISTS pt_unit_fare_pt_route_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_unit_fare_pt_fare_rule_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_unit_fare_pt_route_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_od_fare') THEN
        ALTER TABLE tempus_networks.pt_od_fare DROP CONSTRAINT IF EXISTS pt_od_fare_pt_fare_rule_fkey;
        ALTER TABLE tempus_networks.pt_od_fare DROP CONSTRAINT IF EXISTS pt_od_fare_pt_route_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_od_fare_pt_fare_rule_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_od_fare_pt_route_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_trip') THEN
        ALTER TABLE tempus_networks.pt_trip DROP CONSTRAINT IF EXISTS pt_trip_pt_route_id_fkey;
        ALTER TABLE tempus_networks.pt_trip DROP CONSTRAINT IF EXISTS pt_trip_days_period_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_trip_pt_route_id_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_trip_days_period_id_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_trip_traffic_rules_idx;

    ELSIF (table_name = 'tempus_networks.pt_section') THEN
        ALTER TABLE tempus_networks.pt_section DROP CONSTRAINT IF EXISTS pt_section_arc_id_fkey;
        DROP INDEX IF EXISTS tempus_networks.pt_section_arc_id_idx;

    ELSIF (table_name = 'tempus_networks.pt_section_stop_times') THEN
        ALTER TABLE tempus_networks.pt_section_stop_times DROP CONSTRAINT IF EXISTS pt_section_stop_times_arc_id_fkey;
        ALTER TABLE tempus_networks.pt_section_stop_times DROP CONSTRAINT IF EXISTS pt_section_stop_times_pt_trip_id_fkey;
        ALTER TABLE tempus_networks.pt_section_stop_times DROP CONSTRAINT IF EXISTS pt_section_stop_times_pkey;
        DROP INDEX IF EXISTS tempus_networks.pt_section_stop_times_arc_id_idx;
        DROP INDEX IF EXISTS tempus_networks.pt_section_stop_times_pt_trip_id_idx;
        
    ELSIF (table_name = 'tempus_results.catalog') THEN
        DROP INDEX IF EXISTS tempus_results.catalog_base_obj_type_id_idx;
    
    END IF;
END;
$BODY$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS tempus_networks.refresh_complete_views();
CREATE FUNCTION tempus_networks.refresh_complete_views()
RETURNS TRIGGER LANGUAGE plpgsql AS 
$$
BEGIN
    REFRESH MATERIALIZED VIEW tempus_networks.arc_complete;
    REFRESH MATERIALIZED VIEW tempus_networks.node_complete;
    RETURN NULL;
END 
$$;

DROP FUNCTION IF EXISTS pgtempus.tempus_restore_triggers();
CREATE FUNCTION pgtempus.tempus_restore_triggers()
RETURNS void AS
$BODY$
    CREATE TRIGGER refresh_views
        AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
        ON tempus_networks.arc FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.refresh_complete_views();

    CREATE TRIGGER refresh_views
        AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
        ON tempus_networks.node FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.refresh_complete_views();

    CREATE TRIGGER node_updated
        AFTER UPDATE
        ON tempus_networks.node FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.node_updated();
        
    CREATE TRIGGER arc_updated
        AFTER UPDATE
        ON tempus_networks.arc FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.arc_updated();

    CREATE TRIGGER arcs_sequence_updated
        AFTER UPDATE
        ON tempus_networks.arcs_sequence FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.arcs_sequence_updated();

    CREATE TRIGGER pt_trip_updated
        AFTER UPDATE
        ON tempus_networks.pt_trip FOR EACH STATEMENT
        EXECUTE PROCEDURE tempus_networks.pt_trip_updated();     
$BODY$
LANGUAGE sql; 

SELECT pgtempus.tempus_restore_triggers();

DROP FUNCTION IF EXISTS pgtempus.tempus_remove_triggers();
CREATE FUNCTION pgtempus.tempus_remove_triggers()
RETURNS void AS
$BODY$
    DROP TRIGGER IF EXISTS refresh_views ON tempus_networks.arc; 
    DROP TRIGGER IF EXISTS refresh_views ON tempus_networks.node; 
    DROP TRIGGER IF EXISTS node_updated ON tempus_networks.node; 
    DROP TRIGGER IF EXISTS arc_updated ON tempus_networks.arc; 
    DROP TRIGGER IF EXISTS arcs_sequence_updated ON tempus_networks.arcs_sequence; 
    DROP TRIGGER IF EXISTS pt_trip_updated ON tempus_networks.pt_trip; 
$BODY$
LANGUAGE sql; 

DROP FUNCTION IF EXISTS pgtempus.tempus_refresh_materialized_views();
CREATE FUNCTION pgtempus.tempus_refresh_materialized_views()
RETURNS void AS
$BODY$
    DROP INDEX IF EXISTS tempus_networks.arc_complete_id_idx; 
    DROP INDEX IF EXISTS tempus_networks.arc_complete_sources_id_idx; 
    DROP INDEX IF EXISTS tempus_networks.arc_complete_node_from_id_idx; 
    DROP INDEX IF EXISTS tempus_networks.arc_complete_node_to_id_idx; 
    DROP INDEX IF EXISTS tempus_networks.arc_complete_geom_idx; 
    DROP INDEX IF EXISTS tempus_networks.node_complete_id_idx; 
    DROP INDEX IF EXISTS tempus_networks.node_complete_geom_idx; 
    DROP INDEX IF EXISTS tempus_networks.node_complete_source_id_idx; 
    
    REFRESH MATERIALIZED VIEW tempus_networks.arc_complete; 
    REFRESH MATERIALIZED VIEW tempus_networks.node_complete; 
    
    CREATE INDEX ON tempus_networks.arc_complete(id);
    CREATE INDEX ON tempus_networks.arc_complete USING GIN(sources_id); 
    CREATE INDEX ON tempus_networks.arc_complete(node_from_id); 
    CREATE INDEX ON tempus_networks.arc_complete(node_to_id); 
    CREATE INDEX ON tempus_networks.arc_complete USING GIST(geom);  
    CREATE INDEX ON tempus_networks.node_complete(id); 
    CREATE INDEX ON tempus_networks.node_complete(source_id); 
    CREATE INDEX ON tempus_networks.node_complete USING GIST(geom); 
$BODY$
LANGUAGE sql;

SELECT pgtempus.tempus_refresh_materialized_views();

DROP FUNCTION IF EXISTS pgtempus.tempus_correction_geom(character varying, character varying, character varying);
CREATE FUNCTION pgtempus.tempus_correction_geom(nom_schema character varying, nom_table character varying, nom_colonne_geom character varying)
RETURNS void AS 
$BODY$
DECLARE
	s character varying;
BEGIN
	-- Correction of auto-intersections... 
    s='UPDATE ' || nom_schema || '.' || nom_table || ' 
	   SET ' || nom_colonne_geom || ' = CASE WHEN GeometryType(' || nom_colonne_geom || ') = ''POLYGON'' OR GeometryType(geom) = ''MULTIPOLYGON'' 
                                             THEN ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(' || nom_colonne_geom || ')),3)),0))
                                             WHEN GeometryType(' || nom_colonne_geom || ') = ''LINESTRING'' OR GeometryType(' || nom_colonne_geom || ') = ''MULTILINESTRING'' 
                                             THEN ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(' || nom_colonne_geom || ')),2)),0))
                                             WHEN GeometryType(' || nom_colonne_geom || ') = ''POINT'' OR GeometryType(' || nom_colonne_geom || ') = ''MULTIPOINT''
                                             THEN ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(' || nom_colonne_geom || ')),1)),0))
                                             ELSE ST_MakeValid(' || nom_colonne_geom || ')
				  END
	   WHERE NOT ST_Isvalid(' || nom_colonne_geom || ')';
	EXECUTE(s);    
END;
$BODY$
LANGUAGE 'plpgsql';

DROP FUNCTION IF EXISTS pgtempus.tempus_digitalization_direction_correction();
CREATE FUNCTION pgtempus.tempus_digitalization_direction_correction()
RETURNS void AS 
$BODY$
    WITH valid AS
        (
            SELECT arc.id, (st_distance(st_startpoint(arc.geom), node.geom) < st_distance(st_endpoint(arc.geom), node.geom)) as valid_direction
            FROM tempus_networks.arc join tempus_networks.node on (arc.node_from_id = node.id)
            WHERE arc.sources_id = ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')]
            order by 2
        )
        UPDATE tempus_networks.arc
        SET geom=st_reverse(geom)
        FROM valid
        WHERE valid.valid_direction=FALSE AND valid.id=arc.id;
$BODY$
LANGUAGE sql;

DROP VIEW IF EXISTS pgtempus.missing_indexes;
CREATE VIEW pgtempus.missing_indexes AS (
    SELECT c.conrelid::regclass AS "table",
           /* list of key column names in order */
           string_agg(a.attname, ',' ORDER BY x.n) AS columns,
           pg_catalog.pg_size_pretty(
              pg_catalog.pg_relation_size(c.conrelid)
           ) AS size,
           c.conname AS constraint,
           c.confrelid::regclass AS referenced_table
    FROM pg_catalog.pg_constraint c
       /* enumerated key column numbers per foreign key */
       CROSS JOIN LATERAL
          unnest(c.conkey) WITH ORDINALITY AS x(attnum, n)
       /* name for each key column */
       JOIN pg_catalog.pg_attribute a
          ON a.attnum = x.attnum
             AND a.attrelid = c.conrelid
    WHERE NOT EXISTS
            /* is there a matching index for the constraint? */
            (SELECT 1 FROM pg_catalog.pg_index i
             WHERE i.indrelid = c.conrelid
               /* the first index columns must be the same as the
                  key columns, but order doesn't matter */
               AND (i.indkey::smallint[])[0:cardinality(c.conkey)-1]
                   @> c.conkey)
      AND c.contype = 'f'
    GROUP BY c.conrelid, c.conname, c.confrelid
    ORDER BY pg_catalog.pg_relation_size(c.conrelid) DESC
);

