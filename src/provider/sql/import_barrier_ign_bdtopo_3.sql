-- Tempus - Road BDTopo SQL import Wrapper
/*
        Substitutions options
        %(source_name): name of the road network
        %(merge): IF True then the current network is merged with an existing one (objects having same "original_id" are merged)
*/

CREATE TABLE IF NOT EXISTS %(temp_schema).troncon_de_voie_ferree 
(
    id character varying,
    nature character varying,
    pos_sol character varying,
    etat character varying,
    date_creat character varying,
    date_maj character varying,
    date_app date,
    date_conf date,
    source character varying,
    id_source character varying,
    prec_plani double precision,
    prec_alti double precision,
    electrifie character varying,
    largeur character varying,
    nb_voies integer,
    usage character varying,
    vites_max smallint,
    id_vfn character varying,
    toponyme character varying,
    geom geometry(LineStringZM,2154)
);

CREATE TABLE IF NOT EXISTS %(temp_schema).cours_d_eau 
(
    id character varying,
    code_hydro character varying,
    toponyme character varying, 
    statut_top character varying,
    importance integer,
    date_creat date,
    date_maj date,
    date_app date,
    date_conf date,
    source character varying,
    id_source character varying,
    statut character varying,
    maree character varying,
    permanent character varying,
    comment character varying,
    geom geometry(PolygonZ,2154)
);

CREATE TABLE IF NOT EXISTS %(temp_schema).limite_terre_mer 
(
    id character varying,
    code_hydro character varying,
    code_pays character varying,
    type_limit character varying,
    niveau character varying,
    date_creat character varying,
    date_maj character varying,
    date_app date,
    date_conf date,
    source character varying,
    id_source character varying,
    prec_plani double precision,
    src_coord character varying,
    statut character varying,
    origine character varying,
    comment character varying,
    geom geometry(MultiLineString,2154) 
) ;

DO
$$
BEGIN
    IF %(merge) = True THEN
        RAISE notice '==== 0. Doubles suppression ===';
        DELETE FROM %(temp_schema).limite_terre_mer
        WHERE id IN
        (
            SELECT arc.original_id
            FROM tempus_networks.arc JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                                     JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
            WHERE node_from.source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') AND node_from.source_id = node_to.source_id
        );
        
        DELETE FROM %(temp_schema).troncon_de_voie_ferree
        WHERE id IN
        (
            SELECT arc.original_id
            FROM tempus_networks.arc JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                                     JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
            WHERE node_from.source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') AND node_from.source_id = node_to.source_id
        );
        
        DELETE FROM %(temp_schema).cours_d_eau
        WHERE id IN
        (
            SELECT arc.original_id
            FROM tempus_networks.arc JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                                     JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
            WHERE node_from.source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') AND node_from.source_id = node_to.source_id
        );
    END IF;
END
$$;

DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$;

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'cours_d_eau', 'geom');
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'troncon_de_voie_ferree', 'geom');
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'limite_terre_mer', 'geom');


do $$
begin
RAISE notice '==== 2. Create new types ===';
end$$;

INSERT INTO pgtempus.barrier_type(name)
SELECT 'Limite terre-mer'
UNION
SELECT 'Cours d''eau'
UNION
SELECT 'Tronçon de voie ferrée'
EXCEPT
SELECT name FROM pgtempus.barrier_type;

do $$
begin
RAISE notice '==== 2. Import barriers ===';
end$$;

CREATE OR REPLACE FUNCTION pgtempus.extract_barriers_from_linestrings(table_name character varying)
RETURNS void AS 
$BODY$
DECLARE
    s character varying;
BEGIN
    s = $$ 
          DROP TABLE IF EXISTS %(temp_schema).node ;  
          CREATE TABLE %(temp_schema).node (id serial, geom geometry(pointZ, %(target_srid)));
          
          ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
          SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node));
          
          CREATE TABLE %(temp_schema).$$ || table_name || $$_simple AS
          SELECT *, (st_dump(geom)).geom as geom_simple
          FROM %(temp_schema).$$ || table_name || $$;
          
          -- take the geometry of start and end nodes of each way
          INSERT INTO %(temp_schema).node (geom)
              SELECT st_force3DZ(st_transform(st_startpoint(st_linemerge(geom_simple)), %(target_srid))) from %(temp_schema).$$ || table_name || $$_simple;
          INSERT INTO %(temp_schema).node (geom)
              SELECT st_force3DZ(st_transform(st_endpoint(st_linemerge(geom_simple)), %(target_srid))) from %(temp_schema).$$ || table_name || $$_simple;
          
          
          ALTER TABLE %(temp_schema).$$ || table_name || $$_simple
          ADD COLUMN arc_id bigserial, 
          ADD COLUMN start_geom geometry(pointZ, %(target_srid)), 
          ADD COLUMN end_geom geometry(pointZ, %(target_srid)),
          ADD COLUMN start_node bigint,
          ADD COLUMN end_node bigint;        
          
          UPDATE %(temp_schema).$$ || table_name || $$_simple
          SET arc_id = arc_id + (SELECT coalesce(max(id), 0) FROM tempus_networks.arc); 
          
          UPDATE %(temp_schema).$$ || table_name || $$_simple
          SET start_geom = st_force3DZ(st_transform(st_startpoint(st_linemerge(geom_simple)), %(target_srid))), 
              end_geom = st_force3DZ(st_transform(st_endpoint(st_linemerge(geom_simple)), %(target_srid)));
          
          CREATE INDEX ON %(temp_schema).$$ || table_name || $$_simple using gist(start_geom);
          CREATE INDEX ON %(temp_schema).$$ || table_name || $$_simple using gist(end_geom);
          CREATE INDEX ON %(temp_schema).$$ || table_name || $$_simple using gist(geom_simple);           
          CREATE INDEX ON %(temp_schema).node using gist(geom);
          
          -- remove duplicate nodes (arbitrarily)
          DROP TABLE IF EXISTS %(temp_schema).unique_node;
          CREATE TABLE %(temp_schema).unique_node AS
          (
              SELECT DISTINCT ON(geom) id, geom
              FROM %(temp_schema).node
          );
          CREATE INDEX ON %(temp_schema).unique_node using gist(geom);
          
          INSERT INTO tempus_networks.node(id, source_id, geom)
          (
              SELECT id,  
                     (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') as source_id,
                     ST_Force3DZ(st_transform(geom,%(target_srid)))
              FROM %(temp_schema).unique_node
          );
          
          -- resolve start and end node ids
          UPDATE %(temp_schema).$$ || table_name || $$_simple
          SET start_node = ( SELECT n1.id FROM %(temp_schema).unique_node n1 WHERE st_equals(n1.geom, start_geom) LIMIT 1 ), 
              end_node = ( SELECT n1.id FROM %(temp_schema).unique_node n1 WHERE st_equals(n1.geom, end_geom) LIMIT 1 );
        
          INSERT INTO tempus_networks.arc(id, sources_id, original_id, node_from_id, node_to_id, length, type, diffusion, crossability, geom)
          (
              SELECT
                   arc_id as id, 
                   ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
                   id as original_id, 
                   start_node, 
                   end_node, 
                   round(st_length(geography(st_transform(geom, 4326)))::numeric,2) as length, 
                   3, 
                   0,
                   3, 
                   ST_LineMerge(st_force3DZ(st_transform(st_simplify( geom_simple, %(geom_simplify), True ), %(target_srid)))) as geom 
              FROM %(temp_schema).$$ || table_name || $$_simple
              WHERE arc_id IS NOT NULL AND geom IS NOT NULL
          ); 
       $$; 
    --RAISE NOTICE '%', s;
    EXECUTE(s);
   
    IF (table_name = 'cours_d_eau') THEN
        s=$$
        INSERT INTO tempus_networks.barrier(arc_id, name, type)
        SELECT arc_id, toponyme, (SELECT id FROM pgtempus.barrier_type WHERE name = 'Cours d''eau')
        FROM
        (
            SELECT arc_id, toponyme
            FROM %(temp_schema).$$ || table_name || $$_simple
            WHERE arc_id IS NOT NULL AND geom_simple IS NOT NULL
        ) q;
        $$;
    ELSIF (table_name = 'troncon_de_voie_ferree') THEN
        s=$$
        INSERT INTO tempus_networks.barrier(arc_id, name, type)
        SELECT arc_id, toponyme, (SELECT id FROM pgtempus.barrier_type WHERE name = 'Tronçon de voie ferrée')
        FROM
        (
            SELECT arc_id, toponyme
            FROM %(temp_schema).$$ || table_name || $$_simple
            WHERE arc_id IS NOT NULL AND geom_simple IS NOT NULL
        ) q;
        $$;
    ELSIF (table_name = 'limite_terre_mer') THEN
        s=$$
        INSERT INTO tempus_networks.barrier(arc_id, type)
        SELECT arc_id, (SELECT id FROM pgtempus.barrier_type WHERE name = 'Limite terre-mer')
        FROM
        (
            SELECT arc_id
            FROM %(temp_schema).$$ || table_name || $$_simple
            WHERE arc_id IS NOT NULL AND geom_simple IS NOT NULL
        ) q;
        $$;
    END IF;
    --RAISE NOTICE '%', s;    
    EXECUTE(s);
END;
$BODY$
LANGUAGE plpgsql;

do $$
begin
    raise notice '==== 2.1 Tronçons de voie ferrée ===';
end$$;
SELECT pgtempus.extract_barriers_from_linestrings('troncon_de_voie_ferree');
do $$
begin
    raise notice '==== 2.2 Limite terre - mer ===';
end$$;
SELECT pgtempus.extract_barriers_from_linestrings('limite_terre_mer');
do $$
begin
    raise notice '==== 2.3 Cours d''eau ===';
end$$;
SELECT pgtempus.extract_barriers_from_linestrings('cours_d_eau');       




