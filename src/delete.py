#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus data deleter

import provider
    
def delete(args):
    subs = {}
    if args.source_name is None:
        sys.stderr.write("The road network name must be specified with --source-name !\n")
        sys.exit(1)
    subs["source_name"] = args.source_name[0]
    roadd = provider.Delete(dbstring=args.dbstring, logfile=args.logfile, subs=subs)
    return roadd.run()

