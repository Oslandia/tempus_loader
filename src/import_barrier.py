#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus barrier data importer

import provider
import sys  

def import_barrier_ign_bdtopo(args, shape_options):
    """Load IGN (BDTopo) barrier data into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A road data source name must be supplied. Use --source-name\n")
        sys.exit(1)        
    subs['source_name'] = args.source_name[0]
        
    if args.prefix is None:
        args.prefix = ''
        
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
        
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
        
    if args.source_comment and args.source_comment is not None:
        subs["source_comment"] = args.source_comment
    else:
        subs["source_comment"] = ""
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.geom_simplify and args.geom_simplify is not None: 
        subs["geom_simplify"] = args.geom_simplify
    else: 
        subs["geom_simplify"] = "0"
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    shape_options['S'] = False
    
    Importer = {
        #'2.2': provider.ImportRoadIGNBDTopo_2_2,
        '3': provider.ImportBarrierIGNBDTopo_3,
        None: provider.ImportBarrierIGNBDTopo_3
    } [args.model_version]
    
    bdtopoi = Importer(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)    
    return bdtopoi.run()
    
    
    
    
    
    
    